<?php

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


Route::get('/','ControllerCus@Index')->name("home1");
Route::post('/buses', 'ControllerCus@Buses')->name('Buses');
 /*Đăng ký*/
 Route::post('/register1','ControllerCus@register')->name('register1');
 /*Đăng nhập*/
 Route::post('/login1','ControllerCus@login')->name('login1');
 /*Đăng xuất*/
 Route::get('logout1', function(){
     session()->flush();
    DB::select("SELECT name_province FROM province");
     return redirect(route("home1"));
 })->name('logout1');
 Route::get('/ticket/{id}','ProcessController@Chonve')->middleware('checkdangnhap')->name('chonve');
 Route::post('/process-book','ProcessController@processbook')->name('process-book');
 Route::post('/process-cancel','ProcessController@processcancel')->name('process-cancel');
 Route::get('/chonmon','ProcessController@chonmon')->name('chonmon');
 Route::post('/pick-ticket','ProcessController@chondatve')->name('pick');
 Route::get('/finish/{id}/{makh}/{ran}','ProcessController@finish')->middleware('checkdangnhap')->name('finish');
 Route::get('/payment','ProcessController@payment')->name('payment');
 Route::post('/payment-create','ProcessController@payment_create')->name('payment_create');
 Route::get('/return','ProcessController@payment_return')->name('return');
 Route::get('/result','ProcessController@result')->name('result');
 Route::get('/send/message','SmsController@sendMessage');

 Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
  });
/////////////////////////////////////////////////////////////////////////////////////////////
// Route::middleware('auth:api')->get('/user', 'Controller@AuthRouteAPI');
Route::prefix('admin')->middleware('auth')->name('admin.')->group(function(){
   /*=============================USER========================== */

    $prefixUrl = "user";
    $controllerName = "user";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::get('formEdit', $controller . 'formEdit') -> name('formEdit');
        Route::post('save', $controller . 'save') -> name('save');
        Route::get('changeStatus/{status}', $controller . 'changeStatus') -> name('changeStatus');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });

    /*=============================PAYMENT========================== */

    $prefixUrl = "payment";
    $controllerName = "payment";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::get('formEdit', $controller . 'formEdit') -> name('formEdit');
        Route::post('save', $controller . 'save') -> name('save');
        Route::get('changeStatus/{status}', $controller . 'changeStatus') -> name('changeStatus');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });

    /*=============================CHUYEN XE========================== */
    $prefixUrl = "chuyenxe";
    $controllerName = "chuyenxe";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::get('formEdit', $controller . 'formEdit') -> name('formEdit');
        Route::post('save', $controller . 'save') -> name('save');
        Route::get('changeStatus/{status}', $controller . 'changeStatus') -> name('changeStatus');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });
    /*=============================KHACH HANG========================== */

    $prefixUrl = "khachhang";
    $controllerName = "khachhang";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });
    /*=============================MENU========================== */

    $prefixUrl = "menu";
    $controllerName = "menu";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });
    /*=============================NHAN VIEN========================== */

    $prefixUrl = "nhanvien";
    $controllerName = "nhanvien";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });

    /*=============================TIN TUC========================== */

    $prefixUrl = "tintuc";
    $controllerName = "tintuc";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });

    /*=============================VE XE========================== */

    $prefixUrl = "vexe";
    $controllerName = "vexe";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
        Route::get('sendsms', $controller . 'sendsms') -> name('sendsms');
    });

    /*=============================LO TRINH========================== */

    $prefixUrl = "lotrinh";
    $controllerName = "lotrinh";
    Route::prefix($prefixUrl) ->name ($prefixUrl . '.') ->group(function() use($controllerName){
        $controller = ucfirst($controllerName) . 'Controller@';
        Route::get('index', $controller . 'index')->name('index');
        Route::get('form', $controller . 'form') -> name('form');
        Route::post('save', $controller . 'save') -> name('save');
        Route::post('deleteItem', $controller . 'deleteItem') -> name('deleteItem');
    });

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


