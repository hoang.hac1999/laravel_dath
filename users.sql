-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 02, 2021 lúc 06:11 PM
-- Phiên bản máy phục vụ: 10.4.17-MariaDB
-- Phiên bản PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bus_ticket`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `image`, `status`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ngô Thanh Thái', 'thanhthaiqngai3011@gmail.com', NULL, '$2y$10$gjxzDl5alNaI36bjzQhsKeZTP8wEFf7Wl5lOZna6t7aFicKFAqMjm', 'Tk9025U2.jpg', 1, 1, NULL, '2021-07-01 07:14:58', '2021-07-02 03:04:33'),
(5, 'Phan Trung Huy', 'phantrunghuy@gmail.com', NULL, '$2y$10$1i2F6ONZPZioja5RGkvsNeAY/UEmIIlt8KiOzSE4m7cplsHbPTEVK', NULL, 1, 1, NULL, '2021-07-02 14:55:21', '2021-07-02 03:04:20'),
(6, 'Nguyễn Văn A', 'nguyenvana@gmail.com', NULL, '$2y$10$EduTqJDbCm5T/7/aIE/zz.xTtAWa9.ZX0idCVCzWYPeNGgHYuSGzW', NULL, 1, 3, NULL, '2021-07-02 14:56:19', '2021-07-02 03:04:03'),
(7, 'Võ Ngọc Linh', 'vongoclinh@gmail.com', NULL, '$2y$10$Q0rrl6UXRkMgKCE5KPxSQe1o1sCiDTGq7cAEJOKXIeWcgX5UBLLI6', NULL, 1, 2, NULL, '2021-07-02 15:05:06', '2021-07-02 03:05:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
