-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 24, 2021 lúc 05:33 AM
-- Phiên bản máy phục vụ: 10.4.17-MariaDB
-- Phiên bản PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bus_ticket`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `buses`
--

CREATE TABLE `buses` (
  `id` int(11) NOT NULL,
  `id_staff` int(11) NOT NULL,
  `id_route` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `time_start` time NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `ticket_price` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_del` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `buses`
--

INSERT INTO `buses` (`id`, `id_staff`, `id_route`, `id_driver`, `id_vehicle`, `date_start`, `time_start`, `status`, `ticket_price`, `created_at`, `updated_at`, `is_del`) VALUES
(1, 1, 2, 3, 6, '2021-05-05', '22:00:00', 3, 350000, '2021-01-14 23:26:48', '2021-07-24 10:03:26', 0),
(2, 1, 1, 3, 7, '2021-07-05', '12:00:00', 2, 350000, '2021-02-01 11:57:22', '2021-07-24 10:03:17', 0),
(5, 1, 1, 4, 7, '2021-07-15', '10:30:00', 2, 350000, '2021-07-10 06:03:51', '2021-07-24 10:03:07', 0),
(6, 1, 2, 5, 6, '2021-07-24', '13:00:00', 1, 350000, '2021-07-10 06:16:45', '2021-07-24 10:02:57', 0),
(7, 1, 1, 3, 6, '2021-07-24', '19:30:00', 0, 350000, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0),
(8, 1, 2, 5, 8, '2021-07-24', '19:00:00', 0, 350000, '2021-07-24 10:17:15', '2021-07-24 10:17:39', 0),
(9, 1, 1, 3, 6, '2021-07-24', '15:30:00', 0, 350000, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bus_model`
--

CREATE TABLE `bus_model` (
  `Mã` int(11) NOT NULL,
  `Tên_Loại` varchar(255) NOT NULL,
  `Loại_ghế` int(11) NOT NULL,
  `Số_ghế` int(11) NOT NULL,
  `Số_hàng` int(11) NOT NULL,
  `Số_cột` int(11) NOT NULL,
  `Sơ_đồ` varchar(255) NOT NULL,
  `Mã_nhân_viên_tạo` int(11) NOT NULL,
  `Mã_nhân_viên_chỉnh_sửa` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `bus_model`
--

INSERT INTO `bus_model` (`Mã`, `Tên_Loại`, `Loại_ghế`, `Số_ghế`, `Số_hàng`, `Số_cột`, `Sơ_đồ`, `Mã_nhân_viên_tạo`, `Mã_nhân_viên_chỉnh_sửa`, `created_at`, `updated_at`) VALUES
(6, 'Ghe Ngoi', 0, 47, 12, 6, '100000110011110011110011110011110011110011110011110011110011110011111111', 1, 1, '2021-01-15 00:36:52', '2021-01-15 00:37:04'),
(7, 'Giuong Nam', 1, 41, 13, 5, '10000101011010110101101011010111111101011010110101101011010111111', 1, 1, '2021-01-15 00:37:01', '2021-01-15 00:37:07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `name_en` varchar(255) NOT NULL,
  `birthday` date DEFAULT NULL,
  `sex` set('0','1','2') DEFAULT NULL,
  `address` tinytext DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `name_en`, `birthday`, `sex`, `address`, `password`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(6, 'Phan Trung Huy', 'Phan Trung Huy', '1999-08-01', '1', NULL, 'a9e613cb8ecebd0517fbe86c75e5720c', NULL, '0366440472', '2021-06-26 16:10:50', '2021-06-26 16:10:50'),
(9, 'Phan Trung Huy', 'Phan Trung Huy', '1999-08-01', '1', NULL, 'a9e613cb8ecebd0517fbe86c75e5720c', NULL, '0366440444', '2021-06-26 16:10:50', '2021-06-26 16:10:50'),
(10, 'trung huy', 'trung huy', '1994-02-20', '1', NULL, 'a9e613cb8ecebd0517fbe86c75e5720c', NULL, '0366440470', '2021-07-04 19:20:40', '2021-07-04 19:20:40'),
(11, 'Ngô Thanh Thái', 'Ngo Thanh Thai', '1994-02-20', '1', NULL, '329c77bc29aa4990e4bd9407fc3bd962', NULL, '0364129272', '2021-07-10 18:04:42', '2021-07-10 18:04:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employee`
--

CREATE TABLE `employee` (
  `Mã` int(11) NOT NULL,
  `Họ_Tên` varchar(255) NOT NULL,
  `Ngày_sinh` date NOT NULL,
  `Giới_tính` set('0','1','2') DEFAULT NULL,
  `Địa_chỉ` tinytext NOT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Loại_NV` set('QTV','QLDV','TX') NOT NULL,
  `Chi_nhánh` varchar(255) DEFAULT NULL,
  `Bằng_lái` varchar(255) DEFAULT NULL,
  `Sđt` varchar(255) NOT NULL,
  `Date_Starting` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `employee`
--

INSERT INTO `employee` (`Mã`, `Họ_Tên`, `Ngày_sinh`, `Giới_tính`, `Địa_chỉ`, `Username`, `Password`, `Email`, `Loại_NV`, `Chi_nhánh`, `Bằng_lái`, `Sđt`, `Date_Starting`, `created_at`, `updated_at`) VALUES
(1, 'Ngô Thanh Thái', '1994-04-10', '1', 'Quảng Ngãi', 'admin', '96e79218965eb72c92a549dd5a330112', 'hhhhhh@gmail.com', 'QTV', 'Hồ Chí Minh', NULL, '0946881949', '2021-01-15', '2021-01-15 00:31:45', '2021-01-15 00:32:05'),
(3, 'Phan Trung Huy', '1993-05-11', '1', 'Quảng Ngãi', 'tx_anhson', 'd41d8cd98f00b204e9800998ecf8427e', 'phananhson@email.com', 'TX', 'Hồ Chí Minh', '12345678', '0967676767', '2021-01-15', '2021-01-15 00:31:49', '2021-01-15 00:32:08'),
(4, 'Nguyễn Văn A', '1994-04-10', '1', '506/19/15 3/2 street', 'tx_nguyenbang', 'd41d8cd98f00b204e9800998ecf8427e', 'nguyenbang@gmail.com', 'TX', 'Ho Chi Minh City', '8886534', '0998989898', '2021-01-15', '2021-01-15 00:31:52', '2021-01-15 00:32:11'),
(5, 'Nguyễn Văn B', '1988-01-10', '1', '402/33 Xô Viết Nghệ TĨnh', 'tx_vanhoang', 'd41d8cd98f00b204e9800998ecf8427e', 'vanhoang@gmail.com', 'TX', 'HCMC', '123321', '0909321123', '2021-01-15', '2021-01-15 00:31:56', '2021-01-15 00:32:14'),
(6, 'Phan Anh Lộc', '1995-09-11', '0', '506/19/15 3/2 street', 'qldv_anhloc', '96e79218965eb72c92a549dd5a330112', 'anhminh@email.com', 'QLDV', 'Ho Chi Minh City', NULL, '0987435212', '2021-01-15', '2021-01-15 00:31:59', '2021-01-15 00:32:18'),
(7, 'Trần Hoàng Anh', '1992-04-20', '1', 'Linh Trung, Thủ Đức', 'qldv_hoanganh', 'd41d8cd98f00b204e9800998ecf8427e', 'tranhoanganh@gmail.com', 'QLDV', 'Hồ Chí Minh', NULL, '0987788778', '2021-01-15', '2021-01-15 00:32:02', '2021-01-15 00:32:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_food`
--

CREATE TABLE `list_food` (
  `Mã` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `quatity` int(11) DEFAULT NULL,
  `id_ticket` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `list_food`
--

INSERT INTO `list_food` (`Mã`, `id_menu`, `quatity`, `id_ticket`) VALUES
(64, 1, 1, '20210627113252'),
(65, 6, 2, '20210627135200'),
(66, 1, 1, '20210627140043'),
(67, 6, 1, '20210627142631'),
(68, 1, 2, '20210702145040'),
(69, 1, 2, '20210702164556'),
(70, 1, 1, '20210702173909'),
(71, 1, 1, '20210702214910'),
(72, 1, 2, '20210702220619'),
(73, 1, 1, '20210702222626'),
(74, 1, 2, '20210702222755'),
(75, 1, 1, '20210702223154'),
(76, 1, 1, '20210703180333'),
(77, 1, 1, '20210703180335'),
(78, 1, 2, '20210704181740'),
(79, 1, 1, '20210704182913'),
(80, 1, 1, '20210704191720'),
(81, 1, 1, '20210704194315'),
(82, 1, 1, '20210704194443'),
(83, 1, 2, '20210704194559'),
(84, 1, 2, '20210704195902'),
(85, 1, 2, '20210704200002'),
(86, 1, 1, '20210704200122'),
(87, 1, 2, '20210704200309'),
(88, 1, 2, '20210704200607'),
(89, 1, 1, '20210704201214'),
(90, 1, 1, '20210704201331'),
(91, 1, 1, '20210704201331'),
(92, 3, 1, '20210710180905'),
(93, 4, 1, '20210710181753'),
(94, 2, 1, '20210724101918');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_food`
--

CREATE TABLE `menu_food` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `menu_food`
--

INSERT INTO `menu_food` (`id`, `name`, `price`, `image`, `status`) VALUES
(1, 'Cơm gà', 30000, 'HClNWV4r.jpg', 1),
(2, 'Bún bò huế', 35000, 'dsU12xQE.jpg', 1),
(3, 'Phở bò', 35000, 'iT5eQ8Xb.jpg', 1),
(4, 'Cơm chay', 20000, 'ohfKY4Ar.jpg', 1),
(5, 'Cháo sườn', 20000, '4JLVxETt.jpg', 1),
(6, 'Coca cola', 10000, 'cvNqYGr8.jpg', 1),
(7, 'Nước cam', 10000, 'OXpXMLsq.jpg', 1),
(8, 'Revive', 10000, 'z4YChs3F.jpg', 1),
(9, 'Bia tiger', 25000, 'Xg2bwXnp.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id_news` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL,
  `introduce` text CHARACTER SET utf8 NOT NULL,
  `content` longtext CHARACTER SET utf8 NOT NULL,
  `check_slide` int(11) NOT NULL,
  `id_admin_created` int(10) NOT NULL,
  `id_admin_changed` int(10) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id_news`, `title`, `image`, `introduce`, `content`, `check_slide`, `id_admin_created`, `id_admin_changed`, `is_disabled`, `created_at`, `updated_at`) VALUES
(1, 'Mã Vũng Mới Cho Các Tỉnh Thành Trong 3 Đợt Thay Đổi Năm 2017', '1543856022-mavung.jpg', 'Bắt đầu từ 0h ngày 11/2/2017, 13 tỉnh thành đầu tiên trên cả nước sẽ tiến hành chuyển đổi sang mã vùng điện thoại cố định mới giai đoạn 1. Thời gian quay số song song từ 11/2/2017 đến 12/3/2017. Thời gian duy trì âm báo từ 13/3/2017 đến 14/4/2017.', '<p>C&aacute;c tỉnh n&agrave;y gồm Sơn La, Lai Ch&acirc;u, L&agrave;o Cai, Điện Bi&ecirc;n, Y&ecirc;n B&aacute;i, Quảng B&igrave;nh, Quảng Trị, Thừa Thi&ecirc;n - Huế, Quảng Nam, Đ&agrave; Nẵng, Thanh Ho&aacute;, Nghệ An, H&agrave; Tĩnh. C&aacute;c m&atilde; v&ugrave;ng mới được quy định cụ thể như sau:</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/mavung.jpg\" style=\"height:410px; width:850px\" /></p>\r\n\r\n<p>Giai đoạn 2 bắt đầu chuyển đổi từ 0h ng&agrave;y 15/04/2017, &aacute;p dụng cho 23 tỉnh, th&agrave;nh phố. Thời gian quay số song song từ 15/4/2017 đến 14/5/2017. Thời gian duy tr&igrave; &acirc;m b&aacute;o từ 15/5/2017 đến 16/6/2017. C&aacute;c m&atilde; v&ugrave;ng mới của 23 tỉnh th&agrave;nh chuyển đổi giai đoạn 2 được quy định cụ thể như sau:</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/mavung2.jpg\" style=\"height:613px; width:850px\" />Giai đoạn 3 bắt đầu từ 0h ng&agrave;y 17/06/2017 &aacute;p dụng cho 23 tỉnh, th&agrave;nh phố cuối c&ugrave;ng. Thời gian quay số song song từ 17/6/2017 đến 16/7/2017. Thời gian duy tr&igrave; &acirc;m b&aacute;o từ 17/7/2017 đến 31/8/2017.&nbsp;C&aacute;c m&atilde; v&ugrave;ng mới của 23 tỉnh th&agrave;nh chuyển đổi giai đoạn 3&nbsp;được quy định cụ thể như sau:</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/mavung3.jpg\" style=\"height:617px; width:850px\" /></p>\r\n\r\n<p>Ri&ecirc;ng 4 tỉnh Vĩnh Ph&uacute;c, Ph&uacute; Thọ, H&ograve;a B&igrave;nh v&agrave; H&agrave; Giang được giữ nguy&ecirc;n m&atilde; v&ugrave;ng.</p>\r\n\r\n<p><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (Theo&nbsp;http://vietnamnet.vn)</em></p>\r\n\r\n<p>&nbsp;</p>', 0, 1, 1, 0, '2018-11-15 09:10:29', '2018-12-04 05:12:45'),
(3, 'Chương Trình Tặng Vé Tết Đinh Dậu 2017 Cho Sinh Viên', 'vetet.jpg', 'Sáng 18/01, tại Bến xe Cầu Rào, Thành đoàn – Hội Sinh viên Việt Nam thành phố phối hợp với Sở Giao thông vận tải Hải Phòng tổ chức chương trình “Hỗ trợ sinh viên học tập tại Hải Phòng về quê đón Tết Nguyên đán 2017”.', '<p>Tới dự chương tr&igrave;nh c&oacute; c&aacute;c đồng ch&iacute;: Trần Quang Tường - Ủy vi&ecirc;n Ban Chấp h&agrave;nh Trung ương Đo&agrave;n, Th&agrave;nh ủy vi&ecirc;n, B&iacute; thư Th&agrave;nh đo&agrave;n; Nguyễn B&igrave;nh Minh - Ủy vi&ecirc;n Ban Chấp h&agrave;nh Trung ương Đo&agrave;n, Ph&oacute; Trưởng Ban Thanh ni&ecirc;n Trường học Trung ương Đo&agrave;n; B&ugrave;i Thị Ngọc - Ph&oacute; B&iacute; thư Thường trực Th&agrave;nh đo&agrave;n; Ủy vi&ecirc;n Ban Thư k&yacute; Trung ương Hội Sinh vi&ecirc;n Việt Nam; Phạm Văn Huy - Ph&oacute; Gi&aacute;m đốc Sở Giao th&ocirc;ng vận tải Hải Ph&ograve;ng.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/vetet.jpg\" style=\"height:735px; width:980px\" />Tại Chương tr&igrave;nh, Th&agrave;nh đo&agrave;n - Sở Giao th&ocirc;ng vận tải - Hội Sinh vi&ecirc;n Việt Nam th&agrave;nh phố v&agrave; c&aacute;c doanh nghiệp vận tải Ho&agrave;ng Long, Hải &Acirc;u ...&nbsp;đ&atilde; trao tặng hơn 690 v&eacute; xe miễn ph&iacute; cho sinh vi&ecirc;n về 27 tỉnh, th&agrave;nh phố. Ngo&agrave;i những tấm v&eacute; nghĩa t&igrave;nh, hơn 170 bạn sinh vi&ecirc;n c&oacute; ho&agrave;n cảnh kh&oacute; khăn c&ograve;n được nhận những suất qu&agrave; Tết, những phần qu&agrave; &yacute; nghĩa gi&uacute;p c&aacute;c bạn trở về b&ecirc;n gia đ&igrave;nh trong những ng&agrave;y xu&acirc;n th&ecirc;m đầm ấm.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; năm thứ 5 chương tr&igrave;nh được tổ chức nhằm hỗ trợ v&agrave; tạo điều kiện cho sinh vi&ecirc;n c&oacute; ho&agrave;n cảnh kh&oacute; khăn đang theo học tại c&aacute;c trường Đại học, Cao đẳng tr&ecirc;n địa b&agrave;n th&agrave;nh phố Hải Ph&ograve;ng về qu&ecirc; sum họp c&ugrave;ng gia đ&igrave;nh trong dịp Tết Nguy&ecirc;n đ&aacute;n. Qua đ&oacute;, thể hiện sự quan t&acirc;m, chia sẻ của x&atilde; hội, đ&atilde; tạo hiệu ứng t&iacute;ch cực v&agrave; lan tỏa trong sinh vi&ecirc;n v&agrave; cộng đồng. Tổng gi&aacute; trị của đợt hỗ trợ dịp Tết Nguy&ecirc;n đ&aacute;n Đinh Dậu 2017 cho sinh vi&ecirc;n l&agrave; gần 200 triệu đồng.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/vetet2.jpg\" style=\"height:735px; width:980px\" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<em>(Theo http://thanhdoanhaiphong.gov.vn)</em></p>', 0, 1, 1, 0, '2018-11-15 10:12:50', '2018-11-15 10:12:50'),
(5, 'Chuyển Tuyến Hải Phòng ↔ Hồ Chí Minh, Vũng Tàu Về Bến Thượng Lý - Hải Phòng Từ 22/11/2017', 'tintuc3.jpg', 'Thông báo chuyển về BẾN XE THƯỢNG LÝ-HẢI PHÒNG từ 22/11/2017\r\nBẾN XE THƯỢNG LÝ: SỐ 52 ĐƯỜNG HÀ NỘI (SỞ DẦU) - HỒNG BÀNG.', '<p>Xe kh&aacute;ch Ho&agrave;ng Long k&iacute;nh b&aacute;o tới qu&yacute; kh&aacute;ch h&agrave;ng chuyển bến xe từ 22/11/2017 :<br />\r\n- Theo sự động vi&ecirc;n, định hướng từ Ủy Ban nh&acirc;n d&acirc;n Th&agrave;nh phố v&agrave; quy hoạch của Sở giao th&ocirc;ng vận tải Hải Ph&ograve;ng.</p>\r\n\r\n<p>- Kể từ 22/11/2017, Xe kh&aacute;ch Ho&agrave;ng Long chuyển to&agrave;n bộ tuyến đi c&aacute;c tỉnh miền trung, miền nam từ bến xe Niệm Nghĩa về BẾN XE THƯỢNG L&Yacute; :</p>\r\n\r\n<p><strong>Hải Ph&ograve;ng &harr;&nbsp;HỒ CH&Iacute; MINH<br />\r\nHải Ph&ograve;ng &harr; VŨNG T&Agrave;U&nbsp;<br />\r\nHải Ph&ograve;ng &harr; Đ&Agrave; NẴNG</strong></p>\r\n\r\n<p>C&aacute;c tuyến đi tới c&aacute;c tỉnh, th&agrave;nh phố tr&ecirc;n to&agrave;n Quốc như:&nbsp;<br />\r\n<em><strong>Ninh B&igrave;nh &rarr; Thanh H&oacute;a &rarr; Vinh, Nghệ An&rarr; H&agrave; Tĩnh &rarr; Đồng Hới, Quảng B&igrave;nh &rarr; Quảng Trị&nbsp;&rarr; Huế &rarr; Quảng Nam &rarr; Quảng Ng&atilde;i &rarr; B&igrave;nh Đ&igrave;nh, Quy Nhơn, Ph&uacute; Y&ecirc;n &rarr; Nha Trang, Cam Ranh,Kh&aacute;nh H&ograve;a &rarr; Phan Rang, Phan Thiết, B&igrave;nh Thuận &rarr; Đồng Nai &hellip; cũng sẽ xuất ph&aacute;t tại Bến xe Thượng L&yacute; v&agrave; ngược lại.&nbsp;</strong></em><br />\r\n- Ho&agrave;ng Long hiện đ&atilde; mở th&ecirc;m văn ph&ograve;ng chuyển ph&aacute;t nhanh h&agrave;ng h&oacute;a, b&aacute;n v&eacute; c&aacute;c tuyến ngắn v&agrave; tuyến miền trung, miền nam tại vị tr&iacute;&nbsp;<strong>cổng ch&iacute;nh Bến xe Thượng L&yacute;</strong>.<br />\r\n- Mọi hoạt động kh&aacute;c của xe kh&aacute;ch Ho&agrave;ng Long tr&ecirc;n địa b&agrave;n Hải Ph&ograve;ng kh&ocirc;ng thay đổi.&nbsp;<br />\r\n- Tuyến Hải Ph&ograve;ng- H&agrave; Nội vẫn hoạt động b&igrave;nh thường tại bến xe Niệm Nghĩa.</p>\r\n\r\n<p>- Mọi chi tiết xin li&ecirc;n hệ tổng đ&agrave;i<strong>&nbsp;02253.920.920&nbsp;</strong>24/7 Ho&agrave;ng Long sẽ tư vấn trực tiếp đến qu&yacute; kh&aacute;ch.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/tintuc3.jpg\" style=\"height:638px; width:960px\" /></p>', 0, 1, 1, 0, '2018-11-15 10:17:40', '2018-11-15 10:17:40'),
(6, '21321321', '5.jpg', '21321313', '<p>213213231sadas</p>', 0, 1, 1, 0, '2018-11-27 22:07:29', '2018-11-27 22:07:29'),
(7, 'asdsadsa', '9.jpg', 'sadsasa', '<p>sadsdsadas</p>', 0, 1, 1, 0, '2018-11-27 22:07:38', '2018-11-27 22:07:38'),
(8, 'saddasadsasdaas', '8.jpg', 'asdsasda', '<p>sdasasasadsa</p>', 0, 1, 1, 0, '2018-11-27 22:07:46', '2018-11-27 22:07:46'),
(9, 'sadsasadsa', '5.jpg', 'dsaadsadsa', '<p>sadsadsadsa</p>', 0, 1, 1, 0, '2018-11-27 22:07:55', '2018-11-27 22:07:55'),
(10, 'dsadsasa', '6.jpg', 'sadsadsadsa', '<p>dsadsadsadsadsad</p>', 0, 1, 1, 0, '2018-11-27 22:08:03', '2018-11-27 22:08:03'),
(11, 'sdsadsadsa', '1.jpg', 'sadadsadsa', '<p>dssadsadsadsadsadsadsad</p>', 0, 1, 1, 0, '2018-11-27 22:08:13', '2018-11-27 22:08:13'),
(12, 'dsaddddddddddddddddd', '16.jpg', 'asdsasa', '<p>dsadsasasa</p>', 0, 1, 1, 0, '2018-11-27 22:08:21', '2018-11-27 22:08:21'),
(13, 'adssadassa', '20.jpg', 'adssasadsadq123213', '<p>asdsadsadsda</p>', 0, 1, 1, 0, '2018-11-27 22:08:29', '2018-11-27 22:08:29'),
(14, 'dasdasdsa', '5.jpg', 'adssadsas', '<p>sdsadsadsad</p>', 0, 1, 1, 0, '2018-11-27 22:08:36', '2018-11-27 22:08:36'),
(15, 'sadsadadddddddddddd', '7.jpg', 'sdsadsadsa', '<p>sdsadsadsadsa</p>', 0, 1, 1, 0, '2018-11-27 22:08:44', '2018-11-27 22:08:44'),
(16, 'test', '5.jpg', '2321321313dsadadsa', '<p>sdsadsadsadsa</p>', 0, 1, 1, 0, '2018-11-29 21:14:13', '2018-11-29 21:14:13'),
(18, 'Vẻ đẹp đặc biệt của thành phố Hồ Chí Minh được hãng tin BBC hết lời khen ngợi', 'anh ho chi minh.jpg', 'Mới đây, hãng tin BBC đã đăng tải một bài viết giới thiệu về vẻ đẹp đặc biệt của Sài Gòn – một thành phố đáng để đặt chân tới với những trải nghiệm đầy thú vị cùng sự thân thiện từ con người nơi đây.', '<p>Lần đầu đặt ch&acirc;n tới th&agrave;nh phố Hồ Ch&iacute; Minh, hay c&ograve;n được gọi với c&aacute;i t&ecirc;n th&acirc;n mật kh&aacute;c l&agrave; S&agrave;i G&ograve;n, c&oacute; lẽ nhiều du kh&aacute;ch sẽ cảm thấy kh&aacute; sốc trước t&igrave;nh trạng &quot;hỗn loạn&quot; tại khu vực sầm uất nhất Việt Nam.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm1.jpg\" style=\"height:263px; width:600px\" /></p>\r\n\r\n<p>&quot;Những đ&aacute;m đ&ocirc;ng v&agrave; d&ograve;ng xe cộ hỗn loạn c&oacute; thể khiến cho nhiều du kh&aacute;ch cảm thấy nản l&ograve;ng v&agrave; muốn t&igrave;m tới nơi kh&aacute;c ngay lập tức. Tuy nhi&ecirc;n, đ&oacute; l&agrave; một sai lầm cực k&igrave; lớn.&quot;, anh James Clark, một người Australia sống tại S&agrave;i G&ograve;n từ năm 2012 chia sẻ.</p>\r\n\r\n<p>Mặc d&ugrave; giao th&ocirc;ng ở đ&acirc;y c&oacute; phần đi&ecirc;n rồ, song nếu c&oacute; cơ hội vi vu khắp c&aacute;c con phố nhỏ tr&ecirc;n một chiếc xe m&aacute;y đời cũ n&agrave;o đ&oacute; th&igrave; bạn sẽ thấy mọi chuyện kh&aacute;c xa so với tưởng tượng ban đầu.</p>\r\n\r\n<p>Đa phần c&aacute;c phương tiện đều di chuyển với tốc độ rất chậm, v&agrave; hiếm khi thấy vụ ẩu đả n&agrave;o xảy ra giữa đường lớn.</p>\r\n\r\n<p>B&ecirc;n cạnh sự đ&ocirc;ng đ&uacute;c vốn c&oacute;, S&agrave;i G&ograve;n c&ograve;n mang trong m&igrave;nh n&eacute;t dung dị rất &quot;đời&quot; khiến con người ta chợt cảm thấy y&ecirc;u v&agrave; thấy nhớ mỗi khi phải chia xa.&nbsp;Tất nhi&ecirc;n, bạn cần phải c&oacute; kha kh&aacute; thời gian để hiểu, hay đơn giản hơn l&agrave; để th&iacute;ch với th&agrave;nh phố n&agrave;y.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm2.jpg\" style=\"height:380px; width:600px\" /></p>\r\n\r\n<p>Anh Baker &ndash; một người Anh định cư ở S&agrave;i G&ograve;n từng n&oacute;i: &quot;Bạn c&oacute; thể kh&aacute;m ph&aacute; được một th&agrave;nh phố với c&aacute; t&iacute;nh ri&ecirc;ng, t&iacute;nh c&aacute;ch ri&ecirc;ng c&ugrave;ng kh&ocirc;ng gian ẩm thực với mức gi&aacute;&nbsp;rẻ nhất h&agrave;nh tinh nếu chịu d&agrave;nh thời gian cho n&oacute;.&quot;</p>\r\n\r\n<p>Ngo&agrave;i ra, con người nơi đ&acirc;y cũng dễ thương lắm. Họ chẳng hề tỏ ra ki&ecirc;u k&igrave; hay kh&oacute; gần như bạn vẫn nghĩ, m&agrave; ngược lại, t&iacute;nh c&aacute;ch th&acirc;n thiện xuất ph&aacute;t từ tấm l&ograve;ng nhiệt th&agrave;nh sẽ khiến du kh&aacute;ch kh&ocirc;ng t&agrave;i n&agrave;o qu&ecirc;n được.</p>\r\n\r\n<p>&quot;C&oacute; dịp v&ograve;ng vo mọi ng&otilde; ng&aacute;ch b&eacute; x&iacute;u dưới c&aacute;i nắng ngọt ng&agrave;o của miền Nam, bạn sẽ thấy lối sống của người d&acirc;n bản xứ tương đối b&igrave;nh thản.&quot;, c&ocirc; Kelsey Cheng (Chicago, Mỹ) cho biết.</p>\r\n\r\n<p>Cũng theo anh Barker, người d&acirc;n S&agrave;i G&ograve;n thường rất thẳng thắn v&agrave; trực tiếp. Đ&acirc;y l&agrave; một n&eacute;t t&iacute;nh c&aacute;ch kh&aacute; kh&aacute;c biệt nếu so s&aacute;nh với sự th&acirc;n thiện thường thấy tại nhiều đất nước kh&aacute;c, v&iacute; dụ như Th&aacute;i Lan chẳng hạn.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm3.jpeg\" style=\"height:346px; width:600px\" /></p>\r\n\r\n<p>Lần đầu đặt ch&acirc;n tới th&agrave;nh phố Hồ Ch&iacute; Minh, hay c&ograve;n được gọi với c&aacute;i t&ecirc;n th&acirc;n mật kh&aacute;c l&agrave; S&agrave;i G&ograve;n, c&oacute; lẽ nhiều du kh&aacute;ch sẽ cảm thấy kh&aacute; sốc trước t&igrave;nh trạng &quot;hỗn loạn&quot; tại khu vực sầm uất nhất Việt Nam.</p>\r\n\r\n<p><a href=\"https://kenh14cdn.com/2017/1-1490678261402.jpg\" target=\"_blank\"><img alt=\"Vẻ đẹp đặc biệt của thành phố Hồ Chí Minh được hãng tin BBC hết lời khen ngợi - Ảnh 1.\" src=\"https://kenh14cdn.com/2017/1-1490678261402.jpg\" /></a></p>\r\n\r\n<p>Th&agrave;nh phố Hồ Ch&iacute; Minh l&agrave; điểm đến l&yacute; tưởng của rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước.</p>\r\n\r\n<p>&quot;Những đ&aacute;m đ&ocirc;ng v&agrave; d&ograve;ng xe cộ hỗn loạn c&oacute; thể khiến cho nhiều du kh&aacute;ch cảm thấy nản l&ograve;ng v&agrave; muốn t&igrave;m tới nơi kh&aacute;c ngay lập tức. Tuy nhi&ecirc;n, đ&oacute; l&agrave; một sai lầm cực k&igrave; lớn.&quot;, anh James Clark, một người Australia sống tại S&agrave;i G&ograve;n từ năm 2012 chia sẻ.</p>\r\n\r\n<p>Mặc d&ugrave; giao th&ocirc;ng ở đ&acirc;y c&oacute; phần đi&ecirc;n rồ, song nếu c&oacute; cơ hội vi vu khắp c&aacute;c con phố nhỏ tr&ecirc;n một chiếc xe m&aacute;y đời cũ n&agrave;o đ&oacute; th&igrave; bạn sẽ thấy mọi chuyện kh&aacute;c xa so với tưởng tượng ban đầu.</p>\r\n\r\n<p>Đa phần c&aacute;c phương tiện đều di chuyển với tốc độ rất chậm, v&agrave; hiếm khi thấy vụ ẩu đả n&agrave;o xảy ra giữa đường lớn.</p>\r\n\r\n<p>B&ecirc;n cạnh sự đ&ocirc;ng đ&uacute;c vốn c&oacute;, S&agrave;i G&ograve;n c&ograve;n mang trong m&igrave;nh n&eacute;t dung dị rất &quot;đời&quot; khiến con người ta chợt cảm thấy y&ecirc;u v&agrave; thấy nhớ mỗi khi phải chia xa.&nbsp;Tất nhi&ecirc;n, bạn cần phải c&oacute; kha kh&aacute; thời gian để hiểu, hay đơn giản hơn l&agrave; để th&iacute;ch với th&agrave;nh phố n&agrave;y.</p>\r\n\r\n<p><a href=\"https://kenh14cdn.com/2017/2-1490678261403.jpg\" target=\"_blank\"><img alt=\"Vẻ đẹp đặc biệt của thành phố Hồ Chí Minh được hãng tin BBC hết lời khen ngợi - Ảnh 2.\" src=\"https://kenh14cdn.com/2017/2-1490678261403.jpg\" /></a></p>\r\n\r\n<p>Tại nhiều thời điểm, đường phố nơi đ&acirc;y bỗng trở n&ecirc;n b&igrave;nh y&ecirc;n đến lạ.</p>\r\n\r\n<p>Anh Baker &ndash; một người Anh định cư ở S&agrave;i G&ograve;n từng n&oacute;i: &quot;Bạn c&oacute; thể kh&aacute;m ph&aacute; được một th&agrave;nh phố với c&aacute; t&iacute;nh ri&ecirc;ng, t&iacute;nh c&aacute;ch ri&ecirc;ng c&ugrave;ng kh&ocirc;ng gian ẩm thực với mức gi&aacute;&nbsp;rẻ nhất h&agrave;nh tinh nếu chịu d&agrave;nh thời gian cho n&oacute;.&quot;</p>\r\n\r\n<p>Ngo&agrave;i ra, con người nơi đ&acirc;y cũng dễ thương lắm. Họ chẳng hề tỏ ra ki&ecirc;u k&igrave; hay kh&oacute; gần như bạn vẫn nghĩ, m&agrave; ngược lại, t&iacute;nh c&aacute;ch th&acirc;n thiện xuất ph&aacute;t từ tấm l&ograve;ng nhiệt th&agrave;nh sẽ khiến du kh&aacute;ch kh&ocirc;ng t&agrave;i n&agrave;o qu&ecirc;n được.</p>\r\n\r\n<p>&quot;C&oacute; dịp v&ograve;ng vo mọi ng&otilde; ng&aacute;ch b&eacute; x&iacute;u dưới c&aacute;i nắng ngọt ng&agrave;o của miền Nam, bạn sẽ thấy lối sống của người d&acirc;n bản xứ tương đối b&igrave;nh thản.&quot;, c&ocirc; Kelsey Cheng (Chicago, Mỹ) cho biết.</p>\r\n\r\n<p>Cũng theo anh Barker, người d&acirc;n S&agrave;i G&ograve;n thường rất thẳng thắn v&agrave; trực tiếp. Đ&acirc;y l&agrave; một n&eacute;t t&iacute;nh c&aacute;ch kh&aacute; kh&aacute;c biệt nếu so s&aacute;nh với sự th&acirc;n thiện thường thấy tại nhiều đất nước kh&aacute;c, v&iacute; dụ như Th&aacute;i Lan chẳng hạn.</p>\r\n\r\n<p><a href=\"https://kenh14cdn.com/2017/3-1490678261405.jpeg\" target=\"_blank\"><img alt=\"Vẻ đẹp đặc biệt của thành phố Hồ Chí Minh được hãng tin BBC hết lời khen ngợi - Ảnh 3.\" src=\"https://kenh14cdn.com/2017/3-1490678261405.jpeg\" /></a></p>\r\n\r\n<p>S&agrave;i G&ograve;n về đ&ecirc;m với &aacute;nh đ&egrave;n lung linh hắt ra từ những t&ograve;a nh&agrave; cao ốc hiện đại.</p>\r\n\r\n<p>Mặc d&ugrave; một số &iacute;t sẽ cảm thấy h&agrave;nh động đ&oacute; kh&aacute; v&ocirc; duy&ecirc;n, song những người d&acirc;n đang sinh sống tại đ&acirc;y lại nghĩ rằng: &quot;Nhờ vậy m&agrave; ch&uacute;ng tui c&oacute; thể dễ d&agrave;ng hiểu được người kh&aacute;c muốn g&igrave; hay kh&ocirc;ng muốn g&igrave;.&quot;</p>\r\n\r\n<p>Nếu giải th&iacute;ch theo c&aacute;ch đơn giản th&igrave; ở H&agrave; Nội, đ&ocirc;i khi người ta n&oacute;i &quot;c&oacute;&quot; nhưng thực chất lại l&agrave; &quot;kh&ocirc;ng&quot;, c&ograve;n tại th&agrave;nh phố Hồ Ch&iacute; Minh th&igrave; &quot;c&oacute;&quot; l&agrave; &quot;c&oacute;&quot; m&agrave; &quot;kh&ocirc;ng&quot; chắc chắn sẽ l&agrave; &quot;kh&ocirc;ng&quot;.</p>\r\n\r\n<p>Những người nước ngo&agrave;i sống l&acirc;u năm ở đ&acirc;y cũng khẳng định S&agrave;i G&ograve;n l&agrave; một th&agrave;nh phố v&ocirc; c&ugrave;ng năng động, đặc biệt l&agrave; thế hệ trẻ lu&ocirc;n tr&agrave;n đầy ước mơ v&agrave; ho&agrave;i b&atilde;o m&atilde;nh liệt.</p>\r\n\r\n<p>&quot;Những người trẻ c&oacute; vẻ muốn trở th&agrave;nh doanh nh&acirc;n hơn l&agrave; đi l&agrave;m nh&agrave; nước. Họ đều mang b&ecirc;n m&igrave;nh chiếc smartphone v&agrave; đi lại vội v&atilde; tr&ecirc;n đường.&quot;, c&ocirc; Consul Alan Murray - một người Anh đ&atilde; sống tại th&agrave;nh phố Hồ Ch&iacute; Minh hơn 10 năm chia sẻ.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm4.jpg\" style=\"height:354px; width:600px\" /></p>\r\n\r\n<p>Mặc d&ugrave; sống nhanh v&agrave; c&oacute; phần hối hả, thế nhưng cư d&acirc;n tại đ&acirc;y lại v&ocirc; c&ugrave;ng nhiệt th&agrave;nh v&agrave; lu&ocirc;n sẵn s&agrave;ng dừng lại để gi&uacute;p đỡ c&aacute;c ho&agrave;n cảnh kh&oacute; khăn trong cuộc sống.</p>\r\n\r\n<p>&quot;Trong những ng&agrave;y đầu ti&ecirc;n tới đ&acirc;y, t&ocirc;i đ&atilde; bị lạc tại Quận 3. Khi đ&oacute;, t&ocirc;i t&igrave;m tới một nơi ph&aacute;t wifi miễn ph&iacute; để gọi Grab Bike nhưng lại kh&ocirc;ng thể n&agrave;o giao tiếp được với t&agrave;i xế. Hơi hoảng, t&ocirc;i liền đưa chiếc điện thoại của m&igrave;nh cho một người đ&agrave;n &ocirc;ng đứng b&ecirc;n cạnh v&agrave; &ocirc;ng ấy đ&atilde; gi&uacute;p t&ocirc;i n&oacute;i chuyện.</p>\r\n\r\n<p>T&ocirc;i cứ nghĩ người d&acirc;n v&ugrave;ng Midwest (một trong bốn v&ugrave;ng lớn tại Mỹ) l&agrave; th&acirc;n thiện nhất rồi, n&agrave;o ngờ người Việt Nam lại c&agrave;ng th&acirc;n thiện hơn nữa.&quot;, c&ocirc; Cheng kể lại.</p>\r\n\r\n<p>C&ograve;n anh An Dương - trưởng ph&ograve;ng c&ocirc;ng nghệ của c&ocirc;ng ty khởi nghiệp cung cấp dịch vụ t&igrave;m kiếm th&ocirc;ng tin du lịch TourMega v&agrave; l&agrave; một người d&acirc;n bản địa cũng đồng &yacute; với lời nhận x&eacute;t tr&ecirc;n.</p>\r\n\r\n<p>&quot;Người S&agrave;i G&ograve;n thường sẵn s&agrave;ng cho đi m&agrave; kh&ocirc;ng cần nhận lại bất cứ điều g&igrave;. Bạn sẽ thấy v&ocirc; số b&igrave;nh tr&agrave; đ&aacute; miễn ph&iacute; đặt tại nhiều con đường lớn nhỏ để gi&uacute;p đỡ những người b&aacute;n h&agrave;ng rong đang vất vả mưu sinh ngo&agrave;i cuộc sống.&nbsp;Ai nấy đều sẵn l&ograve;ng hỗ trợ người kh&aacute;c một c&aacute;ch nhiệt t&igrave;nh như với người th&acirc;n của m&igrave;nh vậy&quot;.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm5.jpg\" style=\"height:395px; width:600px\" /></p>\r\n\r\n<p>C&aacute;ch tốt nhất để thăm th&uacute; th&agrave;nh phố Hồ Ch&iacute; Minh l&agrave; vi vu tr&ecirc;n một chiếc xe m&aacute;y. Sử dụng phương tiện n&agrave;y, bạn c&oacute; thể tạm thời dừng lại v&agrave; gửi xe một c&aacute;ch dễ d&agrave;ng, từ đ&oacute; c&oacute; thể kh&aacute;m ph&aacute; những địa điểm vui chơi th&uacute; vị cũng như thưởng thức c&aacute;c qu&aacute;n ăn hấp dẫn b&ecirc;n lề đường.</p>\r\n\r\n<p>Anh Barker cho biết, một m&oacute;n ăn m&agrave; du kh&aacute;ch nước ngo&agrave;i kh&ocirc;ng n&ecirc;n bỏ qua khi tới đ&acirc;y l&agrave; m&oacute;n &quot;b&uacute;n thịt nướng&quot;, hay c&ograve;n gọi l&agrave; &quot;b&uacute;n chả&quot; theo c&aacute;ch n&oacute;i của những địa phương ở ph&iacute;a Bắc.</p>\r\n\r\n<p>&quot;Bạn c&oacute; thể t&igrave;m thấy những qu&aacute;n ăn đường phố tại mọi g&oacute;c đường, mọi con phố tr&ecirc;n khắp S&agrave;i G&ograve;n n&agrave;y. Việc bạn cần l&agrave;m ch&iacute;nh l&agrave; k&eacute;o một chiếc ghế nhựa về ph&iacute;a m&igrave;nh, ngồi xuống v&agrave; thưởng thức hương vị hấp dẫn từ b&aacute;t hủ tiếu g&otilde;, t&agrave;u hũ n&oacute;ng hay c&agrave; ph&ecirc; sữa đ&aacute;. Chỉ đơn giản c&oacute; thế m&agrave; th&ocirc;i&quot; .</p>\r\n\r\n<p><strong>Những địa điểm nhất định phải đến khi gh&eacute; thăm S&agrave;i G&ograve;n</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm6.jpg\" style=\"height:401px; width:600px\" /></strong></p>\r\n\r\n<p>To&agrave;n bộ th&agrave;nh phố Hồ Ch&iacute; Minh được chia l&agrave;m 24 quận. Đa phần người nước ngo&agrave;i đều bắt đầu định cư tại Quận 1 &ndash; nơi được mệnh danh l&agrave; &quot;thi&ecirc;n đường&quot; mua sắm với những khu chợ nổi tiếng như Bến Th&agrave;nh hay c&aacute;c con phố lớn với h&agrave;ng loạt hoạt động sầm uất về đ&ecirc;m.</p>\r\n\r\n<p>Cứ thử một lần gh&eacute; tới đường B&ugrave;i Viện hoặc Phạm Ngũ L&atilde;o sau 10 giờ tối m&agrave; xem, bạn sẽ tha hồ được tụ tập với du kh&aacute;ch bốn phương v&agrave; giao lưu đến tận l&uacute;c trời s&aacute;ng trong h&agrave;ng loạt qu&aacute;n pub, qu&aacute;n c&agrave; ph&ecirc;, qu&aacute;n bia tươi lề đường với &aacute;nh đ&egrave;n s&aacute;ng rực.</p>\r\n\r\n<p>Anh Baker chia sẻ: &quot;Khu vực Đa Kao thuộc Quận 1 l&agrave; nơi ưa th&iacute;ch nhất của t&ocirc;i. Nơi n&agrave;y c&oacute; rất nhiều cửa h&agrave;ng ẩm thực để thưởng thức, v&agrave; từ đ&acirc;y c&oacute; thể đi tới to&agrave;n bộ c&aacute;c địa điểm th&uacute; vị kh&aacute;c thuộc trung t&acirc;m th&agrave;nh phố.&quot;</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm7.png\" style=\"height:401px; width:600px\" /></p>\r\n\r\n<p>Khu vực Quận 2 gần s&ocirc;ng S&agrave;i G&ograve;n l&agrave; một v&ugrave;ng mới được ph&aacute;t triển với nhiều qu&aacute;n ăn, khu mua sắm v&agrave; cao ốc đắt tiền. Trong đ&oacute;, người nước ngo&agrave;i thường t&igrave;m tới phường Thảo Điền để tận hưởng cuộc sống tiện nghi nhất m&agrave; chẳng cần lặn lội tới c&aacute;c điểm vui chơi xa x&ocirc;i kh&aacute;c.</p>\r\n\r\n<p>Ngo&agrave;i ra, nếu muốn định cư l&acirc;u d&agrave;i hoặc c&oacute; con nhỏ th&igrave; họ lại lựa chọn Quận 7. Bởi đa phần những ng&ocirc;i trường quốc tế chất lượng cao c&ugrave;ng hệ thống nh&agrave; ở phong ph&uacute; đều tập trung hết ở khu vực n&agrave;y.</p>\r\n\r\n<p><strong>Từ S&agrave;i G&ograve;n, bạn c&oacute; thể vi vu c&aacute;c địa danh nổi tiếng trong v&ograve;ng &quot;một nốt nhạc&quot;</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm8.jpg\" style=\"height:257px; width:600px\" /></strong></p>\r\n\r\n<p>Những người muốn đi du lịch trong ng&agrave;y thường lựa chọn c&aacute;c b&atilde;i biển tuyệt đẹp tại Vũng T&agrave;u (khoảng 93km về ph&iacute;a Đ&ocirc;ng Nam) hay khu vực miền T&acirc;y (khoảng 200km về ph&iacute;a T&acirc;y Nam).</p>\r\n\r\n<p>Tuy nhi&ecirc;n, c&ugrave;ng với sự hiện đại của dịch vụ h&agrave;ng kh&ocirc;ng, du kh&aacute;ch cũng c&oacute; thể đi xa hơn nữa để thỏa sức ngắm nh&igrave;n những địa danh &quot;kh&ocirc;ng đến th&igrave; ph&iacute;&quot; ở Việt Nam.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm9.jpg\" style=\"height:337px; width:600px\" /></p>\r\n\r\n<p>Chỉ mất 1 giờ bay, bạn sẽ được đặt ch&acirc;n tới đảo ngọc Ph&uacute; Quốc &ndash; một trong những điểm hẹn hấp dẫn với b&atilde;i biển xanh biếc, khu rừng mưa nhiệt đới nguy&ecirc;n thủy c&ugrave;ng đời sống ẩm thực, &acirc;m nhạc v&agrave; lễ hội đang hết sức ph&aacute;t triển.</p>\r\n\r\n<p>Nếu muốn tr&aacute;nh c&aacute;i n&oacute;ng bỏng r&aacute;t của miền biển, du kh&aacute;ch cũng c&oacute; thể gh&eacute; thăm Đ&agrave; Lạt (khoảng 300km về ph&iacute;a Đ&ocirc;ng Bắc) - nơi được mệnh danh l&agrave; th&agrave;nh phố của m&ugrave;a xu&acirc;n vĩnh cửu với điều kiện thời tiết m&aacute;t mẻ quanh năm, hay tỉnh Đắk Lắk (khoảng 350km về ph&iacute;a Đ&ocirc;ng Bắc) với những th&aacute;c nước tuyệt đẹp, sản phẩm c&agrave; ph&ecirc; nổi tiếng v&agrave; cơ hội kh&aacute;m ph&aacute; sự đa dạng về văn h&oacute;a của hơn 40 tộc người kh&aacute;c nhau.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm11/hcm13.jpg\" style=\"height:378px; width:600px\" /></p>\r\n\r\n<p>Do đ&acirc;y l&agrave; một vị tr&iacute; chiến lược kh&aacute; quan trọng của to&agrave;n khu vực n&ecirc;n từ th&agrave;nh phố Hồ Ch&iacute; Minh, bạn c&oacute; thể dễ d&agrave;ng đ&aacute;p m&aacute;y bay tới những khu vực nổi tiếng kh&aacute;c tại Đ&ocirc;ng Nam &Aacute; như Bangkok, Kuala Lumpur hay Singapore với thời gian bay chưa đầy 2 giờ đồng hồ.</p>\r\n\r\n<p><strong>Mức gi&aacute; th&acirc;n thiện với t&uacute;i tiền của bạn</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm11/hcm11.jpg\" style=\"height:354px; width:600px\" /></strong></p>\r\n\r\n<p>Nếu so s&aacute;nh với những th&agrave;nh phố kh&aacute;c ở c&aacute;c nước phương T&acirc;y, cuộc sống tại th&agrave;nh phố Hồ Ch&iacute; Minh sẽ khiến bạn cảm thấy rất dễ chịu với mức gi&aacute; kh&ocirc;ng thể &quot;th&acirc;n thiện&quot; hơn.</p>\r\n\r\n<p>Đặc biệt, việc t&igrave;m kiếm địa điểm ăn uống b&ecirc;n ngo&agrave;i thực sự kh&aacute; dễ d&agrave;ng v&agrave; ph&ugrave; hợp với t&uacute;i tiền của số đ&ocirc;ng &ndash; chỉ chưa tới 80.000 đồng cho một bữa ăn thật ngon miệng.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/dalat/HCM/hcm11/hcm12.jpg\" style=\"height:330px; width:600px\" /></p>\r\n\r\n<p>Chi ph&iacute; thu&ecirc; nh&agrave; ở đ&acirc;y cũng rất rẻ, chỉ từ 6,8 triệu đồng mỗi th&aacute;ng l&agrave; bạn đ&atilde; c&oacute; thể sở hữu một căn hộ chung cư tiện nghi cho một người, rẻ hơn tới 85% so với c&aacute;c th&agrave;nh phố lớn kh&aacute;c tại Mỹ.</p>\r\n\r\n<p>&quot;Bạn c&oacute; thể ti&ecirc;u tiền theo nhiều c&aacute;ch kh&aacute;c nhau. Nhưng nếu chỉ thu&ecirc; một căn hộ b&igrave;nh thường v&agrave; ăn uống kh&ocirc;ng qu&aacute; phung ph&iacute; th&igrave; chỉ cần chưa tới 1.000 USD (khoảng 23 triệu đồng) l&agrave; đủ để sống thoải m&aacute;i ở đ&acirc;y rồi.&quot;, anh Clark khẳng định.</p>', 1, 1, 1, 0, '2018-11-30 18:19:45', '2018-11-30 18:19:45'),
(20, 'Quảng Ngãi - Thành Phố Anh Hùng', 'quang ngai.jpg', 'Là tỉnh ven biển nằm trong vùng kinh tế trọng điểm của miền Trung, Quảng Ngãi hội đủ nhiều yếu tố hấp dẫn để các nhà đầu tư an tâm khi đầu tư vào đây, đó là Quảng Ngãi nằm giữa hai đầu Bắc- Nam, vừa có cảng biển, vừa có đường quốc lộ, vừa có đường sắt Bắc-Nam, cùng với những chính sách ưu đãi đầu tư đặc biệt từ Chính phủ và của địa phương. Chính vì vậy Quảng Ngãi là địa điểm lý tưởng cho các công ty muốn mở rộng thị trường đến đến cả hai miền Nam - Bắc và khu vực miền Trung rộng lớn. Ngoài ra, từ Quảng Ngãi, các doanh nghiệp có thể mở rộng tiếp cận thị trường sang Lào, đông bắc Thái Lan và Campuchia', '<p><strong>Ng&agrave;y nay, Quảng Ng&atilde;i đ&atilde; vươn l&ecirc;n trở th&agrave;nh một trong 13 tỉnh c&oacute; đ&oacute;ng g&oacute;p ng&acirc;n s&aacute;ch lớn nhờ c&oacute; Nh&agrave; m&aacute;y Lọc dầu Dung Quất &ndash; nh&agrave; m&aacute;y lọc h&oacute;a dầu đầu ti&ecirc;n của Việt Nam v&agrave; Khu kinh tế (KKT) Dung Quất, một trong những KKT ti&ecirc;n phong v&agrave; th&agrave;nh c&ocirc;ng nhất trong cả nước, g&oacute;p phần lớn cho việc ph&aacute;t triển trong khu vực kinh tế trọng điểm miền Trung.</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/QuangNgai/qn1.jpg\" style=\"height:382px; width:650px\" /></strong></p>\r\n\r\n<p><strong>ĐIỀU KIỆN TỰ NHI&Ecirc;N V&Agrave; CƠ SỞ HẠ TẦNG</strong><br />\r\n<br />\r\n<strong>Địa l&yacute; h&agrave;nh ch&iacute;nh</strong></p>\r\n\r\n<p>Quảng Ng&atilde;i trải d&agrave;i từ 14&deg;32&rsquo; đến 15&deg;25&rsquo; vĩ độ Bắc, từ 108&deg;06&rsquo; đến 109&deg;04&rsquo; kinh độ Đ&ocirc;ng; ph&iacute;a bắc gi&aacute;p tỉnh Quảng Nam, ph&iacute;a nam gi&aacute;p tỉnh B&igrave;nh Định, ph&iacute;a t&acirc;y gi&aacute;p tỉnh Kon Tum, ph&iacute;a đ&ocirc;ng gi&aacute;p biển Đ&ocirc;ng, c&aacute;ch thủ đ&ocirc; H&agrave; Nội 883 km về ph&iacute;a bắc v&agrave; th&agrave;nh phố Hồ Ch&iacute; Minh 838 km về ph&iacute;a nam. Diện t&iacute;ch tự nhi&ecirc;n khoảng 5.152,67 km&sup2;, bao gồm 1 th&agrave;nh phố trực thuộc tỉnh (th&agrave;nh phố Quảng Ng&atilde;i), 6 huyện đồng bằng ven biển (B&igrave;nh Sơn, Sơn Tịnh, Tư Nghĩa, Nghĩa H&agrave;nh, Mộ Đức, Đức Phổ), 6 huyện miền n&uacute;i (Ba Tơ, Tr&agrave; Bồng, T&acirc;y Tr&agrave;, Sơn T&acirc;y, Sơn H&agrave;, Minh Long) v&agrave; 1 huyện đảo (L&yacute; Sơn).<br />\r\n<br />\r\n<strong>D&acirc;n số, d&acirc;n cư</strong></p>\r\n\r\n<p>D&acirc;n số Quảng Ng&atilde;i năm 2013 l&agrave; 1,236 triệu người, với mật độ d&acirc;n số trung b&igrave;nh 237 người/km&sup2;. Đ&acirc;y l&agrave; địa b&agrave;n sinh sống của nhiều tộc người kh&aacute;c nhau; trong đ&oacute; chiếm đa số l&agrave; người Kinh (88%), Hre (8,58%), Cor (1,8%)&hellip;<br />\r\n<br />\r\n<strong>Giao th&ocirc;ng vận tải</strong></p>\r\n\r\n<p><em><strong>1. Đường bộ:</strong></em></p>\r\n\r\n<p>Quốc lộ 1A: đoạn chạy qua tỉnh: 98km</p>\r\n\r\n<p>Quốc lộ 24A: Nối liền Quốc lội 1A (đoạn qua Thạch Trụ, huyện Mộ Đức, tỉnh Quảng Ng&atilde;i) với KonTum d&agrave;i 69km v&agrave; Quốc lộ 24B d&agrave;i 18km.</p>\r\n\r\n<p>Đường Đ&ocirc;ng Trường Sơn đi qua 02 x&atilde; Sơn M&ugrave;a v&agrave; Sơn Bua, huyện Sơn T&acirc;y với tổng chiều d&agrave;i 13 km</p>\r\n\r\n<p>Tỉnh lộ: Gồm 18 tuyến với tổng chiều d&agrave;i 520,5km</p>\r\n\r\n<p>Đường ven biển Dung Quất- Sa Huỳnh đi qua c&aacute;c huyện B&igrave;nh Sơn, Sơn Tịnh, Tư Nghĩa, Mộ Đức, Đức Phổ v&agrave; th&agrave;nh phố Quảng Ng&atilde;i, với chiều d&agrave;i khoảng 117 Km. C&oacute; điểm đầu tại ranh giữa hai tỉnh Quảng Nam v&agrave; Quảng Ng&atilde;i thuộc Khu kinh tế Dung Quất v&agrave; điểm cuối giao với Quốc lộ 1A tại Km1116 thuộc Sa Huỳnh, huyện Đức Phổ. Đ&acirc;y l&agrave; tuyến đường g&oacute;p phần quan trọng khơi dậy tiềm năng vừa ph&aacute;t triển kinh tế - x&atilde; hội dọc v&ugrave;ng ven biển, n&acirc;ng cao đời sống người d&acirc;n, vừa gắn với củng cố, tăng cường tiềm lực quốc ph&ograve;ng - an ninh ở khu vực n&agrave;y.</p>\r\n\r\n<p>Đường nội KKT Dung Quất: c&oacute; tổng chiều d&agrave;i tr&ecirc;n 60km. Hiện tiếp tục đầu tư x&acirc;y dựng ho&agrave;n th&agrave;nh c&aacute;c tuyến đường như: đường Dung Quất nối với đường Hồ Ch&iacute; Minh theo hướng Tr&agrave; My - Tr&agrave; Bồng - B&igrave;nh Long - Ng&atilde; ba Nh&agrave; m&aacute;y lọc dầu - cảng Dung Quất. Quy hoạch v&agrave; x&acirc;y dựng tuyến đường Trị B&igrave;nh - cảng Dung Quất v&agrave; một số tuyến đường trục ch&iacute;nh của đ&ocirc; thị Vạn Tường.<br />\r\n<em><strong>&nbsp;<br />\r\n2. Đường Sắt:</strong></em></p>\r\n\r\n<p>Tuyến đường sắt Bắc - Nam: chạy suốt chiều d&agrave;i tỉnh.</p>\r\n\r\n<p><em><strong>3. Đường h&agrave;ng kh&ocirc;ng:&nbsp;&nbsp;</strong></em></p>\r\n\r\n<p>S&acirc;n bay Chu Lai (Tỉnh Quảng Nam) c&aacute;ch Th&agrave;nh phố Quảng Ng&atilde;i 35km, c&aacute;ch Khu kinh tế Dung Quất 04km về hướng Bắc.</p>\r\n\r\n<p>&nbsp;<strong>4. Hệ thống cảng biển:&nbsp;</strong></p>\r\n\r\n<p>Cảng biển nước s&acirc;u Dung Quất: với lợi thế k&iacute;n gi&oacute;, c&aacute;ch tuyến h&agrave;ng hải quốc tế 90km, tuyến nội hải 30km v&agrave; độ s&acirc;u từ 10-19m, cảng Dung Quất đ&atilde; được thiết kế với hệ thống cảng đa chức năng gồm:</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Khu cảng Dầu kh&iacute; với lượng h&agrave;ng h&oacute;a th&ocirc;ng qua l&agrave; 6,1 triệu tấn dầu sản phẩm/năm v&agrave; x&acirc;y dựng 01 bến phao để nhập dầu th&ocirc; cho t&agrave;u dầu c&oacute; trọng tải từ 80.000 tấn - 110.000 tấn tại vịnh Việt Thanh; cảng chuy&ecirc;n d&ugrave;ng gắn với Khu c&ocirc;ng nghiệp li&ecirc;n hợp t&agrave;u thủy, khu x&acirc;y dựng Nh&agrave; m&aacute;y Luyện c&aacute;n th&eacute;p v&agrave; c&aacute;c Nh&agrave; m&aacute;y C&ocirc;ng nghiệp nặng.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Cảng tổng hợp được chia th&agrave;nh 2 ph&acirc;n khu cảng: ph&acirc;n khu cảng Tổng hợp 1 ở ngay sau Khu cảng Dầu kh&iacute;, đảm bảo cho c&aacute;c t&agrave;u c&oacute; trọng tải từ 5.000 tấn - 50.000 tấn ra v&agrave;o; ph&acirc;n khu cảng Tổng hợp 2 ở ph&iacute;a Nam vịnh Dung Quất, b&ecirc;n tả s&ocirc;ng Đập.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; &nbsp;Khu cảng Chuy&ecirc;n d&ugrave;ng gắn với Khu c&ocirc;ng nghiệp li&ecirc;n hợp t&agrave;u thủy, khu x&acirc;y dựng Nh&agrave; m&aacute;y Luyện c&aacute;n th&eacute;p v&agrave; c&aacute;c Nh&agrave; m&aacute;y C&ocirc;ng nghiệp nặng.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; &nbsp;Khu cảng Thương mại phục vụ cho Khu bảo thuế v&agrave; 01 cảng trung chuyển container quốc tế nằm tại vị tr&iacute; giữa Khu cảng Chuy&ecirc;n d&ugrave;ng v&agrave; Khu cảng Tổng hợp để đ&oacute;n t&agrave;u c&oacute; trọng tải từ 10 - 15 vạn DWT.&nbsp;<br />\r\n&nbsp;<br />\r\nCảng Dung Quất được đầu tư x&acirc;y dựng để bảo đảm khối lượng h&agrave;ng h&oacute;a th&ocirc;ng qua khoảng 20 triệu tấn/năm v&agrave;o năm 2010 v&agrave; khoảng 34 triệu tấn/năm v&agrave;o năm 2020 Hiện nay đang vận h&agrave;nh bến tổng hợp cho t&agrave;u 1,5 vạn DWT.<br />\r\n&nbsp;<br />\r\n* Ngo&agrave;i ra, với bờ biển d&agrave;i 144 km, Quảng Ng&atilde;i c&oacute; nhiều cửa biển, cảng biển nhỏ như: Sa Kỳ, Sa Cần, B&igrave;nh Ch&acirc;u, Mỹ &Aacute;,&hellip; c&oacute; tiềm năng về giao th&ocirc;ng đường thủy, thương mại v&agrave; du lịch</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/QuangNgai/qn2.jpg\" style=\"height:1200px; width:1599px\" /></p>\r\n\r\n<p><strong>T&Agrave;I NGUY&Ecirc;N THI&Ecirc;N NHI&Ecirc;N V&Agrave; TIỀM NĂNG PH&Aacute;T TRIỂN</strong></p>\r\n\r\n<p><strong>T&agrave;i nguy&ecirc;n đất</strong></p>\r\n\r\n<p>Theo kết quả điều tra x&acirc;y dựng bản đồ đất thuộc hệ thống ph&acirc;n loại của FAO - UNESCO, tr&ecirc;n diện t&iacute;ch 513.688,14 ha, Quảng Ng&atilde;i c&oacute; 9 nh&oacute;m đất ch&iacute;nh với 25 đơn vị đất v&agrave; 68 đơn vị đất phụ. Ch&iacute;n nh&oacute;m đất ch&iacute;nh l&agrave;: cồn c&aacute;t, đất c&aacute;t ven biển, đất mặn, đất ph&ugrave; sa, đất gi&acirc;y, đất x&aacute;m, đất đỏ v&agrave;ng, đất đen, đất nứt nẻ, đất x&oacute;i m&ograve;n trơ trọi đ&aacute;. Trong đ&oacute;, nh&oacute;m đất x&aacute;m c&oacute; vị tr&iacute; quan trọng (chiếm 74,65% diện t&iacute;ch đất tự nhi&ecirc;n) th&iacute;ch hợp với c&acirc;y c&ocirc;ng nghiệp d&agrave;i ng&agrave;y, c&acirc;y đặc sản, dược liệu, chăn nu&ocirc;i gia s&uacute;c v&agrave; nh&oacute;m đất ph&ugrave; sa thuộc hạ lưu c&aacute;c s&ocirc;ng (chiếm 19,3% diện t&iacute;ch đất tự nhi&ecirc;n), th&iacute;ch hợp với trồng l&uacute;a, c&acirc;y c&ocirc;ng nghiệp ngắn ng&agrave;y, rau đậu.</p>\r\n\r\n<p>Diện t&iacute;ch đất của Quảng Ng&atilde;i được sử dụng gồm 322.034,59 ha đất n&ocirc;ng nghiệp (62,5% diện t&iacute;ch đất tự nhi&ecirc;n), 45.636,2 ha đất phi n&ocirc;ng nghiệp (8,86% diện t&iacute;ch đất tự nhi&ecirc;n) v&agrave; 147.595,9ha đất chưa sử dụng (28,64% diện t&iacute;ch đất tự nhi&ecirc;n).</p>\r\n\r\n<p><strong>T&agrave;i nguy&ecirc;n rừng</strong></p>\r\n\r\n<p>Rừng Quảng Ng&atilde;i phong ph&uacute; về l&acirc;m, thổ sản với nhiều loại gỗ như: trắc, huỳnh, đinh hương, sến, kiền kiền, gụ, dỗi&hellip;, tổng trữ lượng gỗ khoảng 9,8 triệu m&sup3;. So với c&aacute;c tỉnh trong v&ugrave;ng duy&ecirc;n hải miền Trung, vốn rừng tự nhi&ecirc;n của Quảng Ng&atilde;i rất &iacute;t, chủ yếu l&agrave; rừng trung b&igrave;nh v&agrave; rừng ngh&egrave;o, nhưng nếu so về trữ lượng (t&iacute;nh tr&ecirc;n 1 ha) th&igrave; trữ lượng c&aacute;c loại rừng của Quảng Ng&atilde;i lại cao hơn mức trung b&igrave;nh của cả nước.</p>\r\n\r\n<p><strong>T&agrave;i nguy&ecirc;n kho&aacute;ng sản</strong></p>\r\n\r\n<p>T&agrave;i nguy&ecirc;n kho&aacute;ng sản kh&ocirc;ng đa dạng về chủng loại, chủ yếu l&agrave; kho&aacute;ng sản phục vụ cho c&ocirc;ng nghiệp vật liệu x&acirc;y dựng, nước kho&aacute;ng v&agrave; một số kho&aacute;ng sản kh&aacute;c.</p>\r\n\r\n<p>Những kho&aacute;ng sản c&oacute; thể khai th&aacute;c trong 10 năm tới l&agrave;: graph&iacute;t trữ lượng khoảng 4 triệu tấn, trong đ&oacute; trữ lượng cho ph&eacute;p đưa v&agrave;o khai th&aacute;c 2,5 triệu tấn, h&agrave;m lượng c&aacute;cbon trung b&igrave;nh 20%, c&oacute; nơi 24% nằm tr&ecirc;n địa b&agrave;n huyện Sơn Tịnh; silimanhit trữ lượng 1 triệu tấn, ph&acirc;n bổ ở Hưng Nhượng (Sơn Tịnh); than b&ugrave;n ở B&igrave;nh Ph&uacute; (B&igrave;nh Sơn) trữ lượng 476 ngh&igrave;n m&sup3;; cao lanh ở Sơn Tịnh trữ lượng khoảng 4 triệu tấn. Đ&aacute; x&acirc;y dựng gồm c&aacute;c loại đ&aacute; l&agrave;m vật liệu x&acirc;y dựng, rải đường giao th&ocirc;ng, &aacute;p tường, l&aacute;t nền, trữ lượng tr&ecirc;n 7 tỷ m&sup3;, ph&acirc;n bố ở Đức Phổ, Tr&agrave; Bồng v&agrave; một số huyện kh&aacute;c; nước kho&aacute;ng ở Thạch B&iacute;ch (Tr&agrave; Bồng), Đức L&acirc;n (Mộ Đức), Nghĩa Thuận (Tư Nghĩa) v&agrave; Sơn Tịnh.</p>\r\n\r\n<p><strong>T&agrave;i nguy&ecirc;n biển v&agrave; ven biển</strong></p>\r\n\r\n<p>Với bờ biển d&agrave;i 144 km, c&ugrave;ng v&ugrave;ng l&atilde;nh hải rộng lớn 11.000 km&sup2;, c&oacute; 6 cửa biển, dồi d&agrave;o nguồn lực hải sản, Quảng Ng&atilde;i c&ograve;n c&oacute; một khả năng lớn để đi l&ecirc;n từ ph&aacute;t triển kinh tế biển đặc biệt l&agrave; khai th&aacute;c c&oacute; hiệu quả c&aacute; nổi trữ lượng khoảng 68.000 tấn c&aacute;c loại. &ETH;&acirc;y cũng l&agrave; ng&agrave;nh kinh tế mũi nhọn của tỉnh. Với 6 huyện ven biển v&agrave; một huyện đảo, c&oacute; thể n&oacute;i biển đảo Quảng Ng&atilde;i c&oacute; nhiều tiềm năng về cảnh quan, địa h&igrave;nh, nhiều di t&iacute;ch lịch sử, lễ hội truyền thống, l&agrave; một trong c&aacute;c tỉnh c&oacute; bờ biển d&agrave;i v&agrave; đẹp, c&oacute; diện t&iacute;ch khai th&aacute;c, nu&ocirc;i trồng thủy hải sản rộng lớn. Đặc biệt, đảo L&yacute; Sơn c&ograve;n l&agrave; nơi lưu giữ h&igrave;nh ảnh của những đội h&ugrave;ng binh xưa dong buồm ra biển Đ&ocirc;ng, đặt những vi&ecirc;n đ&aacute; chủ quyền thi&ecirc;ng li&ecirc;ng đầu ti&ecirc;n tr&ecirc;n quần đảo Ho&agrave;ng Sa v&agrave; Trường Sa.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/QuangNgai/qn3.jpg\" style=\"height:429px; width:640px\" /></p>\r\n\r\n<p><strong>T&agrave;i nguy&ecirc;n du lịch</strong></p>\r\n\r\n<p>Quảng Ng&atilde;i c&oacute; di chỉ văn h&oacute;a Sa Huỳnh, ch&ugrave;a Thi&ecirc;n Ấn, th&agrave;nh cổ Ch&acirc;u Sa... l&agrave; những di t&iacute;ch lịch sử - văn h&oacute;a (LSVH) quan trọng của thời tiền - sơ sử v&agrave; cổ trung đại. Quảng Ng&atilde;i cũng l&agrave; nơi lưu dấu nhiều sự kiện v&agrave; chứng t&iacute;ch quan trọng trong lịch sử đấu tranh c&aacute;ch mạng thời hiện đại như: Chứng t&iacute;ch Sơn Mỹ, địa đạo &ETH;&agrave;m To&aacute;i x&atilde; B&igrave;nh Ch&acirc;u, Ba Tơ quật khởi, mộ nh&agrave; ch&iacute; sĩ y&ecirc;u nước Huỳnh Th&uacute;c Kh&aacute;ng... Ngo&agrave;i ra, Quảng Ng&atilde;i c&oacute; 144 km bờ biển với những quang cảnh n&ecirc;n thơ rất th&iacute;ch hợp cho việc ph&aacute;t triển kinh doanh du lịch như: b&atilde;i biển Sa Huỳnh, b&atilde;i biển Mỹ Kh&ecirc;, vũng Dung Quất, mũi Ba L&agrave;ng An, Cổ Lũy c&ocirc; th&ocirc;n... Trong v&ugrave;ng biển ph&iacute;a đ&ocirc;ng của tỉnh c&oacute; đảo L&yacute; Sơn, một v&ugrave;ng biển đảo c&oacute; nhiều cảnh quan thi&ecirc;n nhi&ecirc;n kỳ th&uacute;, l&agrave; v&ugrave;ng đất ph&ecirc;n dậu ph&iacute;a đ&ocirc;ng của tỉnh, qu&ecirc; hương của những &ldquo;h&ugrave;ng binh&rdquo; Ho&agrave;ng Sa thuở trước, l&agrave; &ldquo;vương quốc&rdquo; của nghề trồng tỏi v&agrave; nghề đ&aacute;nh bắt hải sản xa bờ, cũng l&agrave; một &ldquo;trọng địa&rdquo; du lịch đầy tiềm năng với những b&atilde;i biển hoang sơ, c&oacute; nhiều di t&iacute;ch LSVH, c&ugrave;ng những bến thuyền t&agrave;u ghe tấp nập&hellip; hấp dẫn du kh&aacute;ch.</p>\r\n\r\n<p><img alt=\"\" src=\"/lvtn/public/images/tintuc/files/QuangNgai/qn4.jpg\" style=\"height:375px; width:500px\" /></p>\r\n\r\n<p><strong>Di sản văn h&oacute;a</strong></p>\r\n\r\n<p>Quảng Ng&atilde;i l&agrave; mảnh đất c&oacute; bề d&agrave;y lịch sử, l&agrave; qu&ecirc; hương của văn h&oacute;a Sa Huỳnh v&agrave; l&agrave; địa b&agrave;n trọng yếu của văn h&oacute;a Champa, c&oacute; hai danh thắng nổi tiếng l&agrave; &quot;n&uacute;i Ấn, s&ocirc;ng Tr&agrave;&quot;. Đặc biệt, Quảng Ng&atilde;i c&oacute; hệ thống Trường Lũy được x&acirc;y dựng từ thế kỷ XVI, trải dọc v&ugrave;ng trung du ph&iacute;a t&acirc;y của tỉnh, k&eacute;o d&agrave;i đến tỉnh B&igrave;nh Định, l&agrave; hệ thống th&agrave;nh lũy d&agrave;i nhất Việt Nam, được c&ocirc;ng nhận l&agrave; di t&iacute;ch LSVH cấp quốc gia. Quảng Ng&atilde;i c&ograve;n l&agrave; qu&ecirc; hương của c&aacute;c danh nh&acirc;n, danh tướng như: Trương Định, L&ecirc; Trung Đ&igrave;nh, Phạm Văn Đồng; nhiều nh&agrave; tr&iacute; thức, nh&agrave; văn, nh&agrave; thơ, nhạc sĩ, nghệ sĩ t&ecirc;n tuổi: B&iacute;ch Kh&ecirc;, Tế Hanh, NSND Tr&agrave; Giang, Trương Quang Lục, Thế Bảo, Nhất Sinh... Quảng Ng&atilde;i c&ograve;n c&oacute; những lễ hội văn h&oacute;a đặc sắc diễn ra quanh năm như: lễ hội nghinh &Ocirc;ng, lễ khao lề thế l&iacute;nh Ho&agrave;ng Sa (L&yacute; Sơn), lễ hội ăn tr&acirc;u, lễ hội cầu ngư, lễ hội đua thuyền&hellip;</p>\r\n\r\n<p>(Nguồn: www.nhipcauviet.com.vn)</p>', 1, 1, 1, 0, '2018-11-30 18:30:53', '2018-12-05 13:34:27'),
(21, 'Car', 'X4TWfvQV.jpg', 'e', '<p>e</p>', 1, 1, 1, 1, '2021-07-02 05:29:37', '2021-07-02 05:29:37');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `thanh_vien` varchar(100) NOT NULL COMMENT 'thành viên thanh toán',
  `money` float NOT NULL COMMENT 'số tiền thanh toán',
  `note` varchar(255) DEFAULT NULL COMMENT 'ghi chú thanh toán',
  `vnp_response_code` varchar(255) NOT NULL COMMENT 'mã phản hồi',
  `code_vnpay` varchar(255) NOT NULL COMMENT 'mã giao dịch vnpay',
  `code_bank` varchar(255) NOT NULL COMMENT 'mã ngân hàng',
  `time` datetime NOT NULL COMMENT 'thời gian chuyển khoản'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `thanh_vien`, `money`, `note`, `vnp_response_code`, `code_vnpay`, `code_bank`, `time`) VALUES
(25, '20210627104712', '0366440472', 540000, '0366440472', '00', '13533048', 'NCB', '2021-06-27 11:29:43'),
(26, '20210627104712', '0366440472', 540000, '0366440472', '00', '13533048', 'NCB', '2021-06-27 11:29:43'),
(27, '20210627113252', '0366440472', 570000, '0366440472', '00', '13533050', 'NCB', '2021-06-27 11:33:12'),
(28, '20210627133110', '0366440472', 540000, '0366440472', '00', '13533060', 'NCB', '2021-06-27 13:31:29'),
(29, '20210627133110', '0366440472', 540000, '0366440472', '00', '13533060', 'NCB', '2021-06-27 13:31:29'),
(30, '20210627133110', '0366440472', 540000, '0366440472', '00', '13533060', 'NCB', '2021-06-27 13:31:29'),
(31, '20210627133843', '0366440472', 540000, '0366440472', '00', '13533061', 'NCB', '2021-06-27 13:39:03'),
(32, '20210627140043', '0366440472', 570000, '0366440472', '00', '13533066', 'NCB', '2021-06-27 14:01:04'),
(33, '20210627140043', '0366440472', 570000, '0366440472', '00', '13533066', 'NCB', '2021-06-27 14:01:04'),
(34, '20210627140508', '0366440472', 540000, '0366440472', '00', '13533068', 'NCB', '2021-06-27 14:05:45'),
(35, '20210702145040', '0366440472', 600000, '0366440472', '00', '13536949', 'NCB', '2021-07-02 14:52:02'),
(36, '20210702164556', '0366440472', 600000, '0366440472', '00', '13537130', 'NCB', '2021-07-02 16:46:52'),
(37, '20210702173909', '0366440472', 570000, '0366440472', '00', '13537195', 'NCB', '2021-07-02 17:39:54'),
(38, '20210702214910', '0366440472', 570000, '0366440472', '00', '13537286', 'NCB', '2021-07-02 21:49:33'),
(39, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(40, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(41, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(42, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(43, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(44, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(45, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(46, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(47, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(48, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(49, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(50, '20210702220619', '0366440472', 600000, '0366440472', '00', '13537290', 'NCB', '2021-07-02 22:06:47'),
(51, '20210702222626', '0366440472', 570000, '0366440472', '00', '13537297', 'NCB', '2021-07-02 22:26:57'),
(52, '20210702222755', '0366440472', 600000, '0366440472', '00', '13537298', 'NCB', '2021-07-02 22:28:25'),
(53, '20210703180335', '0366440472', 570000, '0366440472', '00', '13537514', 'NCB', '2021-07-03 18:04:48'),
(54, '20210704181740', '0366440472', 600000, '0366440472', '00', '13537719', 'NCB', '2021-07-04 18:18:19'),
(55, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(56, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(57, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(58, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(59, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(60, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(61, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(62, '20210704182913', '0366440472', 570000, '0366440472', '00', '13537723', 'NCB', '2021-07-04 18:29:33'),
(63, '20210704191720', '0366440472', 570000, '0366440472', '00', '13537730', 'NCB', '2021-07-04 19:17:38'),
(64, '20210704194315', '0366440472', 570000, '0366440472', '00', '13537732', 'NCB', '2021-07-04 19:43:35'),
(65, '20210704194443', '0366440472', 570000, '0366440472', '00', '13537733', 'NCB', '2021-07-04 19:44:57'),
(66, '20210704194443', '0366440472', 570000, '0366440472', '00', '13537733', 'NCB', '2021-07-04 19:44:57'),
(67, '20210704194559', '0366440472', 600000, '0366440472', '00', '13537734', 'NCB', '2021-07-04 19:46:20'),
(68, '20210704194559', '0366440472', 600000, '0366440472', '00', '13537734', 'NCB', '2021-07-04 19:46:20'),
(69, '20210704194559', '0366440472', 600000, '0366440472', '00', '13537734', 'NCB', '2021-07-04 19:46:20'),
(70, '20210704195902', '0366440472', 600000, '0366440472', '00', '13537735', 'NCB', '2021-07-04 19:59:21'),
(71, '20210704200002', '0366440472', 600000, '0366440472', '00', '13537736', 'NCB', '2021-07-04 20:00:17'),
(72, '20210704200122', '0366440472', 570000, '0366440472', '00', '13537738', 'NCB', '2021-07-04 20:01:40'),
(73, '20210704200309', '0366440472', 600000, '0366440472', '00', '13537739', 'NCB', '2021-07-04 20:03:22'),
(74, '20210704200607', '0366440472', 600000, '0366440472', '00', '13537740', 'NCB', '2021-07-04 20:06:24'),
(75, '20210704200607', '0366440472', 600000, '0366440472', '00', '13537740', 'NCB', '2021-07-04 20:06:24'),
(76, '20210704200607', '0366440472', 600000, '0366440472', '00', '13537740', 'NCB', '2021-07-04 20:06:24'),
(77, '20210704200607', '0366440472', 600000, '0366440472', '00', '13537740', 'NCB', '2021-07-04 20:06:24'),
(78, '20210704201214', '0366440472', 570000, '0366440472', '00', '13537744', 'NCB', '2021-07-04 20:12:32'),
(79, '20210704201214', '0366440472', 570000, '0366440472', '00', '13537744', 'NCB', '2021-07-04 20:12:32'),
(80, '20210704201331', '0366440472', 600000, '0366440472', '00', '13537745', 'NCB', '2021-07-04 20:13:48'),
(81, '20210704202554', '0366440472', 540000, '0366440472', '00', '13537751', 'NCB', '2021-07-04 20:26:14'),
(82, '20210704202554', '0366440472', 540000, '0366440472', '00', '13537751', 'NCB', '2021-07-04 20:26:14'),
(83, '20210704202719', '0366440472', 540000, '0366440472', '00', '13537753', 'NCB', '2021-07-04 20:27:36'),
(84, '20210704202719', '0366440472', 540000, '0366440472', '00', '13537753', 'NCB', '2021-07-04 20:27:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name_province` varchar(255) NOT NULL,
  `name_province_en` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `province`
--

INSERT INTO `province` (`id`, `name_province`, `name_province_en`, `created_at`, `updated_at`) VALUES
(1, 'Quảng Ngãi', 'Quang Ngai', '2021-01-14 23:10:00', '2021-01-14 23:10:11'),
(2, 'TP.Hồ Chí Minh', 'TP.Ho Chi Minh', '2021-01-14 23:10:22', '2021-01-14 23:10:26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `route`
--

CREATE TABLE `route` (
  `id` int(11) NOT NULL,
  `id_staff_create` int(11) NOT NULL,
  `id_staff_edit` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `bus_station` varchar(255) NOT NULL,
  `estime` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `route`
--

INSERT INTO `route` (`id`, `id_staff_create`, `id_staff_edit`, `from`, `to`, `bus_station`, `estime`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'TP.Hồ Chí Minh', 'Quảng Ngãi', '1,3', 9120, '2021-01-15 00:35:58', '2021-01-15 00:36:06'),
(2, 1, 1, 'Quảng Ngãi', 'TP.Hồ Chí Minh', '1,3', 0, '2021-01-15 00:36:02', '2021-01-15 00:36:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `statistic`
--

CREATE TABLE `statistic` (
  `ID` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `sum_buses` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `revenue` int(11) NOT NULL,
  `created__at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `statistic`
--

INSERT INTO `statistic` (`ID`, `month`, `year`, `sum_buses`, `cost`, `revenue`, `created__at`, `updated_at`) VALUES
(1, 1, 2017, 10, 50000000, 70000000, '2018-10-11 18:04:29', '2018-10-11 18:04:29'),
(2, 2, 2017, 15, 55000000, 80000000, '2018-10-11 19:00:10', '2018-10-11 19:00:10'),
(3, 3, 2017, 12, 56000000, 85000000, '2018-10-11 19:00:54', '2018-10-11 19:00:54'),
(4, 4, 2017, 20, 40000000, 65000000, '2018-10-11 19:01:32', '2018-10-11 19:01:32'),
(5, 5, 2017, 22, 65000000, 90000000, '2018-10-11 19:02:16', '2018-10-11 19:02:16'),
(6, 6, 2017, 23, 73000000, 89000000, '2018-10-11 19:02:58', '2018-10-11 19:02:58'),
(7, 7, 2017, 50, 80000000, 120000000, '2018-10-11 19:03:47', '2018-10-11 19:03:47'),
(8, 8, 2017, 13, 40000000, 67000000, '2018-12-13 17:11:07', '2018-12-13 17:11:07'),
(9, 9, 2017, 23, 45000000, 71000000, '2018-12-13 17:12:14', '2018-12-13 17:12:14'),
(10, 10, 2017, 25, 49000000, 75000000, '2018-12-13 17:12:40', '2018-12-13 17:12:40'),
(11, 11, 2017, 31, 54000000, 80000000, '2018-12-13 17:13:16', '2018-12-13 17:13:16'),
(12, 12, 2017, 35, 54000000, 89000000, '2018-12-13 17:13:45', '2018-12-13 17:13:45'),
(13, 1, 2018, 15, 50000000, 76000000, '2018-12-13 17:16:03', '2018-12-13 17:16:03'),
(14, 2, 2018, 15, 55000000, 82000000, '2018-12-13 17:16:35', '2018-12-13 17:16:35'),
(15, 3, 2018, 17, 56000000, 90000000, '2018-12-13 17:17:04', '2018-12-13 17:17:04'),
(16, 4, 2018, 21, 45000000, 73000000, '2018-12-13 17:17:34', '2018-12-13 17:17:34'),
(17, 5, 2018, 21, 65000000, 93000000, '2018-12-13 17:18:01', '2018-12-13 17:18:01'),
(18, 6, 2018, 31, 81000000, 95000000, '2018-12-13 17:18:36', '2018-12-13 17:18:36'),
(19, 7, 2018, 47, 83000000, 112000000, '2018-12-13 17:19:14', '2018-12-13 17:19:14'),
(20, 8, 2018, 20, 56000000, 75000000, '2018-12-13 17:19:51', '2018-12-13 17:19:51'),
(21, 9, 2018, 27, 49000000, 83000000, '2018-12-13 17:20:28', '2018-12-13 17:20:28'),
(22, 10, 2018, 32, 53000000, 85000000, '2018-12-13 17:21:22', '2018-12-13 17:21:22'),
(23, 11, 2018, 29, 54000000, 75000000, '2018-12-13 17:22:04', '2018-12-13 17:22:04'),
(24, 12, 2018, 42, 63000000, 97000000, '2018-12-13 17:22:40', '2018-12-13 17:22:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `id_buses` int(11) NOT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_ticket` varchar(255) DEFAULT NULL,
  `id_food` int(11) DEFAULT NULL,
  `name_seat` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `id_payment` varchar(50) DEFAULT NULL,
  `payment_status` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_hide` tinyint(1) NOT NULL DEFAULT 0,
  `date_reg` datetime DEFAULT NULL,
  `date_change` date DEFAULT NULL,
  `time_change` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `ticket`
--

INSERT INTO `ticket` (`id`, `id_buses`, `id_staff`, `id_customer`, `id_ticket`, `id_food`, `name_seat`, `status`, `id_payment`, `payment_status`, `created_at`, `updated_at`, `is_hide`, `date_reg`, `date_change`, `time_change`) VALUES
(1, 1, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(2, 1, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(3, 1, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(4, 1, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(5, 1, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(6, 1, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 09:03:02', 0, NULL, NULL, NULL),
(7, 1, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(8, 1, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(9, 1, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(10, 1, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(11, 1, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(12, 1, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(13, 1, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(14, 1, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(15, 1, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(16, 1, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(17, 1, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(18, 1, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(19, 1, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(20, 1, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(21, 1, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(22, 1, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(23, 1, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(24, 1, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(25, 1, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(26, 1, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(27, 1, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(28, 1, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(29, 1, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(30, 1, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(31, 1, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(32, 1, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(33, 1, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(34, 1, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(35, 1, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(36, 1, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(37, 1, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(38, 1, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(39, 1, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(40, 1, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(41, 1, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(42, 1, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(43, 1, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(44, 1, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(45, 1, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(46, 1, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2018-10-24 08:28:26', '2018-10-24 08:28:26', 0, NULL, NULL, NULL),
(47, 2, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(48, 2, 1, 6, '20210704202719', NULL, 'A-2', 1, '20210704202719', 1, '2021-02-01 11:57:22', '2021-07-10 06:06:32', 0, '2021-07-04 20:27:18', '2021-07-20', '23:00:00'),
(49, 2, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(50, 2, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(51, 2, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(52, 2, NULL, 11, '20210710180905', NULL, 'A-6', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, '2021-07-10 18:09:00', NULL, NULL),
(53, 2, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(54, 2, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(55, 2, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(56, 2, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(57, 2, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(58, 2, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(59, 2, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(60, 2, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(61, 2, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(62, 2, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(63, 2, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(64, 2, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(65, 2, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(66, 2, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(67, 2, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(68, 2, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(69, 2, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(70, 2, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(71, 2, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(72, 2, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(73, 2, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(74, 2, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(75, 2, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(76, 2, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(77, 2, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(78, 2, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(79, 2, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(80, 2, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(81, 2, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(82, 2, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(83, 2, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(84, 2, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(85, 2, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(86, 2, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(87, 2, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(88, 2, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-02-01 11:57:22', '2001-07-21 02:07:47', 0, NULL, NULL, NULL),
(89, 2, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(90, 2, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(91, 2, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-02-01 11:57:22', '2021-02-01 11:57:22', 0, NULL, NULL, NULL),
(92, 2, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-02-01 11:57:22', '2001-07-21 02:07:36', 0, NULL, NULL, NULL),
(93, 6, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(94, 6, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(95, 6, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(96, 6, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(97, 6, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(98, 6, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(99, 6, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(100, 6, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(101, 6, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(102, 6, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(103, 6, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(104, 6, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(105, 6, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(106, 6, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(107, 6, NULL, 11, '20210710181753', NULL, 'A-15', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, '2021-07-10 18:17:45', NULL, NULL),
(108, 6, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(109, 6, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(110, 6, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(111, 6, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(112, 6, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(113, 6, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(114, 6, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(115, 6, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(116, 6, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(117, 6, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(118, 6, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(119, 6, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(120, 6, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(121, 6, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(122, 6, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(123, 6, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(124, 6, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(125, 6, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(126, 6, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(127, 6, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(128, 6, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(129, 6, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(130, 6, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(131, 6, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(132, 6, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(133, 6, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(134, 6, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(135, 6, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(136, 6, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(137, 6, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(138, 6, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-07-10 06:16:45', '2021-07-10 06:16:45', 0, NULL, NULL, NULL),
(139, 7, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(140, 7, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(141, 7, NULL, 11, '20210724101918', NULL, 'A-3', 1, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, '2021-07-24 10:19:09', NULL, NULL),
(142, 7, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(143, 7, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(144, 7, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(145, 7, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(146, 7, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(147, 7, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(148, 7, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(149, 7, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(150, 7, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(151, 7, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(152, 7, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(153, 7, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(154, 7, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(155, 7, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(156, 7, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(157, 7, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(158, 7, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(159, 7, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(160, 7, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(161, 7, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(162, 7, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(163, 7, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(164, 7, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(165, 7, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(166, 7, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(167, 7, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(168, 7, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(169, 7, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(170, 7, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(171, 7, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(172, 7, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(173, 7, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(174, 7, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(175, 7, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(176, 7, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(177, 7, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(178, 7, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(179, 7, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(180, 7, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(181, 7, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(182, 7, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(183, 7, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(184, 7, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-07-24 09:31:34', '2021-07-24 09:31:34', 0, NULL, NULL, NULL),
(185, 8, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(186, 8, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(187, 8, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(188, 8, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(189, 8, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(190, 8, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(191, 8, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(192, 8, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(193, 8, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(194, 8, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(195, 8, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(196, 8, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(197, 8, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(198, 8, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(199, 8, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(200, 8, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(201, 8, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(202, 8, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(203, 8, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(204, 8, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(205, 8, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(206, 8, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(207, 8, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(208, 8, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(209, 8, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(210, 8, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(211, 8, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(212, 8, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(213, 8, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(214, 8, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(215, 8, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(216, 8, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(217, 8, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(218, 8, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(219, 8, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(220, 8, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(221, 8, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(222, 8, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(223, 8, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(224, 8, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(225, 8, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(226, 8, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(227, 8, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(228, 8, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(229, 8, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(230, 8, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-07-24 10:17:15', '2021-07-24 10:17:15', 0, NULL, NULL, NULL),
(231, 8, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(232, 8, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(233, 8, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(234, 8, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(235, 8, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(236, 8, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(237, 8, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(238, 8, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(239, 8, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(240, 8, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(241, 8, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(242, 8, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(243, 8, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(244, 8, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(245, 8, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(246, 8, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(247, 8, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(248, 8, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(249, 8, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(250, 8, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(251, 8, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(252, 8, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(253, 8, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(254, 8, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(255, 8, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(256, 8, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(257, 8, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(258, 8, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(259, 8, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(260, 8, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(261, 8, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(262, 8, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(263, 8, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(264, 8, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(265, 8, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(266, 8, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(267, 8, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(268, 8, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(269, 8, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(270, 8, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(271, 8, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(272, 8, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(273, 8, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(274, 8, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(275, 8, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(276, 8, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-07-24 10:17:39', '2021-07-24 10:17:39', 0, NULL, NULL, NULL),
(277, 9, NULL, NULL, NULL, NULL, 'A-1', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(278, 9, NULL, NULL, NULL, NULL, 'A-2', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(279, 9, NULL, NULL, NULL, NULL, 'A-3', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(280, 9, NULL, NULL, NULL, NULL, 'A-4', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(281, 9, NULL, NULL, NULL, NULL, 'A-5', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(282, 9, NULL, NULL, NULL, NULL, 'A-6', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(283, 9, NULL, NULL, NULL, NULL, 'A-7', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(284, 9, NULL, NULL, NULL, NULL, 'A-8', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(285, 9, NULL, NULL, NULL, NULL, 'A-9', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(286, 9, NULL, NULL, NULL, NULL, 'A-10', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(287, 9, NULL, NULL, NULL, NULL, 'A-11', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(288, 9, NULL, NULL, NULL, NULL, 'A-12', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(289, 9, NULL, NULL, NULL, NULL, 'A-13', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(290, 9, NULL, NULL, NULL, NULL, 'A-14', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(291, 9, NULL, NULL, NULL, NULL, 'A-15', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(292, 9, NULL, NULL, NULL, NULL, 'A-16', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(293, 9, NULL, NULL, NULL, NULL, 'A-17', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(294, 9, NULL, NULL, NULL, NULL, 'A-18', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(295, 9, NULL, NULL, NULL, NULL, 'A-19', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(296, 9, NULL, NULL, NULL, NULL, 'A-20', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(297, 9, NULL, NULL, NULL, NULL, 'A-21', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(298, 9, NULL, NULL, NULL, NULL, 'A-22', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(299, 9, NULL, NULL, NULL, NULL, 'A-23', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(300, 9, NULL, NULL, NULL, NULL, 'A-24', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(301, 9, NULL, NULL, NULL, NULL, 'A-25', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(302, 9, NULL, NULL, NULL, NULL, 'A-26', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(303, 9, NULL, NULL, NULL, NULL, 'A-27', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(304, 9, NULL, NULL, NULL, NULL, 'A-28', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(305, 9, NULL, NULL, NULL, NULL, 'A-29', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(306, 9, NULL, NULL, NULL, NULL, 'A-30', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(307, 9, NULL, NULL, NULL, NULL, 'A-31', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(308, 9, NULL, NULL, NULL, NULL, 'A-32', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(309, 9, NULL, NULL, NULL, NULL, 'A-33', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(310, 9, NULL, NULL, NULL, NULL, 'A-34', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(311, 9, NULL, NULL, NULL, NULL, 'A-35', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(312, 9, NULL, NULL, NULL, NULL, 'A-36', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(313, 9, NULL, NULL, NULL, NULL, 'A-37', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(314, 9, NULL, NULL, NULL, NULL, 'A-38', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(315, 9, NULL, NULL, NULL, NULL, 'A-39', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(316, 9, NULL, NULL, NULL, NULL, 'A-40', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(317, 9, NULL, NULL, NULL, NULL, 'A-41', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(318, 9, NULL, NULL, NULL, NULL, 'A-42', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(319, 9, NULL, NULL, NULL, NULL, 'A-43', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(320, 9, NULL, NULL, NULL, NULL, 'A-44', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(321, 9, NULL, NULL, NULL, NULL, 'A-45', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL),
(322, 9, NULL, NULL, NULL, NULL, 'A-46', 0, NULL, 0, '2021-07-24 10:18:08', '2021-07-24 10:18:08', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ticket_bill`
--

CREATE TABLE `ticket_bill` (
  `ID` varchar(255) NOT NULL,
  `book_staff_id` int(11) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `journey_id` int(11) NOT NULL,
  `seat_location` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `ticket_bill`
--

INSERT INTO `ticket_bill` (`ID`, `book_staff_id`, `customer_id`, `journey_id`, `seat_location`, `status`, `created_at`, `updated_at`) VALUES
('141544628358', 1, 14, 22, 'A-8,A-9,A-7,A-4', 2, '2021-01-15 00:37:24', '2021-01-15 00:37:52'),
('281544591129', 1, 28, 22, 'A-1,A-2,A-3', 2, '2021-01-15 00:37:27', '2021-01-15 00:37:57'),
('281544712330', 1, 28, 22, 'A-2', 0, '2021-01-15 00:37:30', '2021-01-15 00:38:00'),
('301544631075', 1, 30, 22, 'A-5', 2, '2021-01-15 00:37:35', '2021-01-15 00:38:02'),
('311544704740', 1, 31, 22, 'A-1', 2, '2021-01-15 00:37:38', '2021-01-15 00:38:05'),
('321544631469', 1, 32, 22, 'A-6', 2, '2021-01-15 00:37:40', '2021-01-15 00:38:08'),
('331544706767', 1, 33, 22, 'A-1', 0, '2021-01-15 00:37:43', '2021-01-15 00:38:10'),
('331544847741', 6, 33, 22, 'A-3,A-4', 0, '2021-01-15 00:37:46', '2021-01-15 00:38:13'),
('71544704995', 1, 7, 22, 'A-1', 2, '2021-01-15 00:37:49', '2021-01-15 00:38:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `image`, `status`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ngô Thanh Thái', 'thanhthaiqngai3011@gmail.com', NULL, '$2y$10$gjxzDl5alNaI36bjzQhsKeZTP8wEFf7Wl5lOZna6t7aFicKFAqMjm', 'Tk9025U2.jpg', 1, 1, NULL, '2021-07-01 07:14:58', '2021-07-02 03:04:33'),
(5, 'Phan Trung Huy', 'phantrunghuy@gmail.com', NULL, '$2y$10$1i2F6ONZPZioja5RGkvsNeAY/UEmIIlt8KiOzSE4m7cplsHbPTEVK', NULL, 1, 1, NULL, '2021-07-02 14:55:21', '2021-07-02 03:04:20'),
(6, 'Nguyễn Văn A', 'nguyenvana@gmail.com', NULL, '$2y$10$EduTqJDbCm5T/7/aIE/zz.xTtAWa9.ZX0idCVCzWYPeNGgHYuSGzW', NULL, 1, 3, NULL, '2021-07-02 14:56:19', '2021-07-02 03:04:03'),
(7, 'Võ Ngọc Linh', 'vongoclinh@gmail.com', NULL, '$2y$10$Q0rrl6UXRkMgKCE5KPxSQe1o1sCiDTGq7cAEJOKXIeWcgX5UBLLI6', NULL, 1, 2, NULL, '2021-07-02 15:05:06', '2021-07-02 03:05:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(11) NOT NULL,
  `license_plate` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `maintenance_date` date NOT NULL,
  `maintenance_date_next` date NOT NULL,
  `location` tinytext DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `vehicle`
--

INSERT INTO `vehicle` (`id`, `license_plate`, `type`, `maintenance_date`, `maintenance_date_next`, `location`, `created_at`, `updated_at`) VALUES
(6, '51H1-34215', 6, '2021-01-15', '2021-01-30', '10.860252,106.761847', '2021-01-15 00:00:00', '2021-01-15 00:00:00'),
(7, '76A2-00011', 6, '2021-01-15', '2021-01-30', '10.859915,106.760239', '2021-01-15 00:00:00', '2021-01-15 00:00:00'),
(8, '51H1-32012', 6, '2021-01-15', '2021-01-30', '10.858756,106.759080', '2021-01-15 00:00:00', '2021-01-15 00:00:00');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `buses`
--
ALTER TABLE `buses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_driver` (`id_driver`),
  ADD KEY `id_staff` (`id_staff`),
  ADD KEY `id_province` (`id_route`) USING BTREE,
  ADD KEY `id_bus` (`id_vehicle`) USING BTREE;

--
-- Chỉ mục cho bảng `bus_model`
--
ALTER TABLE `bus_model`
  ADD PRIMARY KEY (`Mã`),
  ADD UNIQUE KEY `Tên_Loại` (`Tên_Loại`),
  ADD UNIQUE KEY `Sơ_đồ` (`Sơ_đồ`),
  ADD KEY `Mã_nhân_viên_chỉnh_sửa` (`Mã_nhân_viên_chỉnh_sửa`),
  ADD KEY `Mã_nhân_viên_tạo` (`Mã_nhân_viên_tạo`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Mã`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Sđt` (`Sđt`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `list_food`
--
ALTER TABLE `list_food`
  ADD PRIMARY KEY (`Mã`);

--
-- Chỉ mục cho bảng `menu_food`
--
ALTER TABLE `menu_food`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_news`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_province` (`name_province`) USING BTREE;

--
-- Chỉ mục cho bảng `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_staff_edit` (`id_staff_edit`),
  ADD KEY `id_staff_create` (`id_staff_create`);

--
-- Chỉ mục cho bảng `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_buses` (`id_buses`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_staff` (`id_staff`);

--
-- Chỉ mục cho bảng `ticket_bill`
--
ALTER TABLE `ticket_bill`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `license_plate` (`license_plate`),
  ADD KEY `type` (`type`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `buses`
--
ALTER TABLE `buses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `bus_model`
--
ALTER TABLE `bus_model`
  MODIFY `Mã` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `employee`
--
ALTER TABLE `employee`
  MODIFY `Mã` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `list_food`
--
ALTER TABLE `list_food`
  MODIFY `Mã` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT cho bảng `menu_food`
--
ALTER TABLE `menu_food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id_news` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT cho bảng `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `route`
--
ALTER TABLE `route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=323;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
