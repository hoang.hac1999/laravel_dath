<?php

    return [
        'template' => [
            'status' => [
                'all' => '-- Select status --',
                '0' => 'Waiting',
                '1' => 'Running',
                '2' => 'Completed',
                '3' => 'Locked'
            ],
            'sex' =>[
                'all' => '-- Chọn giới tính --',
                '1' => 'Nam',
                '0' => 'Nữ'
            ],
            'status_food'=>[
                'all' => '-- Trạng thái --',
                '1' => 'Còn món',
                '0' => 'Hết món'
            ],
            'Loại_NV'=>[
                'all' => '-- Loại nhân viên --',
                'QTV' => 'Quản trị viên',
                'TX' => 'Tài xế',
                'QLDV' => 'Quản lý đặt vé'
            ],
            'check_slide'=>[
                'all' => '--- Chế độ xem ---',
                '0' => 'Ẩn',
                '1' => 'Hiện'
            ],


        ],
        'ticket'=>[
            'status'=>[
                'all' => '--- Tình trạng vé ---',
                '0' => 'Còn trống',
                '1' => 'Đã đặt'
            ]
        ],
        'user'=>[
            'status'=>[
                'all' => '--- Trạng thái ---',
                '0' => 'Inactive',
                '1' => 'Active'
            ],
            'level'=>[
                'all' => '--- Loại user ---',
                '1' => 'Quản trị viên',
                '2' => 'Nhân viên đặt vé',
                '3' => 'Nhân viên TDC'
            ]
        ],



    ]

?>
