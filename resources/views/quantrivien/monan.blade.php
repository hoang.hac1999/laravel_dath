@extends('quantrivien.main')
@section('title')
	Quản lý Món ăn
@endsection
@section('content')
    <div class="content monan row show" style="overflow: hidden; position: relative; padding: 3em 1em 1em;">
        <h4 style="padding: .5em; position: absolute; top: 0; left: 0; width: 100%;">Bảng Món ăn</h4>
        <div id="food"></div>
        <div class="nutthaotac" style="padding-right: 0;">
            <a href="javascript:void(0)" onclick="window.open('{{url("admin/addmonan")}}')" title="Thêm Món ăn">
                <i class="glyphicon glyphicon-plus"></i>Thêm
            </a>
            <a href="javascript:void(0)" onclick="refreshMonAn()" title="Làm Mới">
                <i class="glyphicon glyphicon-refresh"></i>Reset
            </a>
            <a href="javascript:void(0)" onclick="showFull(this,'food',obj,objlen)">
                <i class="glyphicon glyphicon-resize-full"></i>
            </a>
        </div>
    </div>
@endsection
@section('excontent')
@endsection
@section('script')
    <script>
        option = document.getElementsByClassName("option");
        for (var i = 0; i < option.length; i++) {
            option[i].classList.remove('selected');
        }
        option[4].classList.add('selected');
        option[4].getElementsByTagName('img')[0].setAttribute('src','{{asset("images/icons/partnership-hover.png")}}');
        var obj = {
            width: '100%',
            height: '100%',
            showTop: false,
            showBottom: false,
            collapsible: false,
            showHeader: true,
            filterModel: {on: true, mode: "AND", header: true},
            // scrollModel: {autoFit: true},
            resizable: false,
            roundCorners: false,
            rowBorders: true,
            columnBorders: false,
            postRenderInterval: -1,
            selectionModel: { type: 'row', mode: 'single' },
            hoverMode: 'row',
            numberCell: { show: true, title: 'STT', width: 50, align: 'center'},
            stripeRows: true,
            freezeCols: 1,
            cellDblClick: function (event,ui) {
                window.open("{{url('/admin/addmonan')}}" + "/" + ui.rowData["Mã"]);
            }
        };
        obj.colModel = [
            {
                title: "Thao tác",
                width: 100,
                editor: false,
                dataIndx: "View",
                align: 'center',
                render: function (ui) {
                    var str = '';
                    str += '<a title="Edit" id="idEditFood" ><i class="glyphicon glyphicon-edit  text-success" style="padding-right: 5px; cursor: pointer;"></i></a>';
                    str += '<a title="Delete" id="idDelFood" ><i class="glyphicon glyphicon-remove  text-danger" style="padding-right: 5px; cursor: pointer;"></i></a>';
                    return str;
                },
                postRender: function (ui) {
                    var rowData = ui.rowData,
                        $cell = this.getCell(ui);
                    //add button
                    $cell.find("a#idEditFood")
                        .unbind("click")
                        .bind("click", function (evt) {
                            window.open("{{url('admin/addmonan')}}"+"/"+rowData["Mã"]);
                        });
                    $cell.find("a#idDelFood")
                        .unbind("click")
                        .bind("click", function (evt) {
                            if(confirm("Bạn chắc chắn muốn xóa?"))
                                location.assign("{{url('admin/delmonan')}}"+"/"+rowData["Mã"]);
                        });
                }
            },
            
            {
                title: "Tên món ăn",
                width: 150,
                dataIndx: "Ten_mon_an",
                dataType: "string",
                editor: false,
                align: 'center',
                filter: {
                    type: 'textbox',
                    attr: 'placeholder="Tìm theo Tên"',
                    cls: 'filterstyle',
                    condition: 'contain',
                    listeners: ['keyup']
                }
            },
            {
                title: "Mô tả",
                width: 150,
                dataIndx: "Mo_ta",
                dataType: "string",
                editor: false,
                align: 'center',
                // filter: {
                //     type: 'textbox',
                //     attr: 'placeholder="Tìm theo Tên"',
                //     cls: 'filterstyle',
                //     condition: 'contain',
                //     listeners: ['keyup']
                // }
            },
            {
                title: "Giá",
                width: 200,
                dataIndx: "gia",
                dataType: "string",
                editor: false,
                align: 'center',
                render: function (ui) {
                    switch(ui.rowData["gia"]){
                        case "30":
                            return "30.000 vnđ";
                            break;
                        case "40":
                            return "40.000 vnđ";
                            break;
                        case "50":
                            return "50.000 vnđ";
                            break;
                        default:
                            break;
                    }
                },
                filter: {
                    type: "select",
                    condition: "equal",
                    options: [
                        {"":"[All]"},
                        {"30":"30.000 VNĐ"},
                        {"40":"40.000 VNĐ"},
                        {"50":"50.000 VNĐ"}
                    ],
                    listeners: ["change"]
                }
            },
            {
                title: "Trạng thái",
                width: 150,
                dataIndx: "trang_thai",
                dataType: "string",
                editor: false,
                align: 'center',
                render: function (ui) {
                    switch (ui.rowData["trang_thai"]){
                        case "Hết":
                            return "Hết món";
                            break;
                        case "Còn":
                            return "Còn món";
                            break;
                        
                        default:
                            break;
                    }
                },
                filter: {
                    type: "select",
                    condition: "equal",
                    options: [
                        {"":"[All]"},
                        {"Còn":"Còn món"},
                        {"Hết":"Hết món"}
                    ],
                    listeners: ["change"]
                }
            },
            // {
            //     title: "Ảnh minh họa",
            //     width: 150,
            //     dataIndx: "img",
            //     dataType: "string",
            //     editor: false,
            //     align: 'center',
            //     render: function(ui){
			// 		return "<a href='"+"{{asset("/upload")}}"+"/"+ui.rowData['img']+"' target='_blank'><i class='glyphicon glyphicon-eye-open' style='color: green;'></i></a>";
			// 	}
            // },
        ];
        var objlen = 0;
        for(var i =0; i<obj.colModel.length;i++){
            objlen+=obj.colModel[i].width;
        }
        $(function () {
            obj.dataModel = {
                data: {!! json_encode($food) !!},
                location: "local",
                sorting: "local",
                sortDir: "down"
            };
            obj.pageModel = {type: 'local', rPP: 50, rPPOptions: [50, 100, 150, 200]};
            var $grid = $("#food").pqGrid(obj);
            if(objlen <= document.getElementById('food').offsetWidth){
                $grid.pqGrid('option','scrollModel',{autoFit: true}).pqGrid("refreshDataAndView");
            }
            else{
                $grid.pqGrid('option','scrollModel',{horizontal: true,autoFit: false,flexContent: true}).pqGrid("refreshDataAndView");
            }
        });
        function refreshMonAn(){
            $.ajax({
                type:'POST',
                url:'{{asset("/admin/retrievedata")}}',
				data: {
					_token: '{{csrf_token()}}',
					typedata: 'monan'
				},
                success:function(data){
                    if (data.kq == 1) {
                        obj.dataModel = {
                            data: data.data,
                            location: "local",
                            sorting: "local",
                            sortDir: "down"
                        };
                        obj.pageModel = {type: 'local', rPP: 50, rPPOptions: [50, 100, 150, 200]};
                        var $grid = $("#food").pqGrid(obj);
                        if(objlen <= document.getElementById('food').offsetWidth){
                            $grid.pqGrid('option','scrollModel',{autoFit: true}).pqGrid("refreshDataAndView");
                        }
                        else{
                            $grid.pqGrid('option','scrollModel',{horizontal: true,autoFit: false,flexContent: true}).pqGrid("refreshDataAndView");
                        }
						$("#food").pqGrid("reset",{filter : true});
                    }                   
                },
				timeout: 10000,
				error: function(xhr){
					if(xhr.statusText == "timeout")
					{
						$("#alertmessage .modal-body").html("Vui lòng kiểm tra kết nối!");
						$("#alertmessage").modal("show");
					}
				}
            });
        }
        function showFull(ev,id,obj,s){
            if(ev.getElementsByTagName("i")[0].classList.contains("glyphicon-resize-full")){
                ev.getElementsByTagName("i")[0].classList.remove("glyphicon-resize-full");
                ev.getElementsByTagName("i")[0].classList.add("glyphicon-resize-small");
                document.getElementById(id).style.position = "fixed";
                document.getElementById(id).style.width = "100%";
                document.getElementById(id).style.height = "100%";
                document.getElementById(id).style.top = "0";
                document.getElementById(id).style.left = "0";
                document.getElementById(id).style.paddingTop = "3.45em";
                document.getElementsByClassName("nutthaotac")[0].style.top = "calc(100% - 3em)";
                var $grid = $("#"+id).pqGrid(obj);
                if(s <= document.getElementById(id).offsetWidth){
                    $grid.pqGrid('option','scrollModel',{autoFit: true}).pqGrid("refreshDataAndView");
                }
                else{
                    $grid.pqGrid('option','scrollModel',{horizontal: true,autoFit: false,flexContent: true}).pqGrid("refreshDataAndView");
                }
            }
            else{
                ev.getElementsByTagName("i")[0].classList.remove("glyphicon-resize-small");
                ev.getElementsByTagName("i")[0].classList.add("glyphicon-resize-full");
                document.getElementById(id).style.position = "relative";
                document.getElementById(id).style.width = "100%";
                document.getElementById(id).style.height = "auto";
                document.getElementById(id).style.paddingTop = "0";
                document.getElementsByClassName("nutthaotac")[0].style.position = "absolute";
                document.getElementsByClassName("nutthaotac")[0].style.top = ".4em";
                var $grid = $("#"+id).pqGrid(obj);
                if(s <= document.getElementById(id).offsetWidth){
                    $grid.pqGrid('option','scrollModel',{autoFit: true}).pqGrid("refreshDataAndView");
                }
                else{
                    $grid.pqGrid('option','scrollModel',{horizontal: true,autoFit: false,flexContent: true}).pqGrid("refreshDataAndView");
                }
            }
        }
    </script>
@endsection
