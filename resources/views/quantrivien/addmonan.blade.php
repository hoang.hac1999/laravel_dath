@extends('quantrivien.main')
@section('title')
	@if(isset($ttmonan))
		Chỉnh sửa món ăn
	@else
		Thêm món ăn
	@endif
@endsection
@section('content')
    <style>
        .row > *:nth-child(2) {
            text-align: left;
        }
		.header .row > *:nth-child(2) {
            text-align: center;
        }
		#nv_doimatkhau .row * {
			text-align: left;
		}
    </style>
    <div class="content show row" id="addmonan">
        <div class="col-lg-3">
        </div>
        <form class="col-lg-6" name="ttmonan" action="{{route('addfood')}}" method="post">
            @csrf
            <fieldset>
                <legend><?php echo isset($ttmonan)? 'Sửa món ăn':'Thêm món ăn';?></legend>
                @isset($ttmonan)
                    <?php
                    $ttmonan = (array)$ttmonan;
                    ?>
                    <input type="hidden" name="ID" value="{{$ttmonan['Mã']}}">
                @endisset
				<div class="form-group col-lg-6">
					<label>Tên món ăn<i class="text text-danger">*</i></label>
					<input type="text" class="form-control" name="name" value="{{isset($ttmonan['Ten_mon_an'])? $ttmonan['Ten_mon_an']:''}}" placeholder="Tên món ăn" required>
                </div>
				<div class="form-group col-lg-6">
					<label>Mô tả<i class="text text-danger">*</i></label>
					<input type="text" class="form-control" name="desc" value="{{isset($ttmonan['Mo_ta'])? $ttmonan['Mo_ta']:''}}" placeholder="Mô tả" required>
                </div>
				<div class="form-group col-lg-12">
					<label>Giá<i class="text text-danger">*</i></label>
					<br>
					<input type="radio" class="form-inline" name="price" value="30" <?php if(!isset($ttmonan)||$ttmonan['gia']=='30') echo "checked";?>> 30.000đ <br>
					<input type="radio" class="form-inline" name="price" value="40" <?php if(isset($ttmonan)&&$ttmonan['gia']=='40') echo "checked";?>> 40.000đ <br>
					<input type="radio" class="form-inline" name="price" value="50" <?php if(isset($ttmonan)&&$ttmonan['gia']=='50') echo "checked";?>> 50.000đ
                </div>
				<div class="form-group col-lg-6">
					<label>Trạng thái<i class="text text-danger">*</i></label>
					<br>
					<input type="radio" class="form-inline" name="status" value="30" <?php if(!isset($ttmonan)||$ttmonan['trang_thai']=='Hết') echo "checked";?>> Hết món <br>
					<input type="radio" class="form-inline" name="status" value="40" <?php if(isset($ttmonan)&&$ttmonan['gia']=='Còn') echo "checked";?>> Còn món <br>
                </div>
				<div class="form-group col-lg-6">
					<label>Hình ảnh<i class="text text-danger">*</i></label>
					<input type="file" class="form-control" name="img" value="{{isset($ttmonan['img'])? $ttmonan['img']:''}}" placeholder="Hình ảnh" required>
                </div>
                <div style="text-align: center; clear: both;">
                    <input type="submit" class="btn btn-success" name="submit" value="<?php echo isset($ttmonan)? 'Sửa Thông Tin':'Thêm Món ăn';?>">
					<!--  -->
                </div>
            </fieldset>
        </form>
        <div class="col-lg-3"></div>
    </div>
@endsection

@section('script')
    <script>
        option = document.getElementsByClassName("option");
        for (var i = 0; i < option.length; i++) {
            option[i].classList.remove('selected');
        }
        option[4].classList.add('selected');
        option[4].getElementsByTagName('img')[0].setAttribute('src','{{asset("images/icons/partnership-hover.png")}}');
    </script>
	@if(isset($ttmonan))
		<script>
			document.forms["ttmonan"]["submit"].onclick = function(ev){
				var id = document.forms["ttmonan"]["ID"];
				var name = document.forms["ttmonan"]["name"];
				var desc = document.forms["ttmonan"]["desc"];
				var gia = document.forms["ttmonan"]["price"];
				var status = document.forms["ttmonan"]["status"];
				var img = document.forms["ttmonan"]["img"];
				
				
				var format_name = /[a-zA-Z][^#&<>\"~;$^%{}?]{1,50}$/;
				var str = "";
				name.style.borderColor = "#ccc";
				desc.style.borderColor = "#ccc";
				gia.style.borderColor = "#ccc";
				status.style.borderColor = "#ccc";
				
				if(name.value == "")
				{
					name.style.borderColor = "red";
					str += "Tên không được để trống!<br>";
				}
				else
				{
					if(name.value.search(format_name) == -1)
					{
						name.style.borderColor = "red";
						str += "Tên không đúng định dạng!<br>";
					}
				}
				if(desc.value == "")
				{
					desc.style.borderColor = "red";
					str += "Tên món ăn không được để trống!<br>";
				}
				if(gia.value == "")
				{
					gia.style.borderColor = "red";
					str += "Gía không được để trống!<br>";
				}
				
				if(status.value == "")
				{
					status.style.borderColor = "red";
					str += "Trạng thái không được để trống!<br>";
				}
				if(str != "")
				{
					ev.preventDefault();
					$("#alertmessage .modal-body").html(str);
					$("#alertmessage").modal();
				}
			};
			
		</script>
	@else
		<script>
			document.forms["ttmonan"]["submit"].onclick = function(ev){
				var name = document.forms["ttmonan"]["name"];
				var desc = document.forms["ttmonan"]["desc"];
				var status = document.forms["ttmonan"]["status"];
				var gia = document.forms["ttmonan"]["price"];
				var img = document.forms["ttmonan"]["img"];
				
				
				var format_name = /[a-zA-Z][^#&<>\"~;$^%{}?]{1,50}$/;
				
				var str = "";
				name.style.borderColor = "#ccc";
				desc.style.borderColor = "#ccc";
				status.style.borderColor = "#ccc";
				gia.style.borderColor = "#ccc";
				img.style.borderColor = "#ccc";
				
				if(name.value == "")
				{
					name.style.borderColor = "red";
					str += "Tên không được để trống!<br>";
				}
				else
				{
					if(name.value.search(format_name) == -1)
					{
						name.style.borderColor = "red";
						str += "Tên không đúng định dạng!<br>";
					}
				}
				if(desc.value == "")
				{
					desc.style.borderColor = "red";
					str += "Tên món ăn không được để trống!<br>";
				}
				if(gia.value == "")
				{
					gia.style.borderColor = "red";
					str += "Gía không được để trống!<br>";
				}
				
				if(status.value == "")
				{
					status.style.borderColor = "red";
					str += "Trạng thái không được để trống!<br>";
				}
				
				if(str != "")
				{
					ev.preventDefault();
					$("#alertmessage .modal-body").html(str);
					$("#alertmessage").modal();
				}
			};
		</script>
	@endif
@endsection
