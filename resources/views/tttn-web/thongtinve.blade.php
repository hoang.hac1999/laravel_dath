@extends('tttn-web.main')
@section('title')
    Trang chủ
@endsection
@section('content')
    <!-- Thông tin vé -->
    <div class="mainthongtinve">
        <h3>Thông Tin Vé Đã Đặt</h3>
            <?php $dem=0;
                $tien= 0;
                $tienmon=0;
                $total_money=0;
            ?>
            @foreach($chonve as $t)
            <p><i class="fa fa-bus"></i> Nơi Khởi Hành:<a> {{$t->from}}</a></p><br>
            <p><i class="fa fa-bus"></i> Nơi đến: <a>{{$t->to}}</a></p> <br>
            <p><span class="glyphicon glyphicon-time"></span> Thời gian khởi hành: {{$t->date_start}} : {{$t->time_start}}</p><br>
           
            <p><i class="fa fa-balance-scale"></i> Giá vé : {{number_format($t->ticket_price)}} VNĐ </p><br>
            
            <?php $tien=$t->ticket_price; ?>
            @endforeach
           
            <p><i class="fa fa-address-card-o"></i> Vé đã đặt:
           
            @foreach($vedadat as $a)
                
                <b>{{$a->name_seat}}</b>,
                <?php  $dem++; ?>
            @endforeach
            
             </p><br>
             <p><i class="fa fa-cutlery"></i> Món :
                 @foreach($mondachon as $m)
                    <b>{{$m->name}} <i>x{{$m->quatity}}</i></b>,
                    <?php 
                        $tienmon+=$m->price*$m->quatity;
                        
                    ?>
                 @endforeach 
            </p><br> 
            <?php
            $total_money = $tienmon + $dem * $tien;
            session()->put('total_money', $total_money)
            ?>
            <p><i class="fa fa-gavel"></i> Tổng tiền: <b>{{number_format( $total_money)  }} VNĐ</b> </p></divbr>
            <a href="{{route('payment')}}"><button type="button" class="btn btn-primary" name="payment" type="submit">Thanh Toán với VNPAY</button>     </a> 
            <!-- <h4 style="color: rgb(0,64,87);">Nhân viên chúng tôi sẻ liên hệ với quý khách để quý khách thanh toán tiền.</h4>
            <h3 style="color: rgb(0,64,87);">Chúc quý khách có một chuyến đi vui vẻ.</3> -->
           
    </div>
    
@endsection
@section('script')
    <script type="text/javascript" src="js/js.js"></script>
    <script type="text/javascript" src="js/date.js"></script>
@endsection
