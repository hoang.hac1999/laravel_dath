@extends('tttn-web.main')
@section('title')
    Chọn vé
@endsection
@section('content')
    <!-- Thêm sự kiện onload vào thẻ td  -->
      <script>
          $(function(){
            $('td[onload]').trigger('onload');
          });
      </script>
    <!-- Phần bước  -->
      <div class="buoc">
          <ul>
              <li onclick="vetrangtruoc2()" class="tay">Tìm Chuyến</li>
              <li onclick="vetrangtruoc()" class="tay">Chọn Vé</li>
              <li style="background: #f57812; color: #FFF;" class="stay tay">Chi Tiết vé</li>
          </ul>
      </div>
    <!-- kết thức phần bước  -->
    <!-- Phần chọn vé  -->
    <div class="chonvemain">
    <div class="chonveleft">
              <?php $makh=Session::get('makh'); 
                    
              ?>
              @foreach($chonve as $t)
              <h3>Thông tin vé</h3>
              <p><i class="fa fa-bus"></i> Nơi Khởi Hành: <a>{{$t->from}}</a></p><br>
              <p><i class="fa fa-bus"></i> Nơi đến: <a>{{$t->to}}</a></p> <br>
              <p><span class="glyphicon glyphicon-time"></span> Ngày đi: {{$t->date_start}}  {{$t->time_start}}</p><br>
              
              <p><i class="fa fa-balance-scale"></i> Giá vé: {{number_format($t->ticket_price)}} VNĐ</p><br>
              <p><i class="fa fa-address-card-o"></i> Vé đang chọn: <b class="vedangchon">
                  @foreach($ve as $v)
                    @if($v->status == 2 && $v->id_customer == $makh )
                        {{$v->name_seat}}
                    @endif
                  @endforeach
              </b> </p><br>
              <a data-toggle="modal" data-target="#chonmon"><p><i class="fa fa-cutlery"></i> Chọn món ăn</p><br></a>
              <button type="button" style="background: #f57812; border: none;" class="btn btn-success chondatve"  data-id={{$id}}>Đặt vé</button>      
          </div>
          @endforeach
          <div class="chonvecenter">
          <div class="tengiuong"><h3>Sơ đồ xe</h3></div>
                <div class="chuygiuong">
                      <ul>
                        <li><i class="loaighetrong"></i> &nbsp;Còn trống</li>
                        <li><i class="loaighechon"></i> &nbsp;Đang chọn</li>
                        <li><i class="loaigheban"></i> &nbsp;Đã bán</li>
                        <li><i class="loaighecochon"></i> &nbsp;Có Người Chọn</li>
                       
                      </ul>
                </div>
                <div class="sodoghe">
                      <!-- Sơ đồ xe ngồi -->
                         <table class="bangve">
                              <?php  $sd = $sodo[0]->Sơ_đồ; $dem=0; ?>
                              @for($i=0;$i<12;$i++)
                              <tr>
                                  @for($j=0;$j<6;$j++)
                                      @if($sd[$i * 6 + $j]==1 && ($i * 6 + $j)==0)
                                          <td class="ghetaixe" title="Ghế tài xế">
                                            <img src="../images/ghetaixe2.png">
                                          </td>
                                      @elseif($sd[$i * 6 + $j] == 1)
                                          @if($ve[$dem]->status == 1)
                                          <td class="ghe" title="Ghế đã bán cho khách" data-ma={{$ve[$dem]->id}} />
                                              <img src="../images/ghe.png"/>
                                               
                                              <div class="content">{{$ve[$dem]->name_seat}}</div></td>
                                          @elseif($ve[$dem]->status ==0)
                                              <td class="ghecontrong" title="Ghế trống" data-vitri={{$ve[$dem]->name_seat}} data-ma={{$ve[$dem]->id}}><img src="../images/ghe.png">
                                                <p class="text1" type="text"></p>
                                                  <div class="content">{{$ve[$dem]->name_seat}}</div></td>
                                          @elseif($ve[$dem]->status == 2)
                                              @if($ve[$dem]->id_customer == Session::get('makh'))
                                                  <td class="ghedangchon" title="Ghế Đang Chọn" onload="demnguoc2({{$ve[$dem]->id}},{{$ve[$dem]->TG}})" data-ma={{$ve[$dem]->id}}><img src="../images/ghe.png">
                                                     <p class="text1" type="text"  ></p><div class="content">{{$ve[$dem]->name_seat}}</div></td>

                                              @else
                                                   <td class="ghecochon" title="Đã Có Người Chọn" onload="demnguoc1({{$ve[$dem]->id}},{{$ve[$dem]->TG}})" data-ma={{$ve[$dem]->id}}><img src="../images/ghe.png">
                                                     <p class="text1"  type="text"  ></p><div class="content">{{$ve[$dem]->name_seat}}</div></td>

                                              @endif
                                          @endif
                                          <?php $dem++; ?>
                                      @else
                                          <td class="ghetrong"></td>
                                      @endif
                                  @endfor
                              </tr>
                              @endfor
                      </table>
                  <!-- Kết thúc sơ đồ xe ngồi -->
              </div>
          </div>
        <!-- kết thưc thông tin chuyến xe -->
    </div>
   
    <!-- kết thúc phần chọn vé -->
@endsection
@section('excontent')
<div id="khongcosan" class="modal fade">
        <div class="modal-dialog" style="width: 400px;">
            <div class="modal-content">
                <div class="modal-header" style="background: rgb(0,64,87); color: #FFF; text-align: center;">
                    <button class="close dongthongbao" data-dismiss="modal" style="color: white;opacity: 1;">&times;</button>
                    <h4 class="modal-title">Thông báo</h4>
                </div>
                <div class="modal-body">
                   
                </div>
            </div>
        </div>
    </div>
 
@endsection
@section('script')
    <script type="text/javascript">
        function vetrangtruoc(){
            history.back();
        }
         function vetrangtruoc2(){
            history.back();
             history.back();
        }
        mang=[];
       /*đếm thời gian giữ vé*/
            function demnguoc(ma,thoigian){
                thoigian = thoigian -1;
                if(thoigian !=-1){
                 $("td[data-ma='"+ma+"'] .text1").html(thoigian + "s");
                  setTimeout("demnguoc("+ma+","+thoigian+")",1000);
                }
                else{
                  $("td[data-ma='"+ma+"'] .text1").hide();
                  
                }
            }
            function demnguoc1(ma,thoigian){
               /*$("td[data-ma='"+ma+"'] .text1").show();*/
              ma = $("td[data-ma='"+ma+"']").attr("data-ma");
                thoigian = thoigian -1;
                if(thoigian !=-1){
                 $("td[data-ma='"+ma+"'] .text1").html(thoigian + "s");
                  setTimeout("demnguoc1("+ma+","+thoigian+")",1000);
                }
                else{
                  $("td[data-ma='"+ma+"'] .text1").hide();
                    $("td[data-ma='"+ma+"']").addClass("ghecontrong");
                    $("td[data-ma='"+ma+"']").removeClass("ghecochon");

                }
            }
             function demnguoc2(ma,thoigian){
              /*$("td[data-ma='"+ma+"'] .text1").show();*/
              ma = $("td[data-ma='"+ma+"']").attr("data-ma");
                thoigian = thoigian -1;
                if(thoigian !=-1){
                 $("td[data-ma='"+ma+"'] .text1").html(thoigian + "s");
                  setTimeout("demnguoc1("+ma+","+thoigian+")",1000);
                }
                else{
                  $("td[data-ma='"+ma+"'] .text1").hide();
                    $("td[data-ma='"+ma+"']").addClass("ghecontrong");
                    $("td[data-ma='"+ma+"']").removeClass("ghedangchon");

                }
            }
        /*Xử lý đặt ghế*/
        function datghe(bien,ma,vitri){
                makh = {{Session::get('makh')}};
                     bien.addClass("ghedangchon");
                     bien.removeClass("ghecontrong");
                     mang.push(ma);
                      $("td[data-ma='"+ma+"'] .text1").show();
                     demnguoc(ma,600);
                     $(".vedangchon").append("<b> "+vitri+"</b>");
                    $.ajax({
                        url: '{{route("process-book")}}',
                        type: 'POST',
                        data: {
                            _token: '{{csrf_token()}}',
                            MA: ma,
                            MAKH: makh
                        },
                        success: function (data) {
                           if(data.kq == 0){
                            bien.addClass("ghecontrong");
                            bien.removeClass("ghedangchon");
                            for (i=0; i < mang.length; i++) {
                               if(mang[i]==ma){
                                    mang[i]=null;
                                    break;
                               }
                           }
                           }
                           else if(data.kq==1){
                            bien.addClass("ghe");
                            bien.removeClass("ghedangchon");
                            for (i=0; i < mang.length; i++) {
                               if(mang[i]==ma){
                                    mang[i]=null;
                                    break;
                               }
                           }
                            $("#khongcosan .modal-body").html("Ghế đã có người mua!");
                           $("#khongcosan").modal("show"); 
                           }
                           else if(data.kq == 2){
                            time= data.TGC;
                            bien.addClass("ghecochon");
                            bien.removeClass("ghedangchon");
                            demnguoc(ma,time);
                            for (i=0; i < mang.length; i++) {
                               if(mang[i]==ma){
                                    mang[i]=null;
                                    break;
                               }
                           }
                            $("#khongcosan .modal-body").html("Ghế Đã có người đặt!");
                           $("#khongcosan").modal("show"); 
                           }
                        }
                 });
            }
        
        $(document).ready(function(){
          /*kích chọn vé ghế*/
             $(".bangve").delegate(".ghecontrong","click",function(){
                    ma = $(this).attr("data-ma");
                    vitri = $(this).attr("data-vitri");
                    bien = $(this);
                    datghe(bien,ma,vitri);
                });
          /*kích hủy vé ghế*/
              $(".bangve").delegate(".ghedangchon","click",function(){
                    ma = $(this).attr("data-ma");
                    bien = $(this);
                    $.ajax({
                        url: '{{route("process-cancel")}}',
                        type: 'POST',
                        data: {
                            _token: '{{csrf_token()}}',
                            MA: ma,
                        },
                        success: function (data) {
                           if(data.kq == 1){
                            bien.addClass("ghecontrong");
                            bien.removeClass("ghedangchon");
                             /*$("td[data-ma='"+ma+"']").off('click',demnguoc);*/
                            $("td[data-ma='"+ma+"'] .text1").hide();
                           for (i=0; i < mang.length; i++) {
                               if(mang[i]==ma){
                                    mang[i]=null;
                                    break;
                               }
                           }
                           location.reload();
                           }
                        }
                 });
            });
          
            /*xử lý đặt vé*/
            $(".chondatve").click(function(){
                    id = $(this).attr("data-id");
                    makh ={{Session::get('makh')}};
                    dodai = mang.length;
                    gt=""; 
                    sl="";
                    var today = new Date();
                   
                    ran =   today.getFullYear()+
                        ("00" + (today.getMonth() + 1)).slice(-2)+
                        ("00" + today.getDate()).slice(-2) + 
                        ("00" + today.getHours()).slice(-2) + 
                        ("00" + today.getMinutes()).slice(-2) + 
                        ("00" + today.getSeconds()).slice(-2);
                        
                  
                   
                    // ran=Math.floor(Math.random() * 9999999999); 
                    var giatri = $(".chonmon");
                  
                        for (var i = 0; i < giatri.length; i++){
                            if (giatri[i].value > 0){
                               sl = giatri[i].value;
                               gt= giatri[i].getAttribute("data-idmon");
                               $.ajax({
                                url: '{{route("pick")}}',
                                type: 'POST',
                                data: {
                                    _token: '{{csrf_token()}}',
                                    ID: id,
                                    MANG:mang,
                                    MAKH:makh,
                                    CHONMON:gt,
                                    SL: sl,
                                    DODAI:dodai,
                                    RAN:ran,
                                },
                                success: function (data) {
                                location.assign("{{asset('finish')}}/"+id+"/"+makh+"/"+ran);
                                }
                                });
                            } 
                            else {
                                $.ajax({
                                url: '{{route("pick")}}',
                                type: 'POST',
                                data: {
                                    _token: '{{csrf_token()}}',
                                    ID: id,
                                    MANG:mang,
                                    MAKH:makh,
                                    DODAI:dodai,
                                    RAN:ran,
                                },
                                success: function (data) {
                                location.assign("{{asset('finish')}}/"+id+"/"+makh+"/"+ran);
                                }
                                });
                            }
                        }
               });
               
            /*xử lý chọn tầng*/
               $(".tren").click(function(){
                    $(".tren").css({"background":"#f57812","color":"#FFF"});
                    $(".duoi").css({"background":"#CCC","color":"#000"});
                    $(".tangtren").show();
                    $(".tangduoi").hide();
               });
               $(".duoi").click(function(){
                  $(".duoi").css({"background":"#f57812","color":"#FFF"});
                  $(".tren").css({"background":"#CCC","color":"#000"});
                  $(".tangduoi").show();
                  $(".tangtren").hide();
               });
            /*kết thúc xử lý chọn tầng*/
        });
            $('#myForm input').on('change', function() {
                       var chonmon = document.getElementsByName("chonmon");
                    
                    
                    
            });
           



    
       
  </script>
@extends('tttn-web.chonmon')
@endsection
</script>
