@extends('tttn-web.main')
@section('title')
    Trạng thái thanh toán
@endsection
@section('content')
<style>
.menu,.btn{
    display: none;
}
</style>
<div class="mainthongtinve">
    <h3><b>TRẠNG THÁI THANH TOÁN</b></h3>
        <!--Begin display -->
        @foreach($sql as $v)
       
            
            <div class="table-responsive">
                <div class="form-group">
                    <label >Mã đơn hàng:</label>

                    <label>{{$v->order_id}}</label>
                </div>    
                <div class="form-group">

                    <label >Số tiền:</label>
                    <label style="color:red"> {{number_format($v->money)}}</label>
                </div>  
                <div class="form-group">
                    <label >Số điện thoại liên hệ:</label>
                    <label style="color:red">{{$v->note}}</label>
                </div> 
                <!-- <div class="form-group">
                    <label >Mã phản hồi (vnp_ResponseCode):</label>
                    <label>{{$v->vnp_response_code}}</label>
                </div>  -->
                <div class="form-group">
                    <label >Mã GD Tại VNPAY:</label>
                    <label>{{$v->code_vnpay}}</label>
                </div> 
                <div class="form-group">
                    <label >Mã Ngân hàng:</label>
                    <label>{{$v->code_bank}}</label>
                </div> 
                <div class="form-group">
                    <label >Thời gian thanh toán:</label>
                    <label>{{$v->time}}</label>
                </div> 
                <div class="form-group">
                    <label >Kết quả:</label>
                    @if (Session::has('success'))
                    <label style="color:red">
                       {{session()->get('success')}}
                    @else
                   {{session()->get('errors')}}
                    </label>
                    @endif
                </div> 
            </div>
            <p>
                &nbsp;
            </p>
       
        @endforeach
       
            <h3><?php echo session()->get('errors') ?></h3>  
       
</div>
@endsection