@php
if (isset(Auth::user()->image)) {
    $src = Auth::user()->getImage();
} else {
    $src = asset('images/user.png');
}
@endphp

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ $src }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column  nav-list" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('admin.user.index') }}" class="nav-link {{ $controllerName == 'user' ? 'active' : '' }}">
              <i class="fas fa-user" style="padding-right: 5px;"></i>
              <p>
                Quản lý user
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.lotrinh.index') }}" class="nav-link {{ $controllerName == 'lotrinh' ? 'active' : '' }}">
              <i class="fas fa-route" style="padding-right: 5px;"></i>
              <p>
                Quản lý lộ trình
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.chuyenxe.index') }}" class="nav-link {{ $controllerName == 'chuyenxe' ? 'active' : '' }}">
              <i class="fas fa-bus" style="padding-right: 5px;"></i>
              <p>
                Quản lý chuyến xe
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.khachhang.index') }}" class="nav-link {{ $controllerName == 'khachhang' ? 'active' : '' }}">
                <i class="fa fa-users" style="padding-right: 5px;"></i>
              <p>
                Quản lý khách hàng
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.vexe.index') }}" class="nav-link {{ $controllerName == 'vexe' ? 'active' : '' }}">
              <i class="fas fa-ticket-alt" style="padding-right: 5px;"></i>
              <p>
                Quản lý vé xe
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.menu.index') }}" class="nav-link {{ $controllerName == 'menu' ? 'active' : '' }}">
              <i class="fas fa-utensils" style="padding-right: 5px;"></i>
              <p>
                Quản lý thực đơn
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.nhanvien.index') }}" class="nav-link {{ $controllerName == 'nhanvien' ? 'active' : '' }}">
              <i class="far fa-user" style="padding-right: 5px;"></i>
              <p>
                Quản lý nhân viên
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.payment.index') }}" class="nav-link {{ $controllerName == 'payment' ? 'active' : '' }}">
              <i class="fab fa-paypal" style="padding-right: 5px;"></i>
              <p>
                Quản lý thanh toán
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.tintuc.index') }}" class="nav-link {{ $controllerName == 'tintuc' ? 'active' : '' }}">
              <i class="far fa-newspaper" style="padding-right: 5px;"></i>
              <p>
                Quản lý tin tức
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

