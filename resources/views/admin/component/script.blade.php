<!-- jQuery -->
<script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<script src="{{ asset('admin/dist/js/style.js') }}"></script>
<script src="{{ asset('admin/toastr-master/build/toastr.min.js') }}"></script>
<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/ckfinder/ckfinder.js') }}"></script>
<script>
    CKEDITOR.replace('ck_content', {
        filebrowserBrowserUrl: '/admin/ckfinder/ckfinder.html',
        filebrowserUploadUrl: '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
    });
</script>

@if(\Session::has('success'))
    <script>
        toastr.success('{{ \Session::get("success") }}', 'Thành công');
    </script>
@elseif(\Session::has('error'))
    <script>
        toastr.error('{{ \Session::get("error") }}', 'Thất bại');
    </script>
@endif

