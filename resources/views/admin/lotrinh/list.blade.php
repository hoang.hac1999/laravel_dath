@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level !== 3)
                        <th>Action</th>
                    @endif
                @endif
                <th>Mã lộ trình</th>
                <th>Nhân viên tạo</th>
                <th>Nhân viên sửa</th>
                <th>Nơi đi</th>
                <th>Nơi đến</th>
                <th>Số trạm dừng</th>
                <th>Estime</th>
                <th>Ngày tạo</th>
                <th>Ngày sửa</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        $departure = $item->from;
                        $destination = $item->to;
                        //$staff_create = $item->nhanvien_qt->Họ_Tên;
                        //$staff_fix = $item->nhanvien_qt->Họ_Tên;
                        $staff_create = $item->id_staff_create;
                        $staff_fix = $item->id_staff_edit;
                        $bus_station = $item->bus_station;
                        $estime = $item->estime;
                        $created_at = $item->created_at;
                        $updated_at = $item->updated_at;
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level !== 3)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{!! $id !!}</td>
                        <td>{{ $staff_create }}</td>
                        <td>{{ $staff_fix }}</td>
                        <td>{{ $departure }}</td>
                        <td>{{ $destination }}</td>
                        <td>{{ $bus_station }}</td>
                        <td>{{ $estime }}</td>
                        <td>{!! $created_at !!}</td>
                        <td>{!! $updated_at !!}</td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
