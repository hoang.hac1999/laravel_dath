@php
$select_status = Form::select('form[status]', config('myconfig.template.status'), @$item['status'], ['class' =>
'form-control']);
$employee_qt = \App\Nhanvien::where('Loại_NV','LIKE' ,'Q%')->orderBy('Mã', 'ASC')->get()->pluck('Họ_Tên','Mã');

$select_employee_create = Form::select('form[id_staff_create]', $employee_qt, @$item['id_staff_create'], ['class' => 'form-control']);
$select_employee_fix = Form::select('form[id_staff_edit]', $employee_qt, @$item['id_staff_edit'], ['class' => 'form-control']);
@endphp

@extends('admin.layout')

@section('content')
@include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card text-center">
                    @include($pathView . 'element.groupBtnForm')
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card " style="height: 460px; overflow-y:scroll;">
                    <form action="#" method="POST" id="frmSave" enctype="multipart/form-data">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                aria-controls="meta" aria-selected="false">Meta</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home">
                                            <div class="row" style="padding-top: 20px">
                                                @if ($errors->any())
                                                <div class="col-md-12">
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Nhân viên tạo</label>
                                                        {!! $select_employee_create !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nhân viên sửa</label>
                                                        {!! $select_employee_fix !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Điểm xuất phát</label>
                                                        <input type="text" name="form[from]" value="{{ @$item['from']}}"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Điểm đến</label>
                                                        <input type="text" name="form[to]" value="{{ @$item['to']}}"
                                                            class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Số trạm dừng</label>
                                                        <input name="form[bus_station]" type="text"
                                                            value="{{ @$item['bus_station'] }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Estime</label>
                                                        <input type="text" name="form[estime]"
                                                            value="{{ @$item['estime']}}" class="form-control">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        @include('admin.component.formMeta')
                                    </div>
                                </div>
                            </div>
                            @csrf
                            @if(isset($item['id']))
                            <input type="hidden" name="id" value="{{ $item['id'] }}">
                            @endif
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>

</div>


@endsection
{{--
<script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}