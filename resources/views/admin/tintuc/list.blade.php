@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level !== 3)
                        <th>Action</th>
                    @endif
                @endif
                <th>Mã</th>
                <th>Tiêu đề</th>
                <!-- <th>Hình ảnh</th> -->
                <!-- <th style="width:fit-content">Giới thiệu</th> -->
                <th>Trạng thái</th>
                <th>Nhân viên tạo</th>
                <th>Nhân viên chỉnh sửa</th>
                <th>Ngày tạo</th>
                <th>Ngày sửa</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id_news, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id_news' => $id]);
                        $title = $item->title;
                        $introduce = $item->introduce;
                        $content = $item->content;
                        $staff_created_id = $item->id_admin_created;
                        $staff_fix = $item->id_admin_changed;
                        $status = Template::showStatus_news($item->check_slide);
                        $created_at = $item['created_at'];
                        $updated_at = $item['updated_at'];
                        $picture = $item->getImage('thumb');
                    @endphp
                    <tr style="height: 70px; text-align: justify;">
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level !== 3)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{!! $id !!}</td>
                        <td>{{ $title }}</td>
                        {{-- <td><img src="{{ $picture }}" alt=""></td>
                        <td>{{ $introduce }}</td> --}}
                        <td>{!! $status !!}</td>
                        <td>{{ $staff_created_id }}</td>
                        <td>{{ $staff_fix }}</td>
                        <td>{{ $created_at }}</td>
                        <td>{{ $updated_at }}</td>

                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
