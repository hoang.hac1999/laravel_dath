@php
$select_status = Form::select('form[check_slide]', config('myconfig.template.check_slide'), @$item['check_slide'], ['class' => 'form-control']);
$employee_qt = \App\Nhanvien::where('Loại_NV','LIKE' ,'Q%')->orderBy('Mã', 'ASC')->get()->pluck('Họ_Tên','Mã');

$select_employee_create = Form::select('form[id_admin_created]', $employee_qt, @$item['id_admin_created'], ['class' => 'form-control']);
$select_employee_fix = Form::select('form[id_admin_changed]', $employee_qt, @$item['id_admin_changed'], ['class' => 'form-control'])
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        @include($pathView . '.element.groupBtnForm')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card " style="height: 460px; overflow-y:scroll;">
                        <form action="#" method="POST" id="frmSave" enctype="multipart/form-data">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                    role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                    aria-controls="meta" aria-selected="false">Meta</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home">
                                                <div class="row" style="padding-top: 20px">
                                                    @if ($errors->any())
                                                        <div class="col-md-12">
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Nhân viên tạo</label>
                                                            {!! $select_employee_create !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Nhân viên sửa</label>
                                                            {!! $select_employee_fix !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tiêu đề</label>
                                                            <input name="form[title]" type="text"
                                                                value="{{ @$item['title'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label style="display: block;">Giới thiệu</label>
                                                            <textarea  name="form[introduce]" id="" cols="78" rows="5">{{ @$item['introduce']}}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label style="display: block;">Nội dung</label>
                                                            <textarea id="ck_content" name="form[content]" id="" cols="78" rows="5">{{ @$item['content']}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Check slide</label>
                                                            <input name="form[check_slide]" type="text"
                                                                value="{{ @$item['check_slide'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Is_disable</label>
                                                            {!! $select_status !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Hình ảnh</label>
                                                            <input name="image" type="file" class="form-control">
                                                            @if (isset($item['image']))
                                                                <img src="{{ $item->getImage('standard') }}" alt="">
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @include('admin.component.formMeta')
                                        </div>
                                    </div>
                                </div>
                                @csrf
                                @if(isset($item['id_news']))
                                    <input type="hidden" name="id" value="{{ $item['id_news'] }}">
                                @endif
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>


@endsection
{{-- <script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}
