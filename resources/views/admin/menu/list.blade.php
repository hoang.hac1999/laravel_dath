@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                <th>Action</th>
                <th>Mã món ăn</th>
                <th>Tên món ăn</th>
                <th>Giá</th>
                <th>Trạng thái</th>
                <th>Hình ảnh</th>
                <th>Ngày tạo</th>
                <th>Ngày cập nhật</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        $name = $item->name;
                        $price = $item->price;
                        $picture = $item->getImage('thumb');
                        $status = Template::showStatus_food($item->status);
                        $created_at = date('d/m/y h:m:s', $item['created_at']);
                        $updated_at = date('d/m/y h:m:s', $item['updated_at']);
                        // $picture = $item -> getImage('thumb');
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                        <td>{!! $id !!}</td>
                        <td>{{ $name }}</td>
                        <td>{{ $price }}</td>
                        <td>{!! $status !!}</td>
                        <td><img src="{{ $picture }}" alt=""></td>
                        <td>{{ $created_at }}</td>
                        <td>{{ $updated_at }}</td>

                        {{-- <td><img src="{{ $picture }}" alt=""></td> --}}
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
