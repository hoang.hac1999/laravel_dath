<html lang="en" style="height: auto;"><head>
  @include('admin.component.head')
</head>
<body class="sidebar-mini" style="height: auto;">
<div class="wrapper">

  @include('admin.component.main-header')
  @include('admin.component.main-sidebar')

  <div class="content-wrapper" style="min-height: 713px;">
    @yield('content')

  </div>

</div>
    @include('admin.component.footer')
    @include('admin.component.script')

</body>
</html>
