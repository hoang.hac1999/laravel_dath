@php
$select_sex = Form::select('form[Giới_tính]', config('myconfig.template.sex'), @$item['Giới_tính'], ['class' => 'form-control']);
$select_emp = Form::select('form[Loại_NV]', config('myconfig.template.Loại_NV'), @$item['Loại_NV'], ['class' => 'form-control']);
// $select_journey_id = Form::select('form[journey_id]', config('myconfig.select.journey_id'), null, ['class' => 'form-control']);
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        {{-- @include($pathView, 'element.groupBtnForm') --}}
                        @include('admin.khachhang.element.groupBtnForm')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card " style="height: 460px; overflow-y:scroll;">
                        <form action="#" method="POST" id="frmSave">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                    role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                    aria-controls="meta" aria-selected="false">Meta</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home">
                                                <div class="row" style="padding-top: 20px">
                                                    @if ($errors->any())
                                                        <div class="col-md-12">
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Tên nhân viên</label>
                                                            <input name="form[Họ_Tên]" type="text"
                                                                value="{{ @$item['Họ_Tên'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Giới tính</label>
                                                            {!! $select_sex !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Ngày sinh</label>

                                                            <input min="1950-01-02" name="form[Ngày_sinh]" type="date"
                                                                value="{{ @$item['Ngày_sinh'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Loại nhân viên</label>
                                                            {!! $select_emp !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email</label>

                                                            <input name="form[Email]" type="email"
                                                                value="{{ @$item['Email'] }}" class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Username</label>
                                                            <input name="form[Username]" type="text"
                                                                value="{{ @$item['Username'] }}" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input name="form[Password]" value="{{ @$item['Password'] }}"
                                                                type="password" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Số điện thoại</label>
                                                            <input name="form[Sđt]" value="{{ @$item['Sđt'] }}"
                                                                type="tel" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Địa chỉ</label>
                                                            <input name="form[Địa_chỉ]" value="{{ @$item['Địa_chỉ'] }}"
                                                                type="text" class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Chi nhánh</label>
                                                            <input id="name_en" name="form[Chi_nhánh]" type="text"
                                                                value="{{ @$item['Chi_nhánh'] }}" class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Bằng lái</label>
                                                            <input id="name_en" name="form[Bằng_lái]" type="text"
                                                                value="{{ @$item['Bằng_lái'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Ngày bắt đầu</label>

                                                            <input min="1950-01-02" name="form[Date_Starting]" type="date"
                                                                value="{{ @$item['Date_Starting'] }}" class="form-control">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Picture</label>
                                                            <input type="file" class="form-control">
                                                        </div>
                                                    </div> --}}
                                                </div>
                                            </div>
                                            @include('admin.component.formMeta')
                                        </div>
                                    </div>
                                </div>
                                @csrf
                                {{-- Nếu form có id thì update --}}
                                @if (isset($item['Mã']))
                                    <input type="hidden" name="id" value="{{ $item['Mã'] }}">
                                @endif
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>


@endsection
{{-- <script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}
