

@php
$select_emp = Form::select('filterStatus', config('myconfig.template.Loại_NV'), $params['filter']['Loại_NV'], ['class' => 'form-control']);
$search_field = [
    'all' => 'Tất cả',
    'id' => 'ID',
    // 'departure' => 'Nơi đi',
    // 'destination' => 'Nơi đến',
    // 'driver' => 'Tài xế',
    // 'create_empl' => 'Nhân viên tạo',
    // 'seat_model' => 'Loại ghế',
    'Họ_Tên' => 'Họ tên',
    'Sđt' => 'Số điện thoại',
    // 'license_plate' => 'Biển số xe',
    'Email' => 'email'
];
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Nhân viên'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        {{-- @include( $pathView . 'element.groupBtnIndex') --}}
                        @include($pathView .'.element.groupBtnIndex')
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="{{ route('admin.' . $controllerName . '.index') }}" class="form-inline">
                                <div class="form-group">
                                    <label for="" style="padding-right: 10px;">Filter loại nhân viên</label>
                                    {!! $select_emp !!}
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <form id="frmSearch" action="{{ route('admin.' . $controllerName . '.index') }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="display: flex;">
                                            <div class="dropdown">
                                                <button id="search_option" class="btn btn-default dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    Tìm kiếm theo {{ $search_field[$params['search']['field']] }}
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    @foreach ($search_field as $key => $item)
                                                        <a class="dropdown-item"
                                                            href="javascript:changeSearchField('{{ $key }}' , '{{ $item }}')">{{ $item }}</a>
                                                    @endforeach
                                                </div>

                                            </div>
                                            <input name="search_value" type="text" class="form-control" value="{{ $params['search']['value'] }}">
                                            <button class="btn btn-default" style="margin-left: 20px;" type="submit">Search</button>
                                            <a href="javascript:clearSearch()" class="btn btn-default" style="margin-left: 20px;" type="submit">Clear</a>
                                        </div>
                                        <input type="hidden" name="search_field" value="{{ $params['search']['field'] }}">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Danh sách nhân viên</h3>
                            <div class="card-tools">
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">

                            @include($pathView . '.list')
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <div class="pagination">
                {!! $items->links() !!}
            </div>
        </div>
    </div>
@endsection

