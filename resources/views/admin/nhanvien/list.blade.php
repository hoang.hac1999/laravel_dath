@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level == 1)
                        <th>Action</th>
                    @endif
                @endif

                <th>Mã NV</th>
                <th>Họ tên</th>
                <th>Ngày sinh</th>
                <th>Giới tính</th>
                <th>Địa chỉ</th>
                <th>Username</th>
                <th>Email</th>
                <th>Loại nhân viên</th>
                <th>Chi nhánh</th>
                <th>Bằng lái</th>
                <th>SĐT</th>
                <th>Ngày bắt đầu</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->Mã, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['Mã' => $id]);
                        $name = Template::highlight($item->Họ_Tên, $params['search']);
                        $sex = Template::showSex($item->Giới_tính);
                        $birthday = $item->Ngày_sinh;
                        $email = Template::highlight($item->Email, $params['search']);
                        $phone = Template::highlight($item->Sđt, $params['search']);
                        $addr = $item->Địa_chỉ;
                        $username = $item->Username;
                        $branch = $item->Chi_nhánh;
                        $cate_emp = $item->Loại_NV;
                        $license = $item->Bằng_lái;
                        $date_satrt = $item->Date_Starting;

                        // $link = route('admin.' . $controllerName . '.changeStatus', ['status' => $item->status]);
                        // $sex = Template::showStatus($item->sex);
                        // $status = $item->status;
                        // $picture = $item -> getImage();

                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level == 1)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{{ $id }}</td>
                        <td>{{ $name }}</td>
                        <td>{{ $birthday }}</td>
                        <td>{!! $sex !!}</td>
                        <td>{{ $addr }}</td>
                        <td>{{ $username }}</td>
                        <td>{{ $email }}</td>
                        <td>{{ $cate_emp }}</td>
                        <td>{{ $branch }}</td>
                        <td>{{ $license }}</td>
                        <td>{{ $phone }}</td>
                        <td>{{ $date_satrt }}</td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
