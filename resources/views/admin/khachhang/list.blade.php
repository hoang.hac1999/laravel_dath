@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level !== 3)
                        <th>Action</th>
                    @endif
                @endif
                <th>Mã KH</th>
                <th>Tên</th>
                <th>Giới tính</th>
                <th>Ngày sinh</th>
                <th>Email</th>
                <th>Số điện thoại</th>
                <th>Địa chỉ</th>

            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        $name = Template::highlight($item->name, $params['search']);
                        $sex = Template::showSex($item->sex);
                        $birthday = $item->birthday;
                        $email = Template::highlight($item->email, $params['search']);
                        $phone = Template::highlight($item->phone, $params['search']);
                        $addr = $item->address;
                        // $link = route('admin.' . $controllerName . '.changeStatus', ['status' => $item->status]);
                        // $sex = Template::showStatus($item->sex);
                        // $status = $item->status;
                        // $picture = $item -> getImage();
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level !== 3)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{{ $id }}</td>
                        <td>{{ $name }}</td>
                        <td>{!! $sex !!}</td>
                        <td>{{ $birthday }}</td>
                        <td>{{ $email }}</td>
                        <td>{{ $phone }}</td>
                        <td>{{ $addr }}</td>

                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
