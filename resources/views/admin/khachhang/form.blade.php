@php
$select_sex = Form::select('form[sex]', config('myconfig.template.sex'), @$item['sex'], ['class' => 'form-control']);
// $select_journey_id = Form::select('form[journey_id]', config('myconfig.select.journey_id'), null, ['class' => 'form-control']);
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        {{-- @include($pathView, 'element.groupBtnForm') --}}
                        @include('admin.khachhang.element.groupBtnForm')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card " style="height: 460px; overflow-y:scroll;">
                        <form action="#" method="POST" id="frmSave">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                    role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                    aria-controls="meta" aria-selected="false">Meta</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home">
                                                <div class="row" style="padding-top: 20px">
                                                    @if ($errors->any())
                                                        <div class="col-md-12">
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Tên khách hàng</label>
                                                            <input onchange="toEnName(this)" name="form[name]" type="text"
                                                                value="{{ @$item['name'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tên không dấu</label>
                                                            <input id="name_en" name="form[name_en]" type="text"
                                                                value="{{ @$item['name_en'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Giới tính</label>
                                                            {!! $select_sex !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Ngày sinh</label>

                                                            <input min="1950-01-02" name="form[birthday]" type="date"
                                                                value="{{ @$item['birthday'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email</label>

                                                            <input name="form[email]" type="email"
                                                                value="{{ @$item['email'] }}" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input name="form[password]" value="{{ @$item['password'] }}"
                                                                type="password" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Số điện thoại</label>
                                                            <input name="form[phone]" value="{{ @$item['phone'] }}"
                                                                type="tel" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Địa chỉ</label>
                                                            <input name="form[address]" value="{{ @$item['address'] }}"
                                                                type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Picture</label>
                                                            <input type="file" class="form-control">
                                                        </div>
                                                    </div> --}}
                                                </div>
                                            </div>
                                            @include('admin.component.formMeta')
                                        </div>
                                    </div>
                                </div>
                                @csrf
                                {{-- Nếu form có id thì update --}}
                                @if (isset($item['id']))
                                    <input type="hidden" name="id" value="{{ $item['id'] }}">
                                @endif
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>


@endsection
{{-- <script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}
<script>
    function toEnName(t) {
    var text = $(t).val();
    var str = text.toLowerCase();

    //Xóa dấu
    str = str.replace(/(à|á|ả|ạ|ã|ă|ắ|ẵ|ẳ|ặ|ằ|â|ấ|ầ|ẫ|ẩ|ậ)/g, "a");
    str = str.replace(/(é|è|ẽ|ẻ|ẹ|ê|ế|ề|ệ|ễ|ể)/g, "e");
    str = str.replace(/(í|ì|ị|ĩ|ỉ|)/g, "");
    str = str.replace(/(ô|ố|ổ|ồ|ỗ|ộ|ơ|ớ|ờ|ỡ|ở|ợ|ó|ò|ỏ|õ|ọ)/g, "o");
    str = str.replace(/(ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự)/g, "u");
    str = str.replace(/(ý|ỳ|ỹ|ỷ|ỵ)/g, "y");
    str = str.replace(/(đ)/g, "d");

    //Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');
    //Xóa khoảng trắng thay bằng ký tự -
    // str = str.replace(/(\s+)/g, "-");

    //Xóa phần dư - ở đầu
    // str = str.replace(/^-+/g, "");
    //Xóa phần dư - ở cuối
    // str = str.replace(/-+$/g, "");


    $("#name_en").val(str);
}
</script>
