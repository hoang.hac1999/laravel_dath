

@php
$select_status = Form::select('form[status]', config('myconfig.template.status'), @$item['status'], ['class' => 'form-control']);
$employee_qt = \App\Nhanvien::where('Loại_NV','LIKE' ,'Q%')->orderBy('Mã', 'ASC')->get()->pluck('Họ_Tên','Mã');
$employee_tx = \App\Nhanvien::where('Loại_NV', 'TX')->orderBy('Mã', 'ASC')->get()->pluck('Họ_Tên','Mã');
$route_id = \App\Lotrinh::select("id", DB::raw("CONCAT(route.from,' - ',route.to) as route"))->orderBy('id', 'ASC')->get()->pluck('route','id');
$id_vehicle = \App\Xe::orderBy('id', 'ASC')->get()->pluck('license_plate','id');

$select_employee_qt = Form::select('form[id_staff]', $employee_qt, @$item['id_staff'], ['class' => 'form-control']);
$select_employee_tx = Form::select('form[id_driver]', $employee_tx, @$item['id_driver'], ['class' => 'form-control']);
$select_route_id = Form::select('form[id_route]', $route_id, @$item['id_route'], ['class' => 'form-control']);
$select_id_vehicle = Form::select('form[id_vehicle]', $id_vehicle, @$item['id_vehicle'], ['class' => 'form-control']);
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        {{-- @include($pathView, 'element.groupBtnForm') --}}
                        @include('admin.chuyenxe.element.groupBtnForm')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card " style="height: 460px; overflow-y:scroll;">
                        <form action="#" method="POST" id="frmSave" enctype="multipart/form-data">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                    role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                    aria-controls="meta" aria-selected="false">Meta</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home">
                                                <div class="row" style="padding-top: 20px">
                                                    @if ($errors->any())
                                                        <div class="col-md-12">
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Nhân viên tạo</label>
                                                            {!! $select_employee_qt !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Mã lộ trình</label>
                                                            {!! $select_route_id !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Mã tài xế</label>
                                                            {!! $select_employee_tx !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Mã xe</label>
                                                            {!! $select_id_vehicle !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Ngày khởi hành</label>
                                                            <input name="form[date_start]" type="date"
                                                                value="{{ {{-- date('Y-m-d') --}} @$item['date_start'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Giờ khởi hành</label>
                                                            <input name="form[time_start]" type="time" value="{{ @$item['time_start'] }}"
                                                                class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Status</label>
                                                            {!! $select_status !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Giá vé</label>
                                                            <input name="form[ticket_price]" type="number" min="100000"
                                                                max="1000000" step="10000" value="350000"
                                                                class="form-control">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            @include('admin.component.formMeta')
                                        </div>
                                    </div>
                                </div>
                                @csrf
                                @if(isset($item['id']))
                                    <input type="hidden" name="id" value="{{ $item['id'] }}">
                                @endif
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>


@endsection
{{-- <script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}
