@php
$select_status = Form::select('filterStatus', config('myconfig.template.status'), $params['filter']['status'], ['class' => 'form-control']);
$search_field = [
    'all' => 'Tất cả',
    'id' => 'ID',
    //'from' => 'Nơi đi',
    //'to' => 'Nơi đến',
    // 'driver' => 'Tài xế',
    //'Họ_Tên' => 'Nhân viên tạo',
    // 'seat_model' => 'Loại ghế',
    'date_start' => 'Ngày khởi hành',
    'time_start' => 'Giờ khởi hành',
    // 'license_plate' => 'Biển số xe',
    'ticket_price' => 'Giá vé'
];
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Chuyến xe'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        {{-- @include( $pathView . 'element.groupBtnIndex') --}}
                        @include($pathView .'.element.groupBtnIndex')
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="{{ route('admin.' . $controllerName . '.index') }}" class="form-inline">
                                <div class="form-group">
                                    <label for="" style="padding-right: 10px;">Filter status</label>
                                    {!! $select_status !!}
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <form id="frmSearch" action="{{ route('admin.' . $controllerName . '.index') }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="display: flex;">
                                            <div class="dropdown">
                                                <button id="search_option" class="btn btn-default dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    Tìm kiếm theo {{ $search_field[$params['search']['field']] }}
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    @foreach ($search_field as $key => $item)
                                                        <a class="dropdown-item"
                                                            href="javascript:changeSearchField('{{ $key }}' , '{{ $item }}')">{{ $item }}</a>
                                                    @endforeach
                                                </div>

                                            </div>
                                            <input name="search_value" type="text" class="form-control" value="{{ $params['search']['value'] }}">
                                            <button class="btn btn-default" style="margin-left: 20px;" type="submit">Search</button>
                                            <a href="javascript:clearSearch()" class="btn btn-default" style="margin-left: 20px;" type="submit">Clear</a>
                                        </div>
                                        <input type="hidden" name="search_field" value="{{ $params['search']['field'] }}">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Danh sách chuyến xe</h3>
                            <div class="card-tools">
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">

                            @include($pathView . '.list')
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <div class="pagination">
                {!! $items->links() !!}
            </div>
        </div>
    </div>
@endsection

