@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level !== 3)
                        <th>Action</th>
                    @endif
                @endif
                <th>Mã CX</th>
                <th>Nơi đi</th>
                <th>Nơi đến</th>
                <th>Nhân viên tạo</th>
                <th>Tài xế</th>
                <th>Biển số xe</th>
                <th>Loại ghế</th>
                <th>Ngày khởi hành</th>
                <th>Giờ khởi hành</th>
                <th>Trạng thái</th>
                <th>Giá vé</th>
                <th>Ngày sửa</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        $departure = $item->route->from;
                        $destination = $item->route->to;
                        $staff_create = $item->nhanvien_qt->Họ_Tên;
                        $driver = $item->nhanvien_tx->Họ_Tên;

                        $name_model = $item->xe->Tên_Loại;
                        $license = $item->xe->license_plate;
                        $start_day = Template::highlight($item->date_start, $params['search']);
                        $start_time = Template::highlight($item->time_start, $params['search']);
                        $price = Template::highlight($item->ticket_price, $params['search']);

                        $status = Template::showStatus($item->status);

                        $updated_at = $item['updated_at'];
                        // $picture = $item -> getImage();
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level !== 3)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{!! $id !!}</td>
                        <td>{{ $departure }}</td>
                        <td>{{ $destination }}</td>
                        <td>{{ $staff_create }}</td>
                        <td>{{ $driver }}</td>
                        <td>{{ $license }}</td>
                        <td>{{ $name_model }}</td>
                        <td>{!! $start_day !!}</td>
                        <td>{!! $start_time !!}</td>
                        <td>{!! $status !!}</td>
                        <td>{!! $price !!}</td>
                        <td>{{ $updated_at }}</td>
                        {{-- <td><img src="{{ $picture }}" alt=""></td> --}}
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
