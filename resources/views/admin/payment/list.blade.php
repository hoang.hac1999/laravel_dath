

@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                {{-- <th>Action</th> --}}
                <th>STT</th>
                <th>Order ID</th>
                <th>Thành viên thanh toán</th>
                <th>Số tiền</th>
                <th>Ghi chú</th>
                <th>Mã phản hồi</th>
                <th>Mã giao dịch</th>
                <th>Mã ngân hàng</th>
                <th>Thời gian chuyển khoản</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route("admin." .$controllerName. ".form", ['id' => $id]);
                        $order_id = $item->order_id;
                        $member = $item->thanh_vien;
                        //$staff_create = $item->nhanvien_qt->Họ_Tên;
                        //$staff_fix = $item->nhanvien_qt->Họ_Tên;
                        $money = $item->money;
                        $note = $item->note;
                        $respone_code = $item->vnp_response_code;
                        $code_vnpay = $item->code_vnpay;
                        $code_bank = $item->code_bank;
                        $time = $item->time;
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        {{-- <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td> --}}
                        <td>{!! $id !!}</td>
                        <td>{{ $order_id }}</td>
                        <td>{{ $member }}</td>
                        <td>{{ $money }}</td>
                        <td>{{ $note }}</td>
                        <td>{{ $respone_code }}</td>
                        <td>{{ $code_vnpay }}</td>
                        <td>{{  $code_bank  }}</td>
                        <td>{{  $time  }}</td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
