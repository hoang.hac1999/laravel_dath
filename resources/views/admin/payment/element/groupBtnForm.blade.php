


<div class="card-body">
    <a href="javascript:submitSaveClose('{{ route('admin.' . $controllerName . '.save', 'type=save') }}')" class="btn btn-app">
        <i class="far fa-save"></i>Save
    </a>
    <a href="javascript:submitSaveClose('{{ route('admin.' . $controllerName . '.save', 'type=new') }}')"
        class="btn btn-app">
        <i class="fas fa-share-square"></i>Save $ New
    </a>
    <a href="javascript:submitSaveClose('{{ route('admin.' . $controllerName . '.save', 'type=close') }}')"
        class="btn btn-app">
        <i class="fas fa-external-link-alt"></i>Save & Close
    </a>
    <a href="{{ route('admin.' . $controllerName . '.index') }}" class="btn btn-app">
        <i class="far fa-window-close"></i>Close
    </a>
</div>
