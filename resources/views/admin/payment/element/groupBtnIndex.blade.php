@if (\Auth::check())
    @if (Auth::user()->level !== 3)
        <div class="card-body">
            <a href="{{ route('admin.' . $controllerName . '.form') }}" class="btn btn-app">
                <i class="fas fa-plus"></i>Add
            </a>
            <a href="javascript:submitForm('{{ route('admin.' . $controllerName . '.deleteItem') }}')"
                class="btn btn-app">
                <i class="fa fa-trash"></i>Delete
            </a>

            <a class="btn btn-app">
                <i class="fa fa-sort"></i> Ordering
            </a>
        </div>
    @endif
@endif
