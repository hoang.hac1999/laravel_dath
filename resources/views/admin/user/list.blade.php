@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level == 1)
                        <th>Action</th>
                    @endif
                @endif
                <th>STT</th>
                <th>Tên user</th>
                <th>Email</th>
                <th>Hình ảnh</th>
                <th>Loại nhân viên</th>
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
                <th>Ngày cập nhật</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = Template::highlight($item->id, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        $name = $item->name;
                        $email = $item->email;
                        $picture = $item->getImage('thumb');
                        $password = $item->password;
                        $level = Template::showUser_level($item->level);
                        $status = Template::showStatus_user($item->status);
                        $created_at = $item->created_at;
                        $updated_at = $item->updated_at;
                        // $picture = $item -> getImage('thumb');
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level == 1)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{!! $id !!}</td>
                        <td>{{ $name }}</td>
                        <td>{{ $email }}</td>
                        <td><img src="{{ $picture }}" alt=""></td>
                        <td>{!! $level !!}</td>
                        <td>{!! $status !!}</td>
                        <td>{{ $created_at }}</td>
                        <td>{{ $updated_at }}</td>

                        {{-- <td><img src="{{ $picture }}" alt=""></td> --}}
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
