@php
$select_status = Form::select('form[status]', config('myconfig.user.status'), @$item['status'], ['class' => 'form-control']);
$select_level = Form::select('form[level]', config('myconfig.user.level'), @$item['level'], ['class' => 'form-control']);
// $select_journey_id = Form::select('form[journey_id]', config('myconfig.select.journey_id'), null, ['class' => 'form-control']);
@endphp

@extends('admin.layout')

@section('content')
    @include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card text-center">
                        @include($pathView . 'element.groupBtnForm')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card " style="height: 460px; overflow-y:scroll;">
                        <form action="#" method="POST" id="frmSave" enctype="multipart/form-data">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                    role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                    aria-controls="meta" aria-selected="false">Meta</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home">
                                                <div class="row" style="padding-top: 20px">
                                                    @if ($errors->any())
                                                        <div class="col-md-12">
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label>Tên người dùng</label>
                                                            <input name="form[name]" type="text" value="{{ @$item['name'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email </label>
                                                            <input name="form[email]" type="email" value="{{ @$item['email'] }}" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input name="form[password]" type="text" value="{{ @$item['password'] }}" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Trạng thái</label>
                                                            {{ $select_status }}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Loại user</label>
                                                            {{ $select_level }}
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Picture</label>
                                                            <input name="image" type="file" class="form-control">
                                                            @if (isset($item['image']))
                                                                <img src="{{ $item->getImage('standard') }}" alt="">
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        {{-- @include('admin.component.formMeta') --}}
                                    </div>
                                </div>
                            </div>
                            @csrf
                            @if (isset($item['id']))
                                <input type="hidden" name="id" value="{{ $item['id'] }}">
                            @endif
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>


@endsection
{{-- <script>
    function submitSaveClose(link) {
        var form = $("#frmSave");
        form.attr("action", link);
        form.submit();
    }
</script> --}}
