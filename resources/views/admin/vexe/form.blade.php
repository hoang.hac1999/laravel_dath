@php
$select_status = Form::select('form[status]', config('myconfig.ticket.status'), @$item['status'], ['class' =>'form-control']);
$buses_id = \App\Chuyenxe::select("id", DB::raw("CONCAT(buses.time_start,' - ',buses.date_start) as buses_id"))->orderBy('id', 'ASC')->get()->pluck('buses_id','id');
$employee_qt = \App\Nhanvien::where('Loại_NV','LIKE' ,'Q%')->orderBy('Mã', 'ASC')->get()->pluck('Họ_Tên','Mã');
$food_id = \App\Menu_chinh::orderBy('id', 'ASC')->get()->pluck('name','id');
//$payment_id = \App\Payment::orderBy('id', 'ASC')->get()->pluck('name','id');

$select_buses_id = Form::select('form[id_buses]', $buses_id, @$item['id_buses'], ['class' => 'form-control']);
$select_staff_id = Form::select('form[id_staff]', $employee_qt, @$item['id_staff'], ['class' => 'form-control']);
// $select_food_id = Form::select('form[id_food]', $food_id, @$item['id_food'], ['class' => 'form-control']);
// $select_journey_id = Form::select('form[journey_id]', config('myconfig.select.journey_id'), null, ['class' =>'form-control']);
@endphp

@extends('admin.layout')

@section('content')
@include('admin.component.content-header', ['title' => 'Thêm/Sửa'])
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card text-center">
                    @include($pathView . 'element.groupBtnForm')
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card " style="height: 460px; overflow-y:scroll;">
                    <form action="#" method="POST" id="frmSave">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="meta-tab" data-toggle="tab" href="#meta" role="tab"
                                                aria-controls="meta" aria-selected="false">Meta</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home">
                                            <div class="row" style="padding-top: 20px">
                                                @if ($errors->any())
                                                <div class="col-md-12">
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Mã chuyến xe</label>
                                                        {!! $select_buses_id !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nhân viên đặt</label>
                                                        {!! $select_staff_id !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Mã khách hàng</label>
                                                        <input name="form[id_customer]" type="email"
                                                                value="{{ @$item['id_customer'] }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Vị trí chọn</label>
                                                        <input id="" name="form[name_seat]" type="text"
                                                            value="{{ @$item['name_seat'] }}" class="form-control">
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Trạng thái</label>
                                                        {!! $select_status !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label>SĐT KH</label>
                                                        <input id="" name="" type="text"
                                                            value="{{ @$item->khachhang->phone }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Thay đổi ngày</label>
                                                        <input name="form[date_change]" type="date"
                                                                value="{{ {{-- date('Y-m-d') --}} @$item['date_change'] }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Thay đổi giờ</label>
                                                        <input name="form[time_change]" type="time" value="{{ @$item['time_change'] }}"
                                                                class="form-control">
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label>Ngày đặt</label>

                                                        <input min="1950-01-02" name="form[moment_booking]" type="date"
                                                            value="{{ @$item['moment_booking'] }}" class="form-control">
                                                    </div> -->
                                                    {{-- <div class="form-group">
                                                        <label>Mã món ăn</label>
                                                        {!! $select_food_id !!}
                                                    </div> --}}
                                                </div>
                                                {{-- <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Picture</label>
                                                        <input type="file" class="form-control">
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        @include('admin.component.formMeta')
                                    </div>
                                </div>
                            </div>
                            @csrf
                            {{-- Nếu form có id thì update --}}
                            @if (isset($item['id']))
                            <input type="hidden" name="id" value="{{ $item['id'] }}">
                            <input type="hidden" name="phone" value="{{ @$item->khachhang->phone }}">
                            @endif
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>

</div>


@endsection
