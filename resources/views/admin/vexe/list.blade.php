@php
use App\Helper\Template;

@endphp

<form action="#" method="POST" id="frmList">
    <table class="table table-hover text-nowrap list-content">
        <thead>
            <tr>
                <th scope="col"><input type="checkbox" id="check-all"></th>
                @if (\Auth::check())
                    @if (Auth::user()->level !== 3)
                        <th>Action</th>
                    @endif
                @endif
                <th>Mã vé</th>
                <th>Mã CX</th>
                <th>Lộ trình</th>
                <th>Chuyến xe</th>
                <th>SĐT KH</th>
                <th>Chỗ ngồi</th>
                <th>Tên món ăn</th>
                <th>Số lượng</th>
                <th>Trạng thái vé</th>
                <th>Trạng thái thanh toán</th>
                <th>SMS</th>
            </tr>
        </thead>
        <tbody>
            @if ($items->count() > 0)
                @foreach ($items as $key => $item)
                    @php
                        $id = $item->id;
                        $id_ticket = Template::highlight($item->id_ticket, $params['search']);
                        $linkEdit = route('admin.' . $controllerName . '.form', ['id' => $id]);
                        @$phones = Template::highlight($item->khachhang->phone, $params['search']);
                        @$date_start = Template::highlight($item->chuyenxe->date_start, $params['search']);
                        @$time_start = $item->chuyenxe->time_start;
                        $id_customer = $item->id_customer;
                        $name_seat = $item->name_seat;
                        $id_buses = Template::highlight($item->id_buses, $params['search']);
                        @$from = $item->chuyenxe->route->from;
                        @$to = $item->chuyenxe->route->to;
                        @$name_food = $item->list_food->menu_food->name;
                        @$quantity = $item->list_food->quatity;
                        $name = Template::highlight($item->name, $params['search']);
                        @$status = Template::showStatus_ticket($item->status);
                        // $food_id = $item->id_food;
                        $payment = Template::showStatus_payment($item->payment_status);
                        $sendsms = route('admin.' . $controllerName . '.sendsms',['phone'=>$phones,'seat'=>$name_seat,'time_start'=>$time_start,'date_start'=>$date_start,'id_bus'=>$id_buses]);
                    @endphp
                    <tr>
                        <td><input name="cid[]" value="{{ $id }}" type="checkbox"></td>
                        @if (\Auth::check())
                            @if (Auth::user()->level !== 3)
                                <td><a href="{{ $linkEdit }}"><i class="far fa-edit"></i></a></td>
                            @endif
                        @endif
                        <td>{!! $id_ticket !!}</td>
                        <td>{!! $id_buses !!}</td>
                        <td>{{ @$from . ' - ' . @$to }}</td>
                        <td>{!! @$date_start . ' - ' . @$time_start !!}</td>
                        {{-- <td>{!! $id_customer !!}</td> --}}
                        <td>{!! @$phones !!}</td>
                        <td>{!! $name_seat !!}</td>
                        <td>{{ @$name_food }}</td>
                        <td>{{ @$quantity }}</td>
                        <td>{!! @$status !!}</td>
                        <td>{!! $payment !!}</td>
                        <td><a href="{{$sendsms}}" type="submit" class="btn btn-primary"> Reminder</a></td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
    @csrf
</form>
