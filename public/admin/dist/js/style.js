$("#check-all").change(function () {
    var checked = $(this).prop("checked");
    $(".list-content input[type='checkbox']").prop("checked", checked);
});

function submitForm(link) {
    var form = $("#frmList");
    if (confirm("Bạn chắc chắn muốn xóa? ")) {
        form.attr("action", link);
        form.submit();
    }
}

function submitSaveClose(link) {
    var form = $("#frmSave");
    form.attr("action", link);
    form.submit();
}
function ajaxStatus(link, id) {
    $.ajax({
        url: link,
        data: { id: id },
        dataType: "json",
        success: function (data) {
            var classAdd = "btn-success";
            var classRemove = "btn-default";
            if (data.status == "inactive") {
                classAdd = "btn-default";
                classRemove = "btn-success";
            }
            $("#status-" + data.id).removeClass(classRemove);
            $("#status-" + data.id).addClass(classAdd);
            $("#status-" + data.id).attr(
                "href",
                "javascript:ajaxStatus('" + data.link + "', " + data.id + ")"
            );
        },
        error: function () {
            alert("Lỗi server");
        },
    });
}

$("select[name='filterStatus']").change(function () {
    $(this).parents("form").submit();
});

function changeSearchField(field, text) {
    $("#search_option").text("Tìm kiếm theo " + text);
    $("input[name='search_field']").val(field);
}

function clearSearch() {
    $("input[name='search_field']").val("all");
    $("input[name='search_value']").val("");
    $("#frmSearch").submit();
}

function toEnName(t) {
    var str = $(t).val();
    str = title.toLowerCase();

    //Xóa dấu
    str = str.replace(/(à|á|ả|ạ|ã|ă|ắ|ẵ|ẳ|ặ|ằ|â|ấ|ầ|ẫ|ẩ|ậ)/g, "a");
    str = str.replace(/(é|è|ẽ|ẻ|ẹ|ê|ế|ề|ệ|ễ|ể)/g, "e");
    str = str.replace(/(í|ì|ị|ĩ|ỉ|)/g, "i");
    str = str.replace(/(ô|ố|ổ|ồ|ỗ|ộ|ơ|ớ|ờ|ỡ|ở|ợ|ó|ò|ỏ|õ|ọ)/g, "o");
    str = str.replace(/(ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự)/g, "u");
    str = str.replace(/(ý|ỳ|ỹ|ỷ|ỵ)/g, "y");
    str = str.replace(/(đ)/g, "d");

    //Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');
    //Xóa khoảng trắng thay bằng ký tự -
    // str = str.replace(/(\s+)/g, "-");

    //Xóa phần dư - ở đầu
    // str = str.replace(/^-+/g, "");
    //Xóa phần dư - ở cuối
    // str = str.replace(/-+$/g, "");


    $("#name_en").val(str);
}

