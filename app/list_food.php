<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;

class list_food extends Model
{
    protected $table = "list_food";
    protected $primaryKey = "Mã";


    public $timestamps = false;

    public function menu_food(){
        return $this->belongsTo('App\Menu_chinh', 'id_menu');
    }
}
