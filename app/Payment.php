<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";
    protected $primaryKey = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    public $search_field = [
        'id',
        'thanh_vien',
        'code_bank'
    ];

    // public function nhanvien_qt(){
    //     return $this->belongsTo('App\Nhanvien');
    // }

    public function listItem($params = null){

        $query = $this->select('*');
        if(!empty($params['search']['value'])){
            if($params['search']['field']!= "all"){
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id';
                $query->where($search_field, 'LIKE' , '%' .$params['search']['value'] .'%');
            }else{
                foreach($this->search_field as $key => $search_field){
                    $query->orWhere($search_field, 'LIKE' , '%' .$params['search']['value'] .'%');
                }
            }

        }

        $query = $query ->orderBy("id", "DESC")->paginate(5);
        return $query;
    }

    public function saveItem($params){
        $data = $params['form'];
        if (!isset($params['id'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $this->where('id', $params['id'])->update($data);
            $item = $this->where('id', $params['id']);
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id'];
        }
        // $data = $params['form'];
        // $this->insert($data);
    }
}
