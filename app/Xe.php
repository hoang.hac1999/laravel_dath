<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xe extends Model
{
    protected $table = "vehicle";
    protected $primaryKey = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    public function chuyenxes(){
        return $this->hasMany('App\Chuyenxe');
    }

    public function bus_model(){
        return $this->belongsToMany('App\Loaixe', 'type');
    }
}
