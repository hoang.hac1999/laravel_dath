<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramdung extends Model
{
    protected $table = "coach_station";
    protected $primaryKey = "ID";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
}
