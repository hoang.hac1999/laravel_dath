<?php

namespace App\Helper;

class Template
{
    public static function showStatus($status)
    {
        switch ($status) {
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-default">Waiting</small>';
                break;
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-success">Running</small>';
                break;
            case 2:
                return '<small style="font-size: .8em;" class="btn btn-warning">Completed</small>';
                break;
            case 3:
                return '<small style="font-size: .8em;" class="btn btn-danger">Locked</small>';
                break;
        }
        // return $html;
    }
    public static function showSex($sex)
    {
        switch ($sex) {
            case 0:
                return '<p>Nữ</p>';
                break;
            case 1:
                return '<p>Nam</p>';
                break;
        }
        // return $html;
    }

    public static function showStatus_food($status)
    {
        switch ($status) {
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-danger">Hết món</small>';
                break;
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-success">Còn món</small>';
                break;
        }
        // return $html;
    }

    public static function showStatus_ticket($status)
    {
        switch ($status) {
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-success">Còn trống</small>';
                break;
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-danger">Đã đặt</small>';
                break;
        }
        // return $html;
    }

    public static function showStatus_news($status)
    {
        switch ($status) {
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-danger">Ẩn</small>';
                break;
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-success">Hiện</small>';
                break;
        }
        // return $html;
    }
    public static function showStatus_payment($status)
    {
        switch ($status) {
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-success">Đã thanh toán</small>';
                break;
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-danger">Chưa thanh toán</small>';
                break;
        }
        // return $html;
    }

    public static function showStatus_user($status){
        switch ($status) {
            case 1:
                return '<small style="font-size: .8em;" class="btn btn-success">Active</small>';
                break;
            case 0:
                return '<small style="font-size: .8em;" class="btn btn-danger">Inactive</small>';
                break;
        }
    }

    public static function showUser_level($status){
        switch ($status) {
            case 1:
                return '<p>Quản trị viên</p>';
                break;
            case 2:
                return '<p>Nhân viên đặt vé</p>';
                break;
            case 3:
                return '<p>Nhân viên TDC</p>';
                break;
        }
    }

    public static function highlight($input, $paramSearch)
    {
        if (empty($paramSearch['value']))
            return $input;
        return preg_replace("/" . $paramSearch['value'] . "/", '<span class="highlight">$0</span>', $input);
        // return $input;
    }
}
