<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nhanvien extends Model
{
    protected $table = "employee";
    protected $primaryKey = "Mã";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    public $search_field = [
        'Mã',
        'Họ_Tên',
        'Địa_chỉ',
        'Sđt',
        'Email',
        'Username'
    ];

    //1 nhan vien co the tao nhieu chuyen xe
    public function chuyenxes(){
        //return $this->hasMany('App\Model bảng con', 'foreign_key')
        return $this->hasMany('App\Chuyenxe');
    }

    public function lotrinh(){
        return $this->hasMany('App\Lotrinh');
    }

    public function listItem($params = null)
    {
        $query = $this->select('*');

        if ($params['filter']['Loại_NV'] != "all") {
            $query->where('Loại_NV', $params['filter']['Loại_NV']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'Mã';
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                foreach ($this->search_field as $key => $search_field) {
                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("Mã", "DESC")->paginate(5);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        // dd($data);
        if (!isset($params['Mã'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            // $this->where('Mã', $params['Mã'])->update($data);
            $item = $this->where('Mã', $params['Mã']);
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $test = $params['Mã'];
            dd($test);
        }
        // $data = $params['form'];
        // $this->insert($data);
    }
}
