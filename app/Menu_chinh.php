<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;

class Menu_chinh extends Model
{
    protected $table = "menu_food";
    protected $primaryKey = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    protected $folderUpload = "food";

    public $timestamps = false;
    public $resize = [
        'standard' => ['width' => 400],
        'thumb' => ['width' => 150]
    ];
    public $search_field = [
        'id',
        'price',
        'name'
    ];

    public function ve(){
        return $this->hasMany('App\Ve');
    }

    public function listItem($params = null)
    {
        // dd($params);

        $query = $this->select('*');
        if ($params['filter']['status_food'] != "all") {
            $query->where('status', $params['filter']['status_food']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id';
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                foreach ($this->search_field as $key => $search_field) {

                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("id", "ASC")->paginate(5);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        if (!isset($params['id'])) {
            $pictureName = $this->upload($params['image']);
            $data['image'] = $pictureName;
            // $data['created_at'] = date("Y-m-d h:i:s");
            // $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $item = $this->where('id', $params['id']);
            if (isset($params['image'])) {
                $pictureName = $this->upload($params['image']);
                $data['image'] = $pictureName;

                //Delete old image
                $item->first()->removeImage();
            }

            // $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id'];
        }
    }


    public function getImage($option = "")
    {
        $src = asset("ticket/" . $this->folderUpload . "/" . $this->image);
        switch ($option) {
            case 'thumb':
                $src = asset("ticket/" . $this->folderUpload . "/thumb" . "/" . $this->image);
                break;
            case 'standard':
                $src = asset("ticket/" . $this->folderUpload . "/standard" . "/" . $this->image);
                break;
        }

        return $src;
    }

    public function upload($objImage)
    {
        $img = Image::make($objImage);
        $newName  = Str::random(8) . '.' . $objImage->getClientOriginalExtension();

        // $img->save('ticket/food/' . $newName);
        $img->save('ticket/' . $this->folderUpload . '/' . $newName);
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $img->resize($size['width'], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save('ticket/' . $this->folderUpload . '/' . $folder . '/' . $newName);
            }
        }
        return $newName;
    }

    public function removeImage()
    {
        $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $this->image;
        if (File::exists($path)) {
            File::delete($path);
        }
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $folder . '/' .  $this->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }
        }
    }
}
