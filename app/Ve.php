<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ve extends Model
{
    protected $table = "ticket";
    protected $primaryKey = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    public $timestamps = false;
    protected $fillable = array('id_ticket', 'id_customer', 'status', 'payment_status', 'id_payment', 'date_reg', 'time_change', 'date_change');

    public $search_field = [
        'id_buses',
        'phone',
        'date_start'

    ];

    public function lotrinh()
    {
        return $this->belongsTo('App\Lotrinh');
    }

    public function list_food()
    {
        return $this->belongsTo('App\list_food', 'id_ticket', 'id_ticket');
    }
    // public function route(){
    //     return $this->hasOne(Lotrinh::class);
    // }
    //1 nhan vien co the tao nhieu chuyen xe
    public function chuyenxe()
    {
        //return $this->hasMany('App\Model bảng con', 'foreign_key')
        return $this->belongsTo('App\Chuyenxe', 'id_buses');
    }

    public function khachhang()
    {
        return $this->belongsTo('App\Khachhang', 'id_customer', 'id');
    }
    public function menu()
    {
        return $this->belongsTo('App\Menu_chinh', 'id_food');
    }
    public function listItem($params = null)
    {
        $query = $this->select('*');
        if ($params['filter']['status'] != "all") {
            $query = $query->select('*');
            $query->where('status', $params['filter']['status']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id_ticket';
                // $query = $query->join('buses as b', 'b.id', '=', 'ticket.id_buses')
                // -> join('customer as c', 'c.id', '=', 'ticket.id_customer');
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                // $query = $query->leftJoin('buses as b', 'b.id', '=', 'ticket.id_buses')
                //                 -> leftJoin('customer as c', 'c.id', '=', 'ticket.id_customer');
                foreach ($this->search_field as $key => $search_field) {
                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("id_ticket", "DESC");
        $query = $query->orderBy("id", "DESC")->paginate(25);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        // dd($data);
        if (!isset($params['id'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            // $this->where('ID', $params['ID'])->update($data);
            $item = $this->where('id', $params['id']);
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return  $params['id'];
        }
    }
    public function sendsms($content, $phone)
    {
        $APIKey = "0C0DF645BC6AFB787D342D3515EDF0";
        $SecretKey = "C71B1127F155B06FDD355D3A2139B8";
        $YourPhone = $phone;
        $ch = curl_init();
        $SampleXml = "<RQST>"
                               . "<APIKEY>". $APIKey ."</APIKEY>"
                               . "<SECRETKEY>". $SecretKey ."</SECRETKEY>"
                               . "<ISFLASH>0</ISFLASH>"
                       		   . "<SMSTYPE>2</SMSTYPE>"
        					   . $content
        					   . "<BRANDNAME>Baotrixemay</BRANDNAME>"//De dang ky brandname rieng vui long lien he hotline 0902435340 hoac nhan vien kinh Doanh cua ban
                               . "<CONTACTS>"
                               . "<CUSTOMER>"
                               . "<PHONE>". $YourPhone ."</PHONE>"
                               . "</CUSTOMER>"
                               . "</CONTACTS>"
        					   . "</RQST>";


        curl_setopt($ch, CURLOPT_URL,            "http://api.esms.vn/MainService.svc/xml/SendMultipleMessage_V4/" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $SampleXml );
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

        $result=curl_exec ($ch);
        $xml = simplexml_load_string($result);

        if ($xml === false) {
        	die('Error parsing XML');
        }
    }
}
