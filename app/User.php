<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;
use App\Http\Controllers\Auth\ResetPasswordController;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = "users";
    protected $primaryKey = "id";

    protected $folderUpload = "user";

    // public $timestamps = true;
    public $resize = [
        'standard' => ['width' => 400],
        'thumb' => ['width' => 150]
    ];
    public $search_field = [
        'id',
        'name',
        'email'
    ];

    public function listItem($params = null)
    {
        // dd($params);

        $query = $this->select('*');
        if ($params['filter']['status'] != "all") {
            $query->where('status', $params['filter']['status']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id';
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                foreach ($this->search_field as $key => $search_field) {

                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("id", "DESC")->paginate(5);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        if (!isset($params['id'])) {
            $pictureName = $this->upload($params['image']);
            $data['image'] = $pictureName;
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $item = $this->where('id', $params['id']);
            if (isset($params['image'])) {
                $pictureName = $this->upload($params['image']);
                $data['image'] = $pictureName;

                //Delete old image
                $item->first()->removeImage();
            }

            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id'];
        }
    }


    public function getImage($option = "")
    {
        $src = asset("ticket/" . $this->folderUpload . "/" . $this->image);
        switch ($option) {
            case 'thumb':
                $src = asset("ticket/" . $this->folderUpload . "/thumb" . "/" . $this->image);
                break;
            case 'standard':
                $src = asset("ticket/" . $this->folderUpload . "/standard" . "/" . $this->image);
                break;
        }

        return $src;
    }

    public function upload($objImage)
    {
        $img = Image::make($objImage);
        $newName  = Str::random(8) . '.' . $objImage->getClientOriginalExtension();

        // $img->save('ticket/food/' . $newName);
        $img->save('ticket/' . $this->folderUpload . '/' . $newName);
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $img->resize($size['width'], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save('ticket/' . $this->folderUpload . '/' . $folder . '/' . $newName);
            }
        }
        return $newName;
    }

    public function removeImage()
    {
        $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $this->image;
        if (File::exists($path)) {
            File::delete($path);
        }
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $folder . '/' .  $this->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
