<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Khachhang extends Model
{
    protected $table = "customer";
    protected $primaryKey = "id";
    protected $folderUpload = "khachhang";

    public $timestamps = false;
    public $resize = [
        'standard' => ['width' => 400],
        'thumb' => ['width' => 150]
    ];
    public $search_field = [
        'id',
        'name',
        'email',
        'phone'
    ];

    public function ve(){
        return $this->hasMany('App\Ve');
    }


    public function listItem($params = null){

        $query = $this->select('*');
        if(!empty($params['search']['value'])){
            if($params['search']['field']!= "all"){
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id';
                $query->where($search_field, 'LIKE' , '%' .$params['search']['value'] .'%');
            }else{
                foreach($this->search_field as $key => $search_field){
                    $query->orWhere($search_field, 'LIKE' , '%' .$params['search']['value'] .'%');
                }
            }

        }

        $query = $query ->orderBy("id", "DESC")->paginate(5);
        return $query;
    }

    public function saveItem($params){
        $data = $params['form'];
        if (!isset($params['id'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $this->where('id', $params['id'])->update($data);
            $item = $this->where('id', $params['id']);
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id'];
        }
        // $data = $params['form'];
        // $this->insert($data);
    }


    public function getImage($option = ""){
        $src = asset("ticket/" . $this-> folderUpload . "/" . $this-> picture);
        return $src;
    }
}
