<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;

class Tintuc extends Model
{
    protected $table = "news";
    protected $primaryKey = "id_news";
    protected $folderUpload = "tintuc";

    public $timestamps = false;
    public $resize = [
        'standard' => ['width' => 400],
        'thumb' => ['width' => 150]
    ];
    public $search_field = [
        // 'id_news',
        // 'ticket_price',
        // 'date_start',
        // 'time_start',

    ];

    //1 chuyen xe do nhan vien nao do tao
    // public function nhanvien_qt(){
    //     return $this->belongsTo('App\Nhanvien','id_staff');
    // }
    // //Nhan vien tai xe
    // public function nhanvien_tx(){
    //     return $this->belongsTo('App\Nhanvien','id_driver');
    // }
    // //Bien so xe
    // public function xe(){
    //     return $this->belongsTo('App\Xe', 'id_vehicle');
    // }
    // // Lo trinh
    // public function route(){
    //     return $this->belongsTo('App\Lotrinh', 'id_route');
    // }

    public function listItem($params = null)
    {
        $query = $this->select('*');
        if ($params['filter']['check_slide'] != "all") {
            $query->where('check_slide', $params['filter']['check_slide']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id_news';
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                foreach ($this->search_field as $key => $search_field) {

                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("id_news", "DESC")->paginate(5);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        if (!isset($params['id_news'])) {
            $pictureName = $this->upload($params['image']);
            $data['image'] = $pictureName;
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $item = $this->where('id_news', $params['id_news']);
            if (isset($params['image'])) {
                $pictureName = $this->upload($params['image']);
                $data['image'] = $pictureName;

                //Delete old image
                $item->first()->removeImage();
            }
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id_news'];
        }
    }


    public function getImage($option = "")
    {
        $src = asset("ticket/" . $this->folderUpload . "/" . $this->image);
        switch ($option) {
            case 'thumb':
                $src = asset("ticket/" . $this->folderUpload . "/thumb" . "/" . $this->image);
                break;
            case 'standard':
                $src = asset("ticket/" . $this->folderUpload . "/standard" . "/" . $this->image);
                break;
        }

        return $src;
    }

    public function upload($objImage)
    {
        $img = Image::make($objImage);
        $newName  = Str::random(8) . '.' . $objImage->getClientOriginalExtension();

        // $img->save('ticket/food/' . $newName);
        $img->save('ticket/' . $this->folderUpload . '/' . $newName);
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $img->resize($size['width'], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save('ticket/' . $this->folderUpload . '/' . $folder . '/' . $newName);
            }
        }
        return $newName;
    }

    public function removeImage()
    {
        $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $this->image;
        if (File::exists($path)) {
            File::delete($path);
        }
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $folder . '/' .  $this->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }
        }
    }
}
