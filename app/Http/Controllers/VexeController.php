<?php

namespace App\Http\Controllers;

use App\Ve as MainModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VexeController extends Controller
{
    //
    public $pathView = 'admin.vexe.';
    public $controllerName = 'vexe';


    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['filter']['status'] = $request->input('filterStatus', 'all');
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id)->update(['status' => 0, 'payment_status' => 0, 'id_ticket' => null, 'id_customer' => null, 'id_payment' => null, 'date_reg' => null, 'time_change' => null, 'date_change' => null]);
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }


    public function form(Request $request)
    {
        $item = [];
        if(isset($request->id)){
            $item = MainModel::find($request->id);
        }
        return view($this->pathView . 'form')->with('item', $item);
    }


    public function save(Request $request)
    {

        $validatedData = $request->validate(
            [
                'form.id_buses' => 'required',
                'form.date_change' => 'required',
                'form.time_change' => 'required'
            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'after_or_equal' => ':attribute không được nhỏ hơn ngày hiện tại',
                'in' => ':attribute không hợp lệ',
                'email' => ':attribute không hợp lệ',
                'digits' => ':attribute không quá 10 số'
            ],
            [
                'form.id_buses' => 'Mã chuyến xe ',
                'form.date_change' => 'Ngày dời chuyến',
                'form.time_change' => 'Giờ dời chuyến'
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id="";

        if((!empty($request['form']['date_change'])&& !empty($request['form']['time_change'])))
        {
            $phone =   $request['phone'];
            $old_date = $request['form']['date_change'];
            $new_date = date("d/m/Y",strtotime($old_date));
            $time_change = $request['form']['time_change'];
            $seat = $request['form']['name_seat'];
            $content = "<CONTENT>". "THÔNG BÁO          Vé Xe của quý khách sẽ bị thay đổi. Khởi hành lúc: ".$time_change." Ngày: ".$new_date.". Tại ghế: ".$seat."      Xin chân thành cảm ơn!"."</CONTENT>";

            $id = $mainModel->saveItem($params);
            $mainModel->sendsms($content,$phone);
        }
        else{
            Session::flash('error', 'Failed - Chưa chọn ngày & giờ thay đổi');
            return redirect()->back();
        }
        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            return redirect()->route('admin.' . $this->controllerName . '.form', ['id'=> $id ] );
        }
    }
    public function sendsms(Request $request){
        if(!empty($request->phone))
        {
            $phone =   $request['phone'];
            $old_date = $request['date_start'];
            $new_date = date("d/m/Y",strtotime($old_date));
            $time_start = $request['time_start'];
            $seat = $request['seat'];
            $content = "<CONTENT>". "THÔNG BÁO          Quý khách có chuyến đi vào ngày: ".$new_date.", lúc: ".$time_start.". Tại ghế: ".$seat."        Xin chân thành cảm ơn!"."</CONTENT>";
            $mainModel = new MainModel;
            $mainModel->sendsms($content,$phone);
            Session::flash('success', 'Đã gửi tin nhắn nhắc nhỡ thành công!');
            return redirect()->back();
        }
        Session::flash('error', 'Failed - Ghế này chưa được đặt trước');
        return redirect()->back();
    }
}
