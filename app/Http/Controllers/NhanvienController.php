<?php

namespace App\Http\Controllers;

use App\Nhanvien as MainModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class NhanvienController extends Controller
{
    //
    public $pathView = 'admin.nhanvien.';
    public $controllerName = 'nhanvien';

    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['filter']['Loại_NV'] = $request->input('filterStatus', 'all');
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id)->delete();
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }


    public function form(Request $request)
    {
        $item = [];
        if(isset($request-> Mã)){
            $item = MainModel::find($request->Mã);
        }
        return view($this->pathView . 'form')->with('item', $item);
    }


    public function save(Request $request)
    {
        // dd($request);
        $validatedData = $request->validate(
            [
                'form.Họ_Tên' => 'required|min:10|max:80',
                'form.Sđt' => 'required|digits:10',
                'form.Email' => 'email:rfc,dns',
                'form.Giới_tính' => 'in:0,1',
                'form.Ngày_sinh' => 'required',
                'form.Loại_NV' => 'in:QTV,TX,QLDV',
                'form.Địa_chỉ' => 'required|max:255',
                'form.Date_Starting' => 'required'

            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'after_or_equal' => ':attribute không được nhỏ hơn ngày hiện tại',
                'in' => ':attribute không hợp lệ',
                'email' => ':attribute không hợp lệ',
                'digits' => ':attribute không quá 10 số'
            ],
            [
                'form.Họ_Tên' => 'Tên ',
                'form.Sđt' => 'Số điện thoại',
                'form.Email' => 'Email',
                'form.Giới_tính' => 'Giới tính',
                'form.Ngày_sinh' => 'Ngày sinh',
                'form.Loại_NV' => 'Loại nhân viên',
                'form.Địa_chỉ' => 'Địa chỉ',
                'form.Date_Starting' => 'Ngày bắt đầu'
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id = $mainModel->saveItem($params);

        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            return redirect()->route('admin.' . $this->controllerName . '.form', ['Mã'=> $id ] );
        }
    }
}
