<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProcessController extends Controller
{
    public function Chonve($id){

        $kt = DB::table("ticket")
        ->where("payment_status","=",0)
        ->where("id_customer","=",session('makh'))
        // ->where("id_ticket",">",session('id_ticket'))
        ->where("status",">",0)
        ->select("status","payment_status","date_reg")
        ->get();
        if(!empty($kt))
        {
            DB::update("UPDATE `ticket` SET `status`= 0 WHERE `id_customer`=".session('makh')." AND `payment_status` = 0" );

        }
        /* lấy thông tin chuyến xe*/
            $chonve = DB::table("buses")
                ->join("route","buses.id_route","=","route.id")
                ->join("vehicle","buses.id_vehicle","=","vehicle.id")
                ->join("bus_model","vehicle.type","=","bus_model.Mã")
                ->where("buses.id","=",$id)
                ->select("buses.id","date_start","time_start","to","from","type","ticket_price")
                ->get();
        /*lấy sơ đồ xe*/
            $sodo = DB::table("buses")
                ->join("vehicle","buses.id_vehicle","=","vehicle.id")
                ->join("bus_model","vehicle.type","=","bus_model.Mã")
                ->where("buses.id","=",$id)
                ->select("Sơ_đồ","Loại_ghế")
                ->get();
        /* lấy thông tin vé*/
            $ve = DB::table("buses")
                ->join("ticket","buses.id","=","ticket.id_buses")

                ->where("buses.id","=",$id)
                ->select("name_seat","ticket.status","ticket.id","ticket.id_customer","ticket.date_reg")
                ->get();
            $mon = DB::select("SELECT * FROM menu_food WHERE status = 1 ");

        /* Thêm thời gian */
            $tmp = date("Y-m-d H:i:s");
            $tmp = strtotime($tmp);
            foreach ($ve as $key => $value ) {
                $time = $ve[$key]->date_reg;
                $time = strtotime($time);
                $time = $tmp - $time;
                if($time < 600){
                    $time = 600 - $time;
                 }
                 else{
                    $time = null;
                 }
                $ve[$key]->TG = $time;
            }
            /////////////////////////

            ////////////////
        /* trả về trang chọn vé*/
            return view('tttn-web.chonve',['chonve'=> $chonve,'ve'=>$ve,'sodo'=>$sodo,'id'=>$id,"mon" => $mon]);
    }

    public function processbook(Request $request){
        $ma = $request -> MA;
        $makh = $request -> MAKH;
        $kt = DB::table("ticket")
        ->where("payment_status","=",0)
        ->where("id_customer","=",session('makh'))
        // ->where("id_ticket",">",session('id_ticket'))
        ->where("status",">",0)
        ->select("status","payment_status","date_reg")
        ->get();
        if(!empty($kt))
        {
            DB::update("UPDATE `ticket` SET `status`= 0 WHERE `id_customer`=".session('makh')." AND `payment_status` = 0" );

        }

        /* Kiểm tra trạng thái vé*/
            $kt = DB::table("ticket")
                ->where("id","=",$ma)
                ->select("status","payment_status","date_reg")
                ->get();
            if($kt[0]->status == 0){
                $time = date("Y-m-d H:i:s");
                /* update trạng thái vé*/
                DB::update("UPDATE `ticket` SET `status`= ?,`id_customer`= ?, `date_reg`= ? WHERE `id`= ?",
                    [2,$makh,$time,$ma]);
                /*hàm sleep đếm ngược*/
                sleep(600);
                /*kiểm tra trạng thái và mã khách hàng*/
                    $kt2 = DB::table("ticket")
                        ->where("id","=",$ma)
                        ->select("status","id_customer","id","date_reg")
                        ->get();
                    if($kt2[0]->status == 2  && $kt2[0]->Thời_điểm_chọn == $time){
                        /*update trạng thái vé*/
                        DB::update("UPDATE `ticket` SET `status`= ?,`id_customer`= ?, `reg_date`=? WHERE `id`= ?",
                            [0,null,null,$ma]);
            }
            /*trả về kq ajax*/
                return \response()->json(['kq'=>0]);
        }
        else if($kt[0]->status == 1 ){
                return \response()->json(['kq'=>1]);
        }

        else if($kt[0]->status == 2){
            $tmp = date("Y-m-d H:i:s");
            $tmp = strtotime($tmp);
            $time = $kt[0]->date_reg;
            $time = strtotime($time);
            $time = $tmp - $time;
            if($time < 600){
                $time = 600 - $time;
            }
            else{
                $time = null;
            }
            /*trả về kq ajax*/
                return \response()->json(['kq'=>2,'TGC'=>$time]);
        }
    }
    /* Xử lý hủy giữ vé*/
    public function processcancel(Request $request){
        $ma = $request -> MA;
        /*update trạng thái vé*/
            DB::update("UPDATE `ticket` SET `status`= ?,`id_customer`= ?, `date_reg`= ? WHERE `id`= ?",
                [0,Null,Null,$ma]);
        /*trả kết quả về ajax*/
            return \response()->json(['kq'=>1]);
    }
    public function chondatve(Request $request){
        $id = $request-> ID;
        $mang = $request-> MANG;
        $makh = $request -> MAKH;
        $cm=$request -> CHONMON;
        $dodai = $request -> DODAI;
        $sl=$request->SL;
        $rd_Madatve=$request -> RAN;
        // rand(0,99999)
        session()->put('id_ticket', $rd_Madatve);
        session()->forget('t.array');
        for($i=0;$i<$dodai;$i++){


            if($mang[$i] != null ){
                        if(empty($cm)&&empty($sl))
                        {
                            session()->put('cancel_book_id',$rd_Madatve);
                            DB::update("UPDATE `ticket` SET `status`= ?,`id_customer`= ?,`id_ticket`= ? WHERE `id`= ? ",
                            [1,$makh, $rd_Madatve,$mang[$i]]);
                        }
                        else{
                            session()->put('session_check_food',$cm);
                            session()->put('cancel_book_id',$rd_Madatve);
                            DB::update("UPDATE `ticket` SET `status`= ?,`id_customer`= ?,`id_ticket`= ? WHERE `id`= ? ",
                            [1,$makh, $rd_Madatve,$mang[$i]]);
                            DB::insert('insert into list_food (id_menu,quatity,id_ticket) values (?,?,?)', [$cm,$sl,$rd_Madatve]);
                        }
            }
        }
            return \response()->json(['id'=>$id,'makh'=>$makh,]);
    }
    public function finish($id,$makh,$ran){
        /* lấy thông tin vé*/
            $chonve = DB::table("buses")
                ->join("route","buses.id_route","=","route.id")
                ->join("vehicle","buses.id_vehicle","=","vehicle.id")
                ->join("bus_model","vehicle.type","=","bus_model.Mã")
                ->where("buses.id","=",$id)
                ->select("date_start","time_start","to","from","type","ticket_price")
                ->get();
                foreach($chonve as $value)
                {
                    session()->put('data_info.from_place',$value->from);
                    session()->put('data_info.to_place',$value->to);
                    session()->put('data_info.date',$value->date_start);
                    session()->put('data_info.time',$value->time_start);
                }
                // session()->put('data_info.seat',$array_seat);
        /* lấy thông tin vé đã đặt*/
        $array_seat=[];
            $vedadat = DB::table("ticket")

                ->where("id_buses","=",$id)
                ->where("id_customer","=",$makh)
                ->where("id_ticket","=",$ran)
                ->select("name_seat")
                ->get();

               foreach($vedadat as $key)
               {
                array_push($array_seat,$key->name_seat);
               }
               $seat = implode(',',$array_seat);
               session()->put('data_info.seat',$seat);
        //    dd("Quý khách đã mua thành công vé: ".session('data_info.seat').". Mã vé: ".session('id_ticket').", Tuyến ".session('data_info.from_place')." - ".session('data_info.to_place').". Xuất bến lúc: ".session('data_info.time')." Ngày: ".session('data_info.date').". Chân thành cảm ơn");
            $mondachon = DB::table('menu_food')

                ->join("list_food","menu_food.id","=","list_food.id_menu")
                ->join("ticket","list_food.id_ticket","=","ticket.id_ticket")
                ->where("ticket.id_buses","=",$id)
                ->where("ticket.id_customer","=",$makh)
                ->where("list_food.id_ticket","=",$ran)
                ->select("name","price","list_food.quatity")
                ->get();
        /* gửi thông tin về trang thông tin vé*/
            return view('tttn-web.thongtinve',["chonve" => $chonve,"vedadat"=>$vedadat,"mondachon"=>$mondachon]);
    }
    public function payment(){
        return view('vnpay.index');
    }
    public function payment_create(Request $request)
        {


            $vnp_TmnCode = "9UCGQKER";
            $vnp_HashSecret = "WVDFCIDUXVOFXVQTPIHUCAGDDBETKNLW";
            $vnp_Url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
            $vnp_Returnurl = route('return');
            $vnp_TxnRef = session()->get('id_ticket') ;//Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
            // $vnp_TxnRef = $_POST['order_id']; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
            $vnp_OrderInfo = $_POST['order_desc'];
            $vnp_OrderType = $_POST['order_type'];
            $vnp_Amount = session()->get('total_money') * 100;
            $vnp_Locale = $_POST['language'];
            $vnp_BankCode = $_POST['bank_code'];
            $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

            $inputData = array(
                "vnp_Version" => "2.0.0",
                "vnp_TmnCode" => $vnp_TmnCode,
                "vnp_Amount" => $vnp_Amount,
                "vnp_Command" => "pay",
                "vnp_CreateDate" => date('YmdHis'),
                "vnp_CurrCode" => "VND",
                "vnp_IpAddr" => $vnp_IpAddr,
                "vnp_Locale" => $vnp_Locale,
                "vnp_OrderInfo" => $vnp_OrderInfo,
                "vnp_OrderType" => $vnp_OrderType,
                "vnp_ReturnUrl" => $vnp_Returnurl,
                "vnp_TxnRef" => $vnp_TxnRef,
            );
            if (isset($vnp_BankCode) && $vnp_BankCode != "") {
                $inputData['vnp_BankCode'] = $vnp_BankCode;
            }
            ksort($inputData);
            $query = "";
            $i = 0;
            $hashdata = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashdata .= '&' . $key . "=" . $value;
                } else {
                    $hashdata .= $key . "=" . $value;
                    $i = 1;
                }
                $query .= urlencode($key) . "=" . urlencode($value) . '&';
            }

            $vnp_Url = $vnp_Url . "?" . $query;
            if (isset($vnp_HashSecret)) {
               // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
                $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
                $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
            }
            return redirect($vnp_Url);
        }
    public function payment_return(Request $request)
        {
           $inputData = array();
           foreach ($_GET as $key => $value) {
               if (substr($key, 0, 4) == "vnp_") {
                   $inputData[$key] = $value;
               }
           }
           unset($inputData['vnp_SecureHashType']);
           unset($inputData['vnp_SecureHash']);
           ksort($inputData);
           $i = 0;
           $hashData = "";
           foreach ($inputData as $key => $value) {
               if ($i == 1) {
                   $hashData = $hashData . '&' . $key . "=" . $value;
               } else {
                   $hashData = $hashData . $key . "=" . $value;
                   $i = 1;
               }
           }
            if($request->vnp_ResponseCode == "00") {
                                $order_id = $_GET['vnp_TxnRef'];
                                $money = $_GET['vnp_Amount']/100;
                                $note = $_GET['vnp_OrderInfo'];
                                $vnp_response_code = $_GET['vnp_ResponseCode'];
                                $code_vnpay = $_GET['vnp_TransactionNo'];
                                $code_bank = $_GET['vnp_BankCode'];
                                $time = $_GET['vnp_PayDate'];
                                $date_time = substr($time, 0, 4) . '-' . substr($time, 4, 2) . '-' . substr($time, 6, 2) . ':' . substr($time, 8, 2) . ':' . substr($time, 10, 2) . ':' . substr($time, 12, 2);
                                $taikhoan = session()->get('sdt');
                                $sql =  DB::table("payments")
                                ->where("order_id","=",$order_id)
                                ->select("*")
                                ->get();

                                if(!$sql){
                                    $sql=DB::update("UPDATE `payments` SET `order_id`= ?,`money`= ?,`note`= ?,`vnp_response_code`=?,`code_vnpay`=?,`code_bank`=? WHERE `order_id`= ? ",
                                    [$order_id,$money, $note,$vnp_response_code,$code_vnpay,$code_bank,$order_id]);

                                }
                                else
                                {

                                    $sql=DB::insert('insert into payments (order_id, thanh_vien, money, note, vnp_response_code, code_vnpay, code_bank, time) values (?,?,?,?,?,?,?,?)', [$order_id, $taikhoan, $money, $note, $vnp_response_code, $code_vnpay, $code_bank,$date_time]);
                                    DB::update("UPDATE `ticket` SET `payment_status`= 1, `id_payment` = $order_id WHERE `id_ticket` =". session('id_ticket') );
                                }

                ///////////////////////////////////send sms////////////////////////////////////
        $APIKey="0C0DF645BC6AFB787D342D3515EDF0";
		$SecretKey="C71B1127F155B06FDD355D3A2139B8";
		$YourPhone=session()->get('sdt');
        $ch = curl_init();
        $old_date = session('data_info.date');
		$new_date=date("d/m/Y",strtotime($old_date));
		$SampleXml = "<RQST>"
                               . "<APIKEY>". $APIKey ."</APIKEY>"
                               . "<SECRETKEY>". $SecretKey ."</SECRETKEY>"
                               . "<ISFLASH>0</ISFLASH>"
		               		   . "<SMSTYPE>2</SMSTYPE>"
							   . "<CONTENT>". "Quý khách đã mua thành công vé: ".session('data_info.seat').". Mã vé: ".session('id_ticket').", Tuyến ".session('data_info.from_place')." - ".session('data_info.to_place').". Xuất bến lúc: ".session('data_info.time')." Ngày: ".$new_date.". Chân thành cảm ơn"."</CONTENT>"
							   . "<BRANDNAME>Baotrixemay</BRANDNAME>"//De dang ky brandname rieng vui long lien he hotline 0902435340 hoac nhan vien kinh Doanh cua ban
                               . "<CONTACTS>"
                               . "<CUSTOMER>"
                               . "<PHONE>". $YourPhone ."</PHONE>"
                               . "</CUSTOMER>"
                               . "</CONTACTS>"
							   . "</RQST>";


		curl_setopt($ch, CURLOPT_URL,            "http://api.esms.vn/MainService.svc/xml/SendMultipleMessage_V4/" );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $SampleXml );
		curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

		$result=curl_exec ($ch);
		$xml = simplexml_load_string($result);

		if ($xml === false) {
			die('Error parsing XML');
		}
                /////////////////////////////////////////////////////////////////////////////////
                return redirect()->route('result')->with('success' ,'Thanh toán thành công');
                }
            // session()->forget('url_prev');
            if( session('session_check_food')){
                DB::update("UPDATE `ve` SET `Trạng_thái`= ?,`Mã_khách_hàng`= ?,`Mã_đặt_vé`= ? WHERE `Mã_đặt_vé`= ? ",
                [0,NULL, NULL,session('cancel_book_id')]);
                DB::table('test')->where('Mã_đặt_vé',session('cancel_book_id'))->delete();
                return redirect()->route('result')->with('errors' ,'Thanh toán thất bạn, vui lòng thử lại');
            }
            DB::update("UPDATE `ve` SET `Trạng_thái`= ?,`Mã_khách_hàng`= ?,`Mã_đặt_vé`= ? WHERE `Mã_đặt_vé`= ? ",
            [0,NULL, NULL,session('cancel_book_id')]);
            return redirect()->route('result')->with('errors' ,'Thanh toán thất bạn, vui lòng thử lại');
        }
        public function result(){
            $order_id = session()->get('id_ticket');
             $sql =  DB::table("payments")
             ->where("order_id","=",$order_id)
             ->select("*")
             ->get();
             return view('vnpay.vnpay_return',['sql'=>$sql]);
         }
}
