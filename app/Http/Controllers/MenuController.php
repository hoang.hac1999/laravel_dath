<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu_chinh as MainModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MenuController extends Controller
{
    //
    public $pathView = 'admin.menu.';
    public $controllerName = 'menu';

    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['filter']['status_food'] = $request->input('filterStatus', 'all');
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id);
                $item -> removeImage();
                $item -> delete();
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }

    public function form(Request $request)
    {
        $item = [];
        if(isset($request->id)){
            $item = MainModel::find($request->id);
        }
        return view($this->pathView . 'form')->with('item', $item);
        // $mainModel = new MainModel;
        // $data = $mainModel->getSelectOption();
        // return view($this->pathView . 'form')->with('data', $data);
    }


    public function save(Request $request)
    {
        $validatedData = $request->validate(
            [
                'form.name' => 'required|min:5|max: 50',
                'form.price' => 'required|numeric',
                'form.image' => 'file',
                'form.status' => 'required|in:0,1',

            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'in' => ':attribute không hợp lệ',
                'numeric' => ':attribute phải là số',
                'file' => 'Định dạng :attribute là *.jpg, *.png '
            ],
            [
                'form.name' => 'Tên món ăn',
                'form.price' => 'giá',
                'form.image' => 'Hình ảnh',
                'form.status' => 'Trạng thái',
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id = $mainModel->saveItem($params);

        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            return redirect()->route('admin.' . $this->controllerName . '.form', ['id'=> $id ] );
        }
    }
}
