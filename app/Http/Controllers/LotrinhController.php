<?php

namespace App\Http\Controllers;

use App\Lotrinh as MainModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LotrinhController extends Controller
{
    //
    public $pathView = 'admin.lotrinh.';
    public $controllerName = 'lotrinh';

    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id)->delete();
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }


    public function form(Request $request)
    {
        $item = [];
        if(isset($request->id)){
            $item = MainModel::find($request->id);
        }
        return view($this->pathView . 'form')->with('item', $item);
    }


    public function save(Request $request)
    {
        // dd($request);
        $validatedData = $request->validate(
            [
                'form.from' => 'required',
                'form.to' => 'required'
            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'after_or_equal' => ':attribute không được nhỏ hơn ngày hiện tại',
                'in' => ':attribute không hợp lệ',
                'email' => ':attribute không hợp lệ',
                'digits' => ':attribute không quá 10 số'
            ],
            [
                'form.from' => 'Điểm xuất phát',
                'form.to' => 'Diểm đến'
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id = $mainModel->saveItem($params);

        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            return redirect()->route('admin.' . $this->controllerName . '.form', ['id'=> $id ] );
        }
    }
}
