<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tintuc as MainModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class TintucController extends Controller
{
    //
    public $pathView = 'admin.tintuc.';
    public $controllerName = 'tintuc';

    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['filter']['check_slide'] = $request->input('filterStatus', 'all');
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id);
                $item -> removeImage();
                $item -> delete();
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }

    public function changeStatus($status, Request $request)
    {
        $status = $status == "waiting" ? "running" : "waiting";
        $id = $request->ID;
        // MainModel::find($id)->update(['status' => $status]);
        $mainModel = new MainModel;
        $mainModel->where('id', $id)->update(['status' => $status]);
        $response = [
            'link' => route('admin.' . $this->controllerName . '.changeStatus', ['status' => $status]),
            'status' => $status,
            'id' => $id
        ];
        return response()->json($response);
    }


    public function form(Request $request)
    {
        $item = [];
        if(isset($request->id_news)){
            $item = MainModel::find($request->id_news);
        }
        return view($this->pathView . 'form')->with('item', $item);
    }

    // public function formEdit(Request $request)
    // {
    //     $item = [];
    //     $mainModel = new MainModel;
    //     if (isset($request->id)) {
    //         $item = $mainModel::find($request->id);
    //     }
    //     return view($this->pathView . 'formEdit')->with('item', $item);
    // }

    public function save(Request $request)
    {
        $validatedData = $request->validate(
            [
                'form.title' => 'required',
            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'after_or_equal' => ':attribute không được nhỏ hơn ngày hiện tại',
                'in' => ':attribute không hợp lệ'
            ],
            [
                'form.title' => 'Tiêu đề',
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id = $mainModel->saveItem($params);

        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            return redirect()->route('admin.' . $this->controllerName . '.form', ['id_news'=> $id ] );
        }
    }


}
