<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Chuyenxe;
use App\Customer;
use App\Duongdi;
use App\Khachhang;
use App\Loaixe;
use App\Lotrinh;
use App\Nhanvien;
use App\Tinh;
use App\Tramdung;
use App\Ve;
use App\Xe;
use App\Tintuc;

class ControllerCus extends BaseController
{
    public function Index(){
        $province = DB::select("SELECT * FROM province");
        /*lấy 3 tin tức mới nhất*/
        $news = DB::table("news")
        ->orderBy('id_news', 'desc')
        ->limit(3)
        ->select("*")
        ->get();
    /* lấy ảnh slide*/
        $slide = DB::table("news")
            ->where("check_slide","=","1")
            ->select("image","title","id_news")
            ->get();
            
            return view('tttn-web.index',["province" => $province,"news"=>$news, "slide"=>$slide]);
            
    }
    public function Buses(Request $request){
        $from = $request->noidi;
        $to = $request->noiden;
        /*chuyển đổi thời gian đúng định dạng Y-m-d*/
            $Thoigian =  date('Y-m-d',strtotime($request->Ngaydi));
        /*lấy thông tin chuyến xe tìm được */
            $Chuyenxe = DB::table("buses")
                ->join("route","buses.id_route","=","route.id")
                ->join("vehicle","buses.id_vehicle","=","vehicle.id")
                ->join("bus_model","vehicle.type","=","bus_model.Mã")
                ->where("from","=",$from)
                ->where("to","=",$to)
                ->where("status","=","0")
                ->where("is_del","=",0)
                ->where('date_start','=',$Thoigian)
                ->select("from","to","date_start","time_start","route.estime","buses.id","ticket_price","type","license_plate")
                ->get();
        
        /*trả về trang chuyến xe*/
        return view('tttn-web.chuyenxe',["Chuyenxe" => $Chuyenxe]);
    }
    public function login(Request $request){
        $dndt = $request->DNDT;
        $dnmk = md5($request->DNMK); // mã hóa mật khẩu
        /* kiểm tra  số điện thoại và mật khẩu*/
            $dnkt = DB::select("SELECT * FROM customer WHERE phone = ? AND password = ?",[$dndt,$dnmk]);
            if($dnkt){
                $makh = $dnkt[0]->id;
                $sdt = $dnkt[0]->phone;
                $request->session()->put("makh", $makh); // tạo session tên makh
                $request->session()->put("sdt", $sdt); // tạo session tên sdt
                /* trả về ajax*/
                    return \response()->json(['kq'=>1,'sdt'=>$sdt,'ma'=>$makh]);
            }
            else{
                /* trả về ajax*/
                    return \response()->json(['kq'=>0]);
            }

    }
    /* Đăng ký */
    public function register(Request $request){
        $sdt = $request->SDT;
        $mk = $request->MK;
        $ngaysinh = $request->NGAYSINH;
        $gt = $request->GT;
        $name = $request->NAME;
      
        $namekd = FunctionBase::convertAlias($name); //đổi tên về không dấu
        /* kiểm tra số điện thoại*/
            $kt = DB::select("SELECT * FROM customer WHERE phone = ? ",[$sdt]);
            if(!$kt){
                /* thêm mới khách hàng*/
                $account = new Customer();
                $account["phone"] = $sdt;
                $account["password"] = md5($mk);
                $account["name"] = $name;
                $account["name_en"] =$namekd;
                $account["birthday"] = $ngaysinh;
                $account["sex"] = $gt;
                $account->save();
                /* trả kết quả về ajax*/
                    return \response()->json(['kq'=>1]);
               
            }
            else{
                /* trả kết quả về ajax*/
                    return \response()->json(['kq'=>0]);
                }
        } 
    
}
