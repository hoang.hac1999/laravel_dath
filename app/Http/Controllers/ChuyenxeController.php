<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chuyenxe as MainModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ChuyenxeController extends Controller
{
    //
    public $pathView = 'admin.chuyenxe.';
    public $controllerName = 'chuyenxe';

    public function __construct()
    {
        view()->share("controllerName", $this->controllerName);
        view()->share("pathView", $this->pathView);
    }

    public function index(Request $request)
    {
        $params['filter']['status'] = $request->input('filterStatus', 'all');
        $params['search']['field'] = $request->input('search_field', 'all');
        $params['search']['value'] = $request->input('search_value', '');

        $mainModel = new MainModel;
        $items = $mainModel->listItem($params);
        return view($this->pathView . 'index')->with('items', $items)
            ->with('params', $params);
    }

    public function deleteItem(Request $request)
    {
        $arrId = $request->cid;
        if (count($arrId) > 0) {
            foreach ($arrId as $key => $id) {
                $item = MainModel::find($id);
                $item -> removeImage();
                $item -> delete();
                // $item = MainModel::find($id)->delete();
            }
        }
        Session::flash('success', 'Bạn đã xóa thành công!');
        return redirect()->back();
    }

    public function changeStatus($status, Request $request)
    {
        $status = $status == "waiting" ? "running" : "waiting";
        $id = $request->ID;
        // MainModel::find($id)->update(['status' => $status]);
        $mainModel = new MainModel;
        $mainModel->where('id', $id)->update(['status' => $status]);
        $response = [
            'link' => route('admin.' . $this->controllerName . '.changeStatus', ['status' => $status]),
            'status' => $status,
            'id' => $id
        ];
        return response()->json($response);
    }


    public function form(Request $request)
    {
        $item = [];
        if(isset($request->id)){
            $item = MainModel::find($request->id);
        }
        return view($this->pathView . 'form')->with('item', $item);
    }

    public function formEdit(Request $request)
    {
        $item = [];
        $mainModel = new MainModel;
        if (isset($request->id)) {
            $item = $mainModel::find($request->id);
        }
        return view($this->pathView . 'formEdit')->with('item', $item);
    }

    public function save(Request $request)
    {

        $validatedData = $request->validate(
            [
                // 'form.date_start' => 'required|date|after_or_equal:today',
                'form.time_start' => 'required',
                'form.ticket_price' => 'required',
                'form.status' => 'in:0,1,2,3',
                'form.id_driver' => 'required',
                'form.id_staff' => 'required',
                'form.id_route' => 'required',
                'form.id_vehicle' => 'required'
            ],
            [
                'required' => ':attribute không được rỗng',
                'min' => ':attribute ít nhất :min ký tự',
                'max' => ':attribute không được quá :max ký tự',
                'after_or_equal' => ':attribute không được nhỏ hơn ngày hiện tại',
                'in' => ':attribute không hợp lệ'
            ],
            [
                'form.date_start' => 'Ngày khởi hành',
                'form.time_start' => 'Giờ khởi hành',
                'form.ticket_price' => 'Giá vé',
                'form.status' => 'Trạng thái',
                'form.id_driver' => 'Mã tài xế',
                'form.id_staff' => 'Mã nhân viên',
                'form.id_route' => 'Mã lộ trình',
                'form.id_vehicle' => 'Mã xe'
            ]
        );

        $params = $request->all();
        $mainModel = new MainModel;
        $id = $mainModel->saveItem($params);
        if ($request->type == "close") {
            return redirect()->route('admin.' . $this->controllerName . '.index');
        }
        if ($request->type == "new") {
            return redirect()->route('admin.' . $this->controllerName . '.form');
        }
        if ($request->type == "save") {
            $data=$params['form'];
            $idxe = $data['id_vehicle'];
            $created_at = date('Y-m-d h-i-s');
            $updated_at = date('Y-m-d h-i-s');

        $loaixe = DB::table('vehicle')
                    ->join('bus_model', 'vehicle.type', '=', 'bus_model.Mã')
                    ->where('vehicle.ID', '=', $idxe)
                    ->select('bus_model.Số_hàng', 'bus_model.Số_cột', 'bus_model.Sơ_đồ', 'bus_model.Loại_ghế')
                    ->get();

        foreach ($loaixe as $row) {
            $loaixe = (array)$row;
        }

        $sodo = $loaixe["Sơ_đồ"];
        $row = intval($loaixe["Số_hàng"]);
        $col  = intval($loaixe["Số_cột"]);
        $loaighe = $loaixe["Loại_ghế"];
        $trangthai = 0;
        $k = 1;
        if ($loaighe == 1) {
            $row = ($row - 1) / 2 + 1;
        }
        for ($i = 1; $i < $row; $i++) {
            //                    $k = 1;
            for ($j = 0; $j < $col; $j++) {
                /*if($i*$col+$j == 0)
                    continue;*/
                if ($sodo[$i * $col + $j] == 1) {
                    $vitri = 'A-' . ($k);
                    DB::insert(
                        "INSERT INTO `ticket`(`id_buses`, `name_seat`, `status`, `created_at`, `updated_at`) VALUES (?,?,?,?,?)",
                        [$id, $vitri, $trangthai, $created_at, $updated_at]
                    );
                    $k++;
                }
            }
        }
        if ($loaighe == 1) {
            $k = 1;
            for ($i = $row; $i < intval($loaixe["Số_dòng"]); $i++) {
                //                        $k = 1;
                for ($j = 0; $j < $col; $j++) {
                    /*if ($i * $col + $j == 0)
                        continue;*/
                    if ($sodo[$i * $col + $j] == 1) {
                        $vitri = 'B-' . ($k);
                        DB::insert(
                            "INSERT INTO `ticket`(`id_buses`, `name_seat`, `status`, `created_at`, `updated_at`) VALUES (?,?,?,?,?)",
                            [$id, $vitri, $trangthai, $created_at, $updated_at]
                        );
                        $k++;
                    }
                }
            }
        }
            return redirect()->route('admin.' . $this->controllerName . '.form', ['id'=> $id ] );
        }


    }


}
