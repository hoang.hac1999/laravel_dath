<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;

class Chuyenxe extends Model
{
    protected $table = "buses";
    protected $primaryKey = "id";
    protected $folderUpload = "chuyenxe";

    public $timestamps = false;
    public $resize = [
        'standard' => ['width' => 400],
        'thumb' => ['width' => 150]
    ];
    public $search_field = [
        'id',
        'ticket_price',
        'date_start',
        'time_start',

    ];

    //1 chuyen xe do nhan vien nao do tao
    public function nhanvien_qt(){
        return $this->belongsTo('App\Nhanvien','id_staff', 'Mã');
    }
    //Nhan vien tai xe
    public function nhanvien_tx(){
        return $this->belongsTo('App\Nhanvien','id_driver');
    }
    //Bien so xe
    public function xe(){
        return $this->belongsTo('App\Xe', 'id_vehicle');
    }
    // Lo trinh
    public function route(){
        return $this->belongsTo('App\Lotrinh', 'id_route');
    }
    public function ve(){
        return $this->hasMany('App\Ve');
    }

    public function listItem($params = null)
    {
        $query = $this->select('*');
        if ($params['filter']['status'] != "all") {
            $query->where('status', $params['filter']['status']);
        }
        if (!empty($params['search']['value'])) {
            if ($params['search']['field'] != "all") {
                $search_field = in_array($params['search']['field'], $this->search_field) ? $params['search']['field'] : 'id';
                $query->where($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
            } else {
                foreach ($this->search_field as $key => $search_field) {

                    $query->orWhere($search_field, 'LIKE', '%' . $params['search']['value'] . '%');
                }
            }
        }

        $query = $query->orderBy("id", "DESC")->paginate(5);

        return $query;
    }

    public function saveItem($params)
    {
        $data = $params['form'];
        if (!isset($params['id'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");

            return $this->insertGetId($data);
        } else {
            $item = $this->where('id', $params['id']);
            $data['updated_at'] = date("Y-m-d h:i:s");
            $item->update($data);
            return $params['id'];
        }
    }

    public function getSelectOption()
    {
        $query = $this->crossJoin('employee')
            ->crossJoin('route')
            ->crossJoin('vehicle')
            // ->where('employee1.employee_model', 'LIKE', 'tx_%')
            ->select('employee.Mã as eID', 'employee.Họ_Tên as staff_created', 'employee.Họ_Tên as driver', 'route.id as rID', 'route.from as departure', 'route.to as destination', 'vehicle.id as vID', 'vehicle.license_plate as license')
            ->distinct()
            ->get();
        return $query;
    }
    public function getImage($option = "")
    {
        $src = asset("ticket/" . $this->folderUpload . "/" . $this->picture);
        switch ($option) {
            case 'thumb':
                $src = asset("ticket/" . $this->folderUpload . "/thumb" . "/" . $this->picture);
                break;
            case 'standard':
                $src = asset("ticket/" . $this->folderUpload . "/standard" . "/" . $this->picture);
                break;
        }

        return $src;
    }

    public function upload($objImage)
    {
        $img = Image::make($objImage);
        $newName  = Str::random(8) . '.' . $objImage->getClientOriginalExtension();

        // $img->save('ticket/food/' . $newName);
        $img->save('ticket/' . $this->folderUpload . '/' . $newName);
        if (!empty($this->resize)) {
            foreach ($this->resize as $folder => $size) {
                $img->resize($size['width'], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save('ticket/' . $this->folderUpload . '/' . $folder . '/' . $newName);
            }
        }
        return $newName;
    }

    // public function removeImage()
    // {
    //     $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $this->picture;
    //     if (File::exists($path)) {
    //         File::delete($path);
    //     }
    //     if (!empty($this->resize)) {
    //         foreach ($this->resize as $folder => $size) {
    //             $path = public_path() . '/' . 'ticket/' . $this->folderUpload . '/' . $folder . '/' .  $this->picture;
    //             if (File::exists($path)) {
    //                 File::delete($path);
    //             }
    //         }
    //     }
    // }
}
